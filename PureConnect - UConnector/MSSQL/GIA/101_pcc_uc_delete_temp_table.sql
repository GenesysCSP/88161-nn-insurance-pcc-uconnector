USE [SpeechMiner]
GO

/****** Object:  StoredProcedure [dbo].[pcc_uc_delete_temp_table]    Script Date: 01/02/2019 04:59:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[pcc_uc_delete_temp_table]
@table_name VARCHAR(50)
AS
BEGIN

	DECLARE @sql VARCHAR(300)
	
	SET @sql = 'IF OBJECT_ID(''' + @table_name + ''') IS NOT NULL 
		DROP TABLE ' + @table_name
		
	EXECUTE (@sql)

END
GO


