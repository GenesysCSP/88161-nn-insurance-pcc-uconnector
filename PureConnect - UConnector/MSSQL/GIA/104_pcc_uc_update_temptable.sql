USE [SpeechMiner]
GO

/****** Object:  StoredProcedure [dbo].[pcc_uc_update_temptable]    Script Date: 01/02/2019 05:00:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[pcc_uc_update_temptable]
@parentId INT,
@childId INT,
@table_name VARCHAR(50)
AS
BEGIN

	DECLARE @sql VARCHAR(200)

	SET @sql = 'INSERT INTO ' + @table_name + ' VALUES(' + CONVERT(VARCHAR(5), @parentId) + ',' + CONVERT(VARCHAR(5),@childId) + ')'
	
	EXECUTE(@sql)

END
GO


