
GO

/****** Object:  StoredProcedure [dbo].[PCC_UC_DeleteLastHandledRecords]    Script Date: 1/2/2019 5:33:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PCC_UC_DeleteLastHandledRecords] (@comment varchar(MAX))
AS
BEGIN
delete from PURECONNECT_UCONNECTOR where comment=@comment
END
GO


