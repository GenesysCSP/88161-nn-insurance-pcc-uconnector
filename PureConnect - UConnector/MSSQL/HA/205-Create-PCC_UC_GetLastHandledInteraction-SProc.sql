
GO

/****** Object:  StoredProcedure [dbo].[PCC_UC_GetLastHandledInteraction]    Script Date: 1/2/2019 5:33:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PCC_UC_GetLastHandledInteraction] (@comment varchar(MAX))
AS
BEGIN
select top(1) ts from PURECONNECT_UCONNECTOR where comment = @comment order by ts desc
END
GO


