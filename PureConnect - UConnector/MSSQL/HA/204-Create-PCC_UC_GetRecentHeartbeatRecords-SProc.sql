
GO

/****** Object:  StoredProcedure [dbo].[PCC_UC_GetRecentHeartbeatRecords]    Script Date: 1/2/2019 5:33:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PCC_UC_GetRecentHeartbeatRecords] (@lasttime datetime, @comment varchar(MAX))
AS
BEGIN
SELECT * FROM PURECONNECT_UCONNECTOR WHERE ts>@lasttime and comment = @comment
END
		
GO


