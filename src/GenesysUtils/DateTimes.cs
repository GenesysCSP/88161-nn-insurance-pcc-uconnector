﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenUtils.DateTimes
{
    public static partial class DateTimes
    {
        #region Data

        public const string DATE_FORMAT = "yyyy-MM-dd";
        public const string DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
        public const string TIME_FORMAT = "HH:mm:ss";

        #endregion

        #region Properties

        public static DateTime Today
        {
            get
            {
                return DateTime.Today;
            }
        }

        public static DateTime Tomorrow
        {
            get
            {
                return DateTime.Today.AddDays(1);
            }
        }

        public static DateTime Yesterday
        {
            get
            {
                return DateTime.Today.AddDays(-1);
            }
        }

        #endregion

        #region Methods

        public static void GetDateStartEnd(
            DateTime date,
            out DateTime start,
            out DateTime end
            )
        {
            if (date == null)
            {
                throw new ArgumentNullException("date");
            }

            start = date.Date;
            end = date.Date.AddDays(1).AddMilliseconds(-1);
        }

        public static void Normalize(
            ref DateTime start,
            ref DateTime end
            )
        {
            start = (start < end) ? start : end;
            end = (end > start) ? end : start;
        }

        public static IEnumerable<Range> Split(
            DateTime start,
            DateTime end,
            TimeSpan interval,
            bool overlap = false
            )
        {
            Normalize(ref start, ref end);
            if (interval >= (end - start))
            {
                return new Range[] { Range.Create(start, end) };
            }

            long periodInMillis = (long) (end - start).TotalMilliseconds;
            var ranges = new SortedList<DateTime, Range>();

            DateTime rangeStart = start;
            DateTime rangeFinish;
            long curTimeInMillis = 0;
            while (curTimeInMillis < periodInMillis)
            {
                rangeFinish = rangeStart + interval;
                // Remove a second to avoid overlap
                rangeFinish = (overlap == false) ? rangeFinish.AddMilliseconds(-1) : rangeFinish;
                // Check to make sure we haven't passed the end of the period.
                rangeFinish = (rangeFinish > end) ? end : rangeFinish;
                ranges.Add(rangeStart, Range.Create(rangeStart, rangeFinish));

                curTimeInMillis += (long)((overlap == false) ? 
                    interval.TotalMilliseconds + 1 : interval.TotalMilliseconds);
                rangeStart = (overlap == false) ?
                    rangeFinish.AddMilliseconds(1) : rangeFinish;
            }

            var timeRemaining = periodInMillis - curTimeInMillis;
            if (timeRemaining > 0)
            {
                ranges.Add(rangeStart, Range.Create(rangeStart,
                    rangeStart.AddMilliseconds(timeRemaining)));
            }

            return ranges.Values;
        }

        public static IEnumerable<Range> SplitDay(
            DateTime day,
            TimeSpan interval,
            bool overlap = false
            )
        {
            DateTime start;
            DateTime finish;
            GetDateStartEnd(day, out start, out finish);

            return Split(start, finish, interval, overlap);
        }

        #endregion
    }

    #region Types

    public struct Range
    {
        #region Instance Properties

        public DateTime End
        {
            get
            {
                return _finish;
            }
        }

        public DateTime Start
        {
            get
            {
                return _start;
            }
        }

        public double TotalHours
        {
            get
            {
                return (_finish - _start).TotalHours;
            }
        }

        public double TotalMinutes
        {
            get
            {
                return (_finish - _start).TotalMinutes;
            }
        }

        public double TotalSeconds
        {
            get
            {
                return (_finish - _start).TotalSeconds;
            }
        }

        #endregion

        #region Constructors

        private Range(
            DateTime start,
            DateTime finish
            )
        {
            // Make sure they're in the right order: start comes before finish
            DateTimes.Normalize(ref start, ref finish);
            _start = start;
            _utcStart = _start.ToUniversalTime();
            _finish = finish;
            _utcFinish = _finish.ToUniversalTime();
        }

        #endregion

        #region Class Methods

        public static Range Create(
            DateTime? start = null,
            DateTime? finish = null
            )
        {
            if (start.HasValue == true && finish.HasValue == true)
            {
                return new Range(start.Value, finish.Value);
            }

            start = (start.HasValue == false) ? DateTime.MinValue : start;
            finish = (finish.HasValue == false) ? DateTime.MaxValue : finish;

            return new Range(start.Value, finish.Value);
        }

        public static Range CreateForDate(
            DateTime date
            )
        {
            if (date == null)
            {
                throw new ArgumentNullException("date");
            }

            DateTime start;
            DateTime finish;
            DateTimes.GetDateStartEnd(date, out start, out finish);

            return new Range(start, finish);
        }

        #endregion

        #region Instance Methods

        public bool IsWithin(
            DateTime dateTime,
            bool inclusive = true
            )
        {
            var utcDateTime = dateTime.ToUniversalTime();

            return (inclusive == true) ?
                utcDateTime >= _utcStart && utcDateTime <= _utcFinish :
                utcDateTime > _utcStart && utcDateTime < _utcFinish;
        }

        #endregion

        #region Instance Data

        private DateTime _finish;
        private DateTime _start;
        private DateTime _utcFinish;
        private DateTime _utcStart;

        #endregion
    }

    #endregion
}
