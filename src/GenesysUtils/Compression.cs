﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;

namespace GenUtils.Compression
{
    public static partial class Compression
    {
        #region Methods

        public static void Zip(
            FileInfo source,
            FileInfo file,
            bool overwrite = true
            )
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            else if (file == null)
            {
                throw new ArgumentNullException("file");
            }
            else if (source.Exists == false)
            {
                throw new FileNotFoundException();
            }

            using (var zipFile = ZipFile.Create(file, overwrite))
            {
                zipFile.AddFile(source);
            }
        }

        public static void Zip(
            DirectoryInfo source,
            FileInfo file,
            bool overwrite = true
            )
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            else if (file == null)
            {
                throw new ArgumentNullException("file");
            }
            else if (source.Exists == false)
            {
                throw new FileNotFoundException();
            }

            using (var zipFile = ZipFile.Create(file, overwrite))
            {
                zipFile.AddDirectory(source);
            }
        }

        #endregion
    }

    #region Types

    public sealed class ZipFile : IDisposable
    {
        #region Constructors

        private ZipFile(
            FileStream stream
            )
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            _zipStream = new ZipOutputStream(stream);
        }

        #endregion

        #region Class Methods

        public static ZipFile Create(
            FileInfo file,
            bool overwrite = true
            )
        {
            if (file == null)
            {
                throw new ArgumentNullException("file");
            }
            else if (file.Exists == true && overwrite == false)
            {
                throw new ArgumentException("file already exists");
            }
            else if (file.Exists == true)
            {
                File.Delete(file.FullName);
            }

            return new ZipFile(File.Create(file.FullName));
        }

        #endregion

        #region Instance Methods

        public void AddDirectory(
            string dirPath,
            string rootZipPath = "",
            bool recursive = true,
            string searchPattern = null
            )
        {
            if (dirPath == null)
            {
                throw new ArgumentNullException("dirPath");
            }
            else if (rootZipPath == null)
            {
                throw new ArgumentNullException("rootZipPath");
            }

            this.AddDirectory(new DirectoryInfo(dirPath), rootZipPath, recursive, searchPattern);
        }

        public void AddDirectory(
            DirectoryInfo dir,
            string rootZipPath = "",
            bool recursive = true,
            string searchPattern = null
            )
        {
            if (dir == null)
            {
                throw new ArgumentNullException("dir");
            }
            else if (rootZipPath == null)
            {
            }
            else if (dir.Exists == false)
            {
                throw new FileNotFoundException();
            }

            searchPattern = (String.IsNullOrEmpty(searchPattern) == false) ? searchPattern : "*";
            var searchOption = (recursive == true) ?
                SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;

            foreach (var fileName in Directory.EnumerateFiles(dir.FullName, searchPattern, searchOption))
            {
                var fileZipPath = Path.GetDirectoryName(fileName).Substring(dir.FullName.Length);
                fileZipPath = (fileZipPath.StartsWith(@"\") == true) ? fileZipPath.Substring(1) : fileZipPath;
                if (String.IsNullOrEmpty(rootZipPath) == false)
                {
                    fileZipPath = (String.IsNullOrEmpty(fileZipPath) == false) ?
                        String.Format(@"{0}\{1}", rootZipPath, fileZipPath) : rootZipPath;
                }

                this.AddFile(fileName, fileZipPath);
            }
        }

        public void AddFile(
            string filePath,
            string zipPath = ""
            )
        {
            if (filePath == null)
            {
                throw new ArgumentNullException("filePath");
            }
            else if (zipPath == null)
            {
                throw new ArgumentNullException("zipPath");
            }

            this.AddFile(new FileInfo(filePath), zipPath);
        }

        public void AddFile(
            FileInfo file,
            string zipPath = ""
            )
        {
            if (file == null)
            {
                throw new ArgumentNullException("file");
            }
            else if (zipPath == null)
            {
                throw new ArgumentNullException("zipPath");
            }
            else if (file.Exists == false)
            {
                throw new FileNotFoundException();
            }

            using (var input = file.OpenRead())
            {
                var entryName = (String.IsNullOrEmpty(zipPath) == false) ?
                    String.Format(@"{0}\{1}", zipPath, file.Name) : file.Name;
                var entry = new ZipEntry(entryName);
                entry.DateTime = file.LastWriteTimeUtc;
                _zipStream.PutNextEntry(entry);

                input.WriteTo(_zipStream);
            }
        }

        public void Dispose()
        {
            if (_disposed == true)
            {
                return;
            }

            if (_zipStream.IsFinished == false)
            {
                _zipStream.Finish();
            }
            _zipStream.Close();
        }

        #endregion

        #region Instance Data

        private volatile bool _disposed = false;
        private readonly ZipOutputStream _zipStream;

        #endregion
    }

    #endregion
}
