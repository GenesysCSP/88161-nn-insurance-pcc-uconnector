﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace GenUtils
{
    public static partial class Xml
    {
        #region Methods

        public static bool GetChildValue(
            XElement parent,
            XName childName,
            out string value
            )
        {
            if (parent == null)
            {
                throw new ArgumentNullException("parent");
            }
            else if (childName == null)
            {
                throw new ArgumentNullException("childName");
            }

            value = null;

            var child = parent.Element(childName);
            if (child == null)
            {
                return false;
            }

            value = child.Value;
            return true;
        }

        public static bool GetChildValueAsInt(
            XElement parent,
            XName childName,
            out int value
            )
        {
            value = 0;

            string strValue;
            if (GetChildValue(parent, childName, out strValue) == false)
            {
                return false;
            }

            return Int32.TryParse(strValue, out value);
        }

        public static bool GetChildAttribValue(
            XElement parent,
            XName childName,
            XName attribName,
            out string value
            )
        {
            if (parent == null)
            {
                throw new ArgumentNullException("parent");
            }
            else if (childName == null)
            {
                throw new ArgumentNullException("childName");
            }
            else if (attribName == null)
            {
                throw new ArgumentNullException("attribName");
            }

            var attribValue = from x in parent.Elements(childName)
                              where x.Attribute(attribName) != null
                              select x.Attribute(attribName).Value;
            if (attribValue.Any() == false)
            {
                value = null;
                return false;
            }

            value = attribValue.First();
            return true;
        }

        public static bool GetChildAttribValueAsInt(
            XElement parent,
            XName childName,
            XName attribName,
            out int value
            )
        {
            string attribValue;
            if (GetChildAttribValue(parent, childName, attribName, out attribValue) == false)
            {
                value = 0;
                return false;
            }

            return Int32.TryParse(attribValue, out value);
        }

        #endregion
    }
}
