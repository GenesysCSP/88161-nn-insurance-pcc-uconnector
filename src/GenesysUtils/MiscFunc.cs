﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenUtils
{
    public enum LoggingLevel
    {
        DEBUG,
        INFO,
        WARN,
        ERROR,
        FATAL
    }

    public static partial class Func
    {
        #region Methods

        public static string FormatError(
            string errorMsg,
            Exception cause
            )
        {
            if (cause == null)
            {
                throw new ArgumentNullException("cause");
            }

            return FormatError(errorMsg, cause.Message, cause);
        }

        public static string FormatError(
            string errorMsg,
            string reason,
            Exception cause = null
            )
        {
            if (String.IsNullOrWhiteSpace(errorMsg) == true)
            {
                return String.Empty;
            }
            else if(cause == null && String.IsNullOrWhiteSpace(reason) == true)
            {
                return errorMsg;
            }

            if (String.IsNullOrWhiteSpace(reason) == true && cause != null)
            {
                reason = cause.Message;
            }

            return (cause == null) ? 
                String.Format(MSG_FORMAT, errorMsg, reason) :
                String.Format(MSG_FORMAT_WITH_CAUSE, errorMsg, reason, cause);
        }

        public static IDictionary<string, string> GetArgumentMap(
            string[] args
            )
        {
            if (args == null)
            {
                throw new ArgumentNullException("args");
            }
            else if (args.Length == 0)
            {
                return new Dictionary<string, string>();
            }

            var argMap = new Dictionary<string, string>();
            foreach (var arg in args)
            {
                if (arg.StartsWith("--") == false)
                {
                    argMap[DEFAULT_ARG] = arg;
                    continue;
                }

                var parts = arg.Substring(2).Split('=');
                if (parts.Length > 2)
                {
                    throw new ApplicationException("invalid parameter specified " + arg);
                }

                var name = parts[0].ToLower();
                var value = (parts.Length > 1) ? parts[1] : null;
                value = (String.IsNullOrEmpty(value) == false) ? value : null;

                argMap[name] = value;
            }

            return argMap;
        }

        #endregion

        #region Data

        public const string DEFAULT_ARG = "";
        private const string MSG_FORMAT = "{0} (Reason: {1})";
        private static readonly string MSG_FORMAT_WITH_CAUSE = MSG_FORMAT + Environment.NewLine + "{2}";

        #endregion
    }
}
