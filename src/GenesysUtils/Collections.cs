﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace GenUtils.Collections
{
    #region Types

    public class CompositeEnumerable<T> : IEnumerable<T>
    {
        #region Constructors

        public CompositeEnumerable(
            params IEnumerable<T>[] items
            )
        {
            items.ForEach((item) => { _items.Add(item); });
        }

        public CompositeEnumerable(
            IEnumerable<IEnumerable<T>> items
            )
        {
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }

            items.ForEach((item) => { _items.Add(item); });
        }

        #endregion

        #region Instance Methods

        public IEnumerator<T> GetEnumerator()
        {
            return new CompositeEnumerator<T>(_items);
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return new CompositeEnumerator<T>(_items);
        }

        #endregion

        #region Instance Data

        private readonly IList<IEnumerable<T>> _items = new List<IEnumerable<T>>();

        #endregion
    }

    public class CompositeEnumerator<T> : IEnumerator<T>
    {
        #region Instance Properties

        public T Current
        {
            get
            {
                return _subEnum.Current;
            }
        }

        object System.Collections.IEnumerator.Current
        {
            get
            {
                return _subEnum.Current;
            }
        }

        #endregion

        #region Constructors

        public CompositeEnumerator(
            params IEnumerable<T>[] items
            )
        {
            items.ForEach((item) => { _items.Add(item); });
            _mainEnum = _items.GetEnumerator();
        }

        public CompositeEnumerator(
            IEnumerable<IEnumerable<T>> enums
            )
        {
            if (enums == null)
            {
                throw new ArgumentNullException("enums");
            }

            enums.ForEach((@enum) => { _items.Add(@enum); });
            _mainEnum = _items.GetEnumerator();
        }

        #endregion

        #region Instance Methods

        public void Dispose()
        {
            if (_mainEnum != null)
            {
                _mainEnum.Dispose();
            }

            if (_subEnum != null)
            {
                _subEnum.Dispose();
            }
        }

        public bool MoveNext()
        {
            return this.MoveNextImpl();
        }

        bool System.Collections.IEnumerator.MoveNext()
        {
            return this.MoveNextImpl();
        }

        private bool MoveNextImpl()
        {
            if (_subEnum != null && _subEnum.MoveNext() == true)
            {
                return true;
            }
            else if (_mainEnum.MoveNext() == false)
            {
                return false;
            }

            _subEnum = _mainEnum.Current.GetEnumerator();
            while (_subEnum == null || _subEnum.MoveNext() == false)
            {
                if (_mainEnum.MoveNext() == false)
                {
                    return false;
                }

                _subEnum.Dispose();
                _subEnum = null;
                if (_mainEnum.Current == null)
                {
                    continue;
                }

                _subEnum = _mainEnum.Current.GetEnumerator();
            }

            return true;
        }

        public void Reset()
        {
            this.ResetImpl();
        }

        void System.Collections.IEnumerator.Reset()
        {
            this.ResetImpl();
        }

        private void ResetImpl()
        {
            _mainEnum.Reset();
            _subEnum = null;
        }

        #endregion

        #region Instance Data

        private readonly IList<IEnumerable<T>> _items = new List<IEnumerable<T>>();
        private readonly IEnumerator<IEnumerable<T>> _mainEnum;
        private IEnumerator<T> _subEnum;

        #endregion
    }

    public class EmptyDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        #region Instance Properties

        public int Count
        {
            get 
            {
                return 0;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return true;
            }
        }

        public ICollection<TKey> Keys
        {
            get
            {
                return EMPTY_KEYCOL;
            }
        }

        public ICollection<TValue> Values
        {
            get
            {
                return EMPTY_VALUECOL;
            }
        }

        public TValue this[TKey key]
        {
            get
            {
                throw new KeyNotFoundException();
            }
            set
            {
                throw new NotSupportedException();
            }
        }

        #endregion

        #region Class methods

        public static IDictionary<TKey, TValue> Get()
        {
            return INSTANCE;
        }

        #endregion

        #region Instance Methods

        public void Add(TKey key, TValue value)
        {
            throw new NotSupportedException();
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            throw new NotSupportedException();
        }

        public void Clear()
        {
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return false;
        }

        public bool ContainsKey(TKey key)
        {
            return false;
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return EMPTY_KVPCOL.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return EMPTY_KVPCOL.GetEnumerator();
        }

        public bool Remove(TKey key)
        {
            return false;
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            return false;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            value = default(TValue);
            return false;
        }

        #endregion

        #region Class Data

        private static readonly ICollection<TKey> EMPTY_KEYCOL = new TKey[0];
        private static readonly ICollection<KeyValuePair<TKey, TValue>> EMPTY_KVPCOL = new KeyValuePair<TKey, TValue>[0];
        private static readonly ICollection<TValue> EMPTY_VALUECOL = new TValue[0];
        private static readonly IDictionary<TKey, TValue> INSTANCE = new EmptyDictionary<TKey, TValue>();

        #endregion
    }

    public class QueueDictionary<TKey, TValue>
    {
        #region Instance Properties

        public TValue this[TKey key]
        {
            get
            {
                _lock.EnterReadLock();
                try
                {
                    return _dictionary[key];
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
            set
            {
                _lock.EnterWriteLock();
                try
                {
                    // If a fixed-size is defined and adding this entry will push it over the limit,
                    // the first item in the queue needs to be removed first.
                    if (_fixedSize.HasValue == true && _dictionary.ContainsKey(key) == false &&
                        _dictionary.Count > 0 && _dictionary.Count + 1 > _fixedSize.Value)
                    {
                        var first = _queue.First();
                        _queue.RemoveFirst();
                        _dictionary.Remove(first.Key);
                    }

                    _queue.AddLast(new KeyValuePair<TKey, TValue>(key, value));
                    _dictionary[key] = value;
                }
                finally
                {
                    _lock.ExitWriteLock();
                }
            }
        }

        public bool TryGetValue(
            TKey key,
            out TValue value
            )
        {
            _lock.EnterReadLock();
            try
            {
                return _dictionary.TryGetValue(key, out value);
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }

        #endregion

        #region Constructors

        public QueueDictionary(
            int? fixedSize = null
            )
        {
            _fixedSize = fixedSize;
        }

        #endregion

        #region Instance Data

        private readonly IDictionary<TKey, TValue> _dictionary = new Dictionary<TKey, TValue>();
        private int? _fixedSize;
        private readonly ReaderWriterLockSlim _lock = new ReaderWriterLockSlim();
        private readonly LinkedList<KeyValuePair<TKey, TValue>> _queue = new LinkedList<KeyValuePair<TKey, TValue>>();

        #endregion
    }

    #endregion
}
