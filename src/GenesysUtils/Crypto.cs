﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using Org.BouncyCastle.Bcpg;
using Org.BouncyCastle.Bcpg.OpenPgp;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities.IO;

namespace GenUtils.Crypto
{
    public static partial class Crypto
    {
        public static void DecryptFile(
            string srcPath,
            string destPath,
            string privateKeyPath,
            char[] passPhrase = null
            )
        {
            if (srcPath == null)
            {
                throw new ArgumentNullException("srcPath");
            }
            else if (destPath == null)
            {
                throw new ArgumentNullException("destPath");
            }
            else if (privateKeyPath == null)
            {
                throw new ArgumentNullException("privateKeyPath");
            }

            DecryptFile(new FileInfo(srcPath), new FileInfo(destPath), new FileInfo(privateKeyPath), passPhrase);
        }

        public static void DecryptFile(
            FileInfo srcFile,
            FileInfo destFile,
            FileInfo privateKeyFile,
            char[] passPhrase = null
            )
        {
            if (srcFile == null)
            {
                throw new ArgumentNullException("srcFile");
            }
            else if (srcFile.Exists == false)
            {
                throw new FileNotFoundException("source file not found");
            }
            else if (destFile == null)
            {
                throw new ArgumentNullException("destFile");
            }
            else if (privateKeyFile == null)
            {
                throw new ArgumentNullException("privateKeyFile");
            }
            else if (privateKeyFile.Exists == false)
            {
                throw new FileNotFoundException("private key file not found");
            }

            PgpSecretKeyRingBundle keyRing;
            using (var input = privateKeyFile.OpenRead())
            {
                keyRing = new PgpSecretKeyRingBundle(PgpUtilities.GetDecoderStream(input));
            }

            using (var source = srcFile.OpenRead())
            {
                var pgpFactory = new PgpObjectFactory(PgpUtilities.GetDecoderStream(source));
                var pgpObject = pgpFactory.NextPgpObject();

                // the first object might be a PGP marker packet.
                var encDataList = ((pgpObject is PgpEncryptedDataList) ?
                    pgpObject : pgpFactory.NextPgpObject()) as PgpEncryptedDataList;

                PgpPrivateKey secretKey = null;
                PgpPublicKeyEncryptedData pked = null;

                // Locate the a key and the encrypted data.
                foreach (PgpPublicKeyEncryptedData curPked in encDataList.GetEncryptedDataObjects())
                {
                    secretKey = FindSecretKey(keyRing, curPked.KeyId, passPhrase);
                    if(secretKey != null)
                    {
                        pked = curPked;
                        break;
                    }
                }

                if (secretKey == null)
                {
                    throw new ApplicationException("secret key not found for message");
                }

                PgpObjectFactory objFactory = null;
                using (var clear = pked.GetDataStream(secretKey))
                {
                    objFactory = new PgpObjectFactory(clear);
                }

                var message = objFactory.NextPgpObject();
                if (message is PgpCompressedData)
                {
                    var compData = message as PgpCompressedData;

                    //using (var output = destFile.Create())
                    //{
                    //    compData.GetDataStream().WriteTo(output);
                    //}

                    using (var compInput = compData.GetDataStream())
                    {
                        var newFactory = new PgpObjectFactory(compInput);
                        message = newFactory.NextPgpObject();
                    }
                }
                
                if (message is PgpLiteralData)
                {
                    using(var output = destFile.Create())
                    {
                        var litData = message as PgpLiteralData;
                        litData.GetInputStream().WriteTo(output);
                    }
                }
            }
        }

        public static void EncryptFile(
            string srcPath,
            string destPath,
            string publicKeyPath,
            SymmetricKeyAlgorithmTag algorithm = SymmetricKeyAlgorithmTag.Blowfish
            )
        {
            if (srcPath == null)
            {
                throw new ArgumentNullException("srcPath");
            }
            else if (destPath == null)
            {
                throw new ArgumentNullException("destPath");
            }
            else if (publicKeyPath == null)
            {
                throw new ArgumentNullException("publicKeyPath");
            }

            EncryptFile(new FileInfo(srcPath), new FileInfo(destPath), new FileInfo(publicKeyPath), algorithm);
        }

        public static void EncryptFile(
            FileInfo srcFile,
            FileInfo destFile,
            FileInfo publicKeyFile,
            SymmetricKeyAlgorithmTag algorithm = SymmetricKeyAlgorithmTag.Blowfish
            )
        {
            if (srcFile == null)
            {
                throw new ArgumentNullException("srcFile");
            }
            else if (srcFile.Exists == false)
            {
                throw new FileNotFoundException("source file not found");
            }
            else if (destFile == null)
            {
                throw new ArgumentNullException("destFile");
            }
            else if (publicKeyFile == null)
            {
                throw new ArgumentNullException("publicKeyFile");
            }
            else if (publicKeyFile.Exists == false)
            {
                throw new FileNotFoundException("public key file not found");
            }

            using (var input = srcFile.OpenRead())
            {
                var encryptionKey = ReadEnryptionKey(publicKeyFile);
                var comTempPath = Path.GetTempFileName();

                try
                {
                    // Compress the input file and write it to a temp file, which will be deleted afterwards.
                    using (var comOutput = File.OpenWrite(comTempPath))
                    {
                        var comDataGen = new PgpCompressedDataGenerator(CompressionAlgorithmTag.Zip);
                        PgpUtilities.WriteFileToLiteralData(comDataGen.Open(comOutput), PgpLiteralData.Binary, srcFile);
                        comDataGen.Close();
                    }

                    if (destFile.Exists == true)
                    {
                        destFile.Delete();
                    }

                    // Create a file, which will be the result of the encryption process.
                    using (var output = destFile.OpenWrite())
                    {
                        var encDataGen = new PgpEncryptedDataGenerator(algorithm, true, new SecureRandom());
                        encDataGen.AddMethod(encryptionKey);

                        // Output the compressed input file and write it out using the encrypted data generator.
                        using (var comInput = File.OpenRead(comTempPath))
                        {
                            // Using a buffer of 512KB
                            using (var encOutput = encDataGen.Open(output, Convert.ToInt64(Math.Pow(2, 19))))
                            {
                                comInput.WriteTo(encOutput);
                            }
                        }
                    }
                }
                finally
                {
                    if (File.Exists(comTempPath) == true)
                    {
                        File.Delete(comTempPath);
                    }
                }
            }
        }

        private static PgpPrivateKey FindSecretKey(
            PgpSecretKeyRingBundle keyRing, 
            long keyId,
            char[] passPhrase = null
            )
        {
            if (keyRing == null)
            {
                throw new ArgumentNullException("keyRing");
            }

            var key = keyRing.GetSecretKey(keyId);
            return (key != null) ? key.ExtractPrivateKey(passPhrase) : null;
        }

        public static PgpPublicKey ReadEnryptionKey(
            string publicKeyPath
            )
        {
            if (publicKeyPath == null)
            {
                throw new ArgumentNullException("publicKeyPath");
            }

            return ReadEnryptionKey(new FileInfo(publicKeyPath));
        }

        public static PgpPublicKey ReadEnryptionKey(
            FileInfo publicKeyFile
            )
        {
            if (publicKeyFile == null)
            {
                throw new ArgumentNullException("publicKeyFile");
            }
            else if (publicKeyFile.Exists == false)
            {
                throw new FileNotFoundException();
            }

            using (var pkInput = publicKeyFile.OpenRead())
            {
                var keyRingBundle = new PgpPublicKeyRingBundle(
                    PgpUtilities.GetDecoderStream(pkInput));
                foreach (PgpPublicKeyRing keyRing in keyRingBundle.GetKeyRings())
                {
                    foreach (PgpPublicKey key in keyRing.GetPublicKeys())
                    {
                        if (key.IsEncryptionKey == true)
                        {
                            return key;
                        }
                    }
                }

                throw new ArgumentException("did not find a public encryption key in key ring");
            }
        }


        public static void Decrypt(
            Stream input,
            Stream output,
            PgpSecretKeyRingBundle keyRing,
            char[] passphrase = null
            )
        {
            if (input == null)
            {
                throw new ArgumentNullException("input");
            }
            else if (output == null)
            {
                throw new ArgumentNullException("output");
            }
            else if (keyRing == null)
            {
                throw new ArgumentNullException("keyRing");
            }

            try
            {
                PgpPublicKeyEncryptedData pubKeyEncData;
                var dataStream = GetDecryptedDataStream(input, keyRing, out pubKeyEncData, passphrase);
                var objFactory = new PgpObjectFactory(dataStream);
                var compressedData = objFactory.NextPgpObject() as PgpCompressedData;

                var pgpFactory = new PgpObjectFactory(compressedData.GetDataStream());
                var message = pgpFactory.NextPgpObject();

                if (message is PgpLiteralData)
                {
                    var literaldata = message as PgpLiteralData;
                    literaldata.GetInputStream().WriteTo(output);

                    if (pubKeyEncData.IsIntegrityProtected() == false)
                    {
                        throw new ApplicationException("Message failed integrity check.");
                    }
                }
                else if (message is PgpOnePassSignatureList)
                {
                    throw new ApplicationException("Encrypted message is signed and not literal data");
                }
                else
                {
                    throw new ApplicationException("Message is not a simple encrypted file (type unknown)");
                }
            }
            catch (ApplicationException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new ApplicationException(String.Format(
                    "Encountered an error decrypting data (Reason: {0})", e.Message));
            }
        }

        public static Stream GetDecryptedDataStream(
            Stream input,
            PgpSecretKeyRingBundle keyRing,
            out PgpPublicKeyEncryptedData pubKeyEncData,
            char[] passPhrase = null
            )
        {
            var objFactory = new PgpObjectFactory(PgpUtilities.GetDecoderStream(input));

            var encDataList = objFactory.NextPgpObject() as PgpEncryptedDataList;
            encDataList = (encDataList == null) ?
                objFactory.NextPgpObject() as PgpEncryptedDataList : encDataList;

            pubKeyEncData = null;
            PgpPrivateKey privKey = null;

            foreach (PgpPublicKeyEncryptedData encData in encDataList.GetEncryptedDataObjects())
            {
                var secretKey = keyRing.GetSecretKey(encData.KeyId);
                if (secretKey == null)
                {
                    continue;
                }

                privKey = secretKey.ExtractPrivateKey(passPhrase);
                if (privKey == null)
                {
                    continue;
                }

                pubKeyEncData = encData;
                break;
            }

            if (privKey == null)
            {
                throw new ApplicationException("Unable to find private key for message");
            }

            return pubKeyEncData.GetDataStream(privKey);
        }
    }

    #region Types

    public interface ISymmetricCipher
    {
        string Decrypt(string text);
        string Encrypt(string text);
    }

    public abstract class SymmetricCipherBase : ISymmetricCipher
    {
        public SymmetricCipherBase(
            string key
            )
        {
            if (String.IsNullOrEmpty(key) == true)
            {
                throw (key == null) ?
                    new ArgumentNullException("key") :
                    new ArgumentException("no encryption key specified");
            }

            _key = key;
            _pwdBytes = Encoding.UTF8.GetBytes(_key);
        }

        protected abstract SymmetricAlgorithm CreateCipher();

        public string Decrypt(
            string encryptedText
            )
        {
            if (encryptedText == null)
            {
                return encryptedText;
            }

            var encryptedData = Convert.FromBase64String(encryptedText);
            var keyBytes = new byte[0x10];

            int len = _pwdBytes.Length;
            if (len > keyBytes.Length)
            {
                len = keyBytes.Length;
            }
            Array.Copy(_pwdBytes, keyBytes, len);

            var cipher = this.CreateCipher();
            cipher.Key = keyBytes;
            cipher.IV = keyBytes;

            var plainText = cipher.CreateDecryptor()
                .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
            return Encoding.UTF8.GetString(plainText);
        }

        public string Encrypt(
            string text
            )
        {
            if (text == null)
            {
                return text;
            }

            var keyBytes = new byte[0x10];

            int len = _pwdBytes.Length;
            if (len > keyBytes.Length)
            {
                len = keyBytes.Length;
            }
            Array.Copy(_pwdBytes, keyBytes, len);

            var cipher = this.CreateCipher();
            cipher.Key = keyBytes;
            cipher.IV = keyBytes;

            ICryptoTransform transform = cipher.CreateEncryptor();
            var plainText = Encoding.UTF8.GetBytes(text);
            return Convert.ToBase64String(
                transform.TransformFinalBlock(plainText, 0, plainText.Length));
        }

        private readonly string _key;
        private readonly byte[] _pwdBytes;
    }

    public class BasicPasswordCipher : SymmetricCipherBase
    {
        private const CipherMode Mode = CipherMode.CBC;
        private const PaddingMode Padding = PaddingMode.PKCS7;
        private const int KeySize = 128;
        private const int BlockSize = 128;

        public BasicPasswordCipher()
            : base("pmDKM9QAKOhzRXYQrWL0Oe5ETMWMlX8U3e/y0zS7U4m5aibl2vUO7iDS/PFkkTE2IR9b6feW97rwzHrlZMufFIj/uut68VUNUY9VAIq8Hl+MPgJQpiuwiiKG4B/wuAFh")
        {
        }

        protected override SymmetricAlgorithm CreateCipher()
        {
            return new RijndaelManaged()
            {
                Mode = Mode,
                Padding = Padding,
                KeySize = KeySize,
                BlockSize = BlockSize
            };
        }
    }

    #endregion
}
