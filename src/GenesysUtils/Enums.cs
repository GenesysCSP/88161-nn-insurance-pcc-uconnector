﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenUtils
{
    public static partial class Enums
    {
        #region Methods

        public static IEnumerable<TEnum> GetEnumValues<TEnum>()
        {
            return GetEnumValues<TEnum, TEnum>();
        }

        public static IEnumerable<TResult> GetEnumValues<TEnum, TResult>() 
        {
            var enumType = typeof(TEnum);
            var resultType = typeof(TResult);

            var enumValues = Enum.GetValues(enumType);
            
            if (resultType == typeof(string))
            {
                var stringValues = new List<string>();
                enumValues.ForEach<Object>(
                    (idx, value) => { stringValues.Add(value.ToString()); });
                return (IEnumerable<TResult>) stringValues;
            }

            var resultValues = new TResult[enumValues.Length];
            enumValues.ForEach<TResult>(
                (idx, value) => { resultValues[idx] = (TResult)value; });

            return resultValues;
        }

        #endregion
    }
}
