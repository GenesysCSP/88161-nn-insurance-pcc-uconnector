﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GenUtils;

namespace Genesys.SpeechMiner.WS
{
    public abstract class CallsClientBase : ICallsClient
    {
        #region Instance Properties

        protected string SessionToken
        {
            get
            {
                return _sessionToken;
            }
            set
            {
                _sessionToken = value;
            }
        }

        public Version SMVersion
        {
            get
            {
                return _smVersion;
            }
        }

        public TimeSpan? Timeout
        {
            get
            {
                return _timeout;
            }
            set
            {
                _timeout = this.OnSetTimeout(value);
            }
        }

        public Uri URI
        {
            get
            {
                return _uri;
            }
        }

        #endregion

        #region Constructors

        protected CallsClientBase(
            Uri wsUri,
            Version smVersion,
            TimeSpan? timeout = null
            )
        {
            if (wsUri == null)
            {
                throw new ArgumentNullException("wsUri");
            }
            else if (smVersion == null)
            {
                throw new ArgumentNullException("version");
            }

            _uri = wsUri;
            _smVersion = smVersion;
            _timeout = timeout;
        }

        #endregion

        #region Instance Methods

        public void GetTranscript(
            int callId,
            ref IDictionary<AudioChannel, string> transcriptData,
            params AudioChannel[] channels
            )
        {
            if (transcriptData == null)
            {
                transcriptData = new Dictionary<AudioChannel, string>();
            }
            else
            {
                transcriptData.Clear();
            }

            channels = (channels.Length == 0) ? DEFAULT_TRANSCRIPT_CHANNELS : channels;
            foreach (var channel in channels)
            {
                transcriptData[channel] = String.Empty;
            }

            var entries = this.GetContent(callId, EntryType.LVCSR, EntryType.Topic);
            if (entries.Any() == false)
            {
                return;
            }

            lock (_bufferLock)
            {
                this.InitTranscriptBuffers();

                for (int idx = 0; idx < entries.Length; idx++)
                {
                    bool openTopicEvent = false;
                    bool closeTopicEvent = false;
                    var entry = entries[idx];

                    if (String.IsNullOrEmpty(entry.Tooltip) == false)
                    {
                        openTopicEvent = idx > 0 && entry.Tooltip.Equals(entries[idx - 1].Tooltip) == false;
                        closeTopicEvent = (idx == entries.Length - 1 ||
                            entry.Tooltip.Equals(entries[idx + 1].Tooltip) == false);
                    }

                    _buffer.Append((openTopicEvent == true) ?
                        String.Format("<{0}>", entry.Tooltip) : String.Empty);

                    _buffer.Append(entry.Text);

                    _buffer.Append((closeTopicEvent == true) ?
                        String.Format("</{0}>", entry.Tooltip) : " ");

                    StringBuilder channelBuffer;
                    if (_channelBuffers.TryGetValue(entry.Channel, out channelBuffer) == true)
                    {
                        channelBuffer.Append(_buffer);
                    }

                    _buffer.Length = 0;
                }

                foreach (var channel in channels)
                {
                    transcriptData[channel] = _channelBuffers[channel].ToString().Trim();
                }

                this.ResetTranscriptBuffers();
            }
        }

        public abstract ContentEntry[] GetContent(int callid, params EntryType[] entryTypes);

        private void InitTranscriptBuffers()
        {
            lock (_bufferLock)
            {
                if (_buffer == null || _channelBuffers == null)
                {
                    _buffer = new StringBuilder(512);
                    _channelBuffers = new Dictionary<AudioChannel, StringBuilder>();
                    Enums.GetEnumValues<AudioChannel>().ForEach(
                        channel => _channelBuffers[channel] = new StringBuilder(512));
                }
                else
                {
                    this.ResetTranscriptBuffers();
                }
            }
        }

        public abstract void Login(string userName, string password, AuthenticationType authType);

        public abstract void Logout();

        protected virtual TimeSpan? OnSetTimeout(
            TimeSpan? timeout
            )
        {
            return null;
        }

        private void ResetTranscriptBuffers()
        {
            lock (_bufferLock)
            {
                if (_buffer != null)
                {
                    _buffer.Length = 0;
                }

                if (_channelBuffers != null)
                {
                    _channelBuffers.Values.ForEach(buffer => buffer.Length = 0);
                }
            }
        }

        #endregion

        #region Class Data

        private readonly static AudioChannel[] DEFAULT_TRANSCRIPT_CHANNELS = new AudioChannel[] 
            { AudioChannel.Left, AudioChannel.Right };

        #endregion

        #region Instance Data

        // Reusable buffers for generating call transcripts to avoid creating a lot of disposable
        // objects when generating transcripts for a large number of calls.
        private StringBuilder _buffer;
        private readonly object _bufferLock = new object();
        private IDictionary<AudioChannel, StringBuilder> _channelBuffers;

        private string _sessionToken;
        private readonly Version _smVersion;
        private TimeSpan? _timeout;
        private readonly Uri _uri;        

        #endregion
    }
}
