﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Genesys.SpeechMiner.Database.Objects
{
    public interface IDBObject
    {
        #region Properties

        int ID { get; set; }

        #endregion
    }
}
