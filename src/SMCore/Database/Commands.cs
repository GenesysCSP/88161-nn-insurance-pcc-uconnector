﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Genesys.SpeechMiner.Database
{
    public static class Commands
    {
        public static SqlCommand AgentHierarchyUpdate(
            SqlCommand dbCmd,
            string sourceTable
            )
        {
            if (dbCmd == null)
            {
                throw new ArgumentNullException("dbCmd");
            }
            if (sourceTable == null)
            {
                throw new ArgumentNullException("sourceTable");
            }

            dbCmd.CommandText = "EXEC sp_updateAgentHierarchy @SourceTable";
            dbCmd.Parameters.AddWithValue("@SourceTable", sourceTable);
            return dbCmd;
        }

        public static SqlCommand ComponentVersion(
            SqlCommand dbCmd,
            string resource = "SM"
            )
        {
            if (dbCmd == null)
            {
            }
            else if (resource == null)
            {
                throw new ArgumentNullException("resource");
            }

            resource = resource.Trim();
            if (String.IsNullOrEmpty(resource) == true)
            {
                throw new ArgumentException("resource not specified");
            }

            dbCmd.CommandText = "SELECT Version FROM versionTbl WHERE resource = @Resource";
            dbCmd.Parameters.AddWithValue("@Resource", resource);
            return dbCmd;
        }

        public static SqlCommand InternalAgentID(
            SqlCommand dbCmd,
            string externalId,
            bool dbSupportsMultiExtIds = false
            )
        {
            if (dbCmd == null)
            {
                throw new ArgumentNullException("dbCmd");
            }
            else if (externalId == null)
            {
                throw new ArgumentNullException("externalId");
            }
            
            string sql = @"SELECT id FROM agentEntityTbl 
                           WHERE externalId = @ExternalID";
            if (dbSupportsMultiExtIds == true)
            {
                sql = @"SELECT id FROM agentEntityTbl
                        WHERE externalId = (SELECT smExternalId 
                                            FROM agentExternalIds 
                                            WHERE externalId = @ExternalID)";
            }

            dbCmd.CommandText = sql;

            var param = dbCmd.CreateParameter();
            param.ParameterName = "@ExternalID";
            param.Value = externalId;
            dbCmd.Parameters.Add(param);

            return dbCmd;
        }
    }
}
