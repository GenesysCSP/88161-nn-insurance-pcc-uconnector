﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Genesys.SpeechMiner.Objects;
using Genesys.SpeechMiner.Agents;
using System.Data.SqlClient;
using System.Data.Common;

namespace Genesys.SpeechMiner.Database
{
    public interface ISMDatabase : IDisposable
    {
        #region Properties

        bool IsConnected { get; }
        Version Version { get; }

        #endregion

        #region Methods

        SqlTransaction BeginTransaction();

        void Connect();

        void Disconnect();

        Call GetCall(
            int id,
            KeyValuePair<string, string>? withMeta = null,
            bool includeMetaData = false,
            IEnumerable<string> metaFields = null,
            SqlTransaction transxn = null);

        //IEnumerable<KeyValuePair<string, string>> GetCallMetaData(
        //    int id,
        //    IEnumerable<string> fields = null
        //    );

        //IEnumerable<IEnumerable<KeyValuePair<string, string>>> GetCallMetaData(
        //    IEnumerable<int> idSet,
        //    IEnumerable<string> fields = null
        //    );

        IEnumerable<Call> GetCalls(
            IEnumerable<int> idSet = null,
            long? limitTo = null, 
            int? programId = null,
            KeyValuePair<string, string>? withMeta = null,
            bool includeMetaData = false,
            IEnumerable<string> metaFields = null,
            SqlTransaction transxn = null);

        IEnumerable<Call> GetCalls(
            DateTime date, 
            IEnumerable<int> idSet = null,
            long? limitTo = null, 
            int? programId = null,
            KeyValuePair<string, string>? withMeta = null,
            bool includeMetaData = false,
            IEnumerable<string> metaFields = null,
            SqlTransaction transxn = null);

        IEnumerable<Call> GetCalls(
            DateTime startTime, 
            DateTime endTime, 
            IEnumerable<int> idSet = null,
            long? limitTo = null, 
            int? programId = null,
            KeyValuePair<string, string>? withMeta = null,
            bool includeMetaData = false,
            IEnumerable<string> metaFields = null,
            SqlTransaction transxn = null);

        int GetInternalAgentID(
            string externalId,
            SqlTransaction transxn = null);

        string GetPrimaryAgentID(
            string externalId,
            SqlTransaction transxn = null);

        Program GetProgram(
            int id,
            SqlTransaction transxn = null);

        Program GetProgram(
            string name,
            SqlTransaction transxn = null);

        IEnumerable<Program> GetPrograms(
            IEnumerable<int> idSet = null,
            IEnumerable<string> nameSet = null,
            SqlTransaction transxn = null);

        Version GetVersion(
            string resource = "SM",
            SqlTransaction transxn = null);
        
        bool HasTable(
            string tableName,
            SqlTransaction transxn = null);

        int Modify(
            string sql,
            SqlTransaction transxn = null);

        DataTable Query(
            string sql,
            SqlTransaction transxn = null,
            CommandBehavior cmdBehavior = CommandBehavior.Default);

        T QueryScalar<T>(
            string sql,
            T defaultValue = default(T),
            SqlTransaction transxn = null);

        // Only temporary. Getting in the way of finishing an unrelated update.
        //T Retrieve<T>(
        //    IDBQuery query
        //    ) where T : IDBObject;

        //IEnumerable<T> RetrieveMultiple<T>(
        //    IDBQuery query
        //    ) where T : IDBObject;

        void UpdateAgentHierarchy(
            Hierarchy hierarchy,
            bool supportForMultiExtIds = false,
            SqlTransaction transxn = null);

        #endregion
    }
}
