﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Data;
using GenUtils;
using Genesys.SpeechMiner.Objects;

namespace Genesys.SpeechMiner.Database
{
    public sealed partial class SMDatabase : SMDatabaseBase
    {
        #region Constructors

        private SMDatabase(
            SqlConnection dbConn,
            Version version,
            ISMDatabase smdbImpl
            ) : base(dbConn, version)
        {
            if (smdbImpl == null)
            {
                throw new ArgumentNullException("smdbImpl");
            }

            _smdbImpl = smdbImpl;
        }

        #endregion

        #region Class Methods

        public static ISMDatabase Connect(
            string server,
            string dbName,
            string userName = null,
            string password = null
            )
        {
            if (server == null)
            {
                throw new ArgumentNullException("server");
            }
            else if (dbName == null)
            {
                throw new ArgumentNullException("dbName");
            }

            var connString = new SqlConnectionStringBuilder();
            connString.DataSource = server;
            connString.InitialCatalog = dbName;
            connString.MultipleActiveResultSets = true;

            if (String.IsNullOrEmpty(userName) == false)
            {
                connString.UserID = userName;
                connString.Password = password;
            }
            else
            {
                connString.IntegratedSecurity = true;
            }

            return SMDatabase.Connect(connString.ToString());
        }

        public static ISMDatabase Connect(
            string connString
            )
        {
            if (connString == null)
            {
                throw new ArgumentNullException("connString");
            }

            connString = connString.Trim();
            if (connString.Length == 0)
            {
                throw new ArgumentException("no connection string specified");
            }

            return Connect(new SqlConnection(connString));
        }

        public static ISMDatabase Connect(
            SqlConnection dbConn
            )
        {
            if (dbConn == null)
            {
                throw new ArgumentNullException("dbConn");
            }

            dbConn.Open();

            //TODO: Choose the appropriate implementation based on version.
            var version = SMDatabase.GetVersion(dbConn);

            return new SMDatabaseDefault(dbConn, version);
        }

        #endregion

        #region Instance Methods

        public new void Dispose()
        {
            base.Dispose();
            _smdbImpl.Dispose();
        }

        public override IEnumerable<Call> GetCalls(
            IEnumerable<int> idSet = null,
            long? limitTo = null,
            int? programId = null,
            KeyValuePair<string, string>? withMeta = null,
            bool includeMetaData = false,
            IEnumerable<string> metaFields = null,
            SqlTransaction transxn = null
            )
        {
            return _smdbImpl.GetCalls(idSet, limitTo, programId, withMeta,
                includeMetaData, metaFields, transxn);
        }

        public override IEnumerable<Call> GetCalls(
            DateTime startTime,
            DateTime endTime,
            IEnumerable<int> idSet = null,
            long? limitTo = null,
            int? programId = null,
            KeyValuePair<string, string>? withMeta = null,
            bool includeMetaData = false,
            IEnumerable<string> metaFields = null,
            SqlTransaction transxn = null
            )
        {
            return _smdbImpl.GetCalls(startTime, endTime, idSet, limitTo, programId, 
                withMeta, includeMetaData, metaFields, transxn);
        }

        public override IEnumerable<Program> GetPrograms(
            IEnumerable<int> idSet = null,
            IEnumerable<string> nameSet = null,
            SqlTransaction transxn = null
            )
        {
            return _smdbImpl.GetPrograms(idSet, nameSet, transxn);
        }

        #endregion

        #region Instance Data

        private readonly ISMDatabase _smdbImpl;

        #endregion
    }
}
