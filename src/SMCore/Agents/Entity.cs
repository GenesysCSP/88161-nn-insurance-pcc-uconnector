﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GenUtils;
using System.Threading;
using Wintellect.PowerCollections;

namespace Genesys.SpeechMiner.Agents
{
    public sealed class Entity : IEnumerable<Entity>, IEquatable<Entity>
    {
        #region Instance Properties

        public string ID
        {
            get
            {
                return _id;
            }
        }

        public IEnumerable<string> IDs
        {
            get
            {
                return _ids;
            }
        }

        public int? InternalID;

        public string Name
        {
            get
            {
                return _name;
            }
        }

        #endregion

        #region Constructors

        internal Entity(
            string id,
            string name,
            IEnumerable<string> ids
            )
        {
            if (id == null)
            {
                throw new ArgumentNullException("id");
            }
            else if (ids == null)
            {
                throw new ArgumentNullException("ids");
            }
            else if (name == null)
            {
                throw new ArgumentNullException("name");
            }

            _id = id;
            _ids = ids;
            _name = name;
        }

        #endregion

        #region Class Methods

        public static Entity Create(
            string id,
            string name,
            IEnumerable<string> extIds = null,
            EntityCache cache = null
            )
        {
            if (id == null)
            {
                throw new ArgumentNullException("id");
            }
            else if (name == null)
            {
                throw new ArgumentNullException("name");
            }

            id = id.Trim();
            if (String.IsNullOrEmpty(id) == true)
            {
                throw new ArgumentException("no unique id specified");
            }

            name = name.Trim();
            if (String.IsNullOrEmpty(name) == true)
            {
                name = id;
            }

            if (cache == null)
            {
                if (extIds != null && extIds.Any() == true)
                {
                    var temp = new HashSet<string>();
                    temp.Add(id);
                    extIds.ForEach((extId) => {
                        temp.Add(extId); 
                    });
                    extIds = temp;
                }
                else
                {
                    extIds = new string[] { id };
                }

                return new Entity(id, name, extIds);
            }
            else
            {
                return cache.GetOrCreateEntity(id, name, extIds);
            }
        }

        public static bool Equals(
            Entity entity,
            string id,
            string name,
            IEnumerable<string> extIds
            )
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            else if (id == null)
            {
                throw new ArgumentNullException("id");
            }
            else if (name == null)
            {
                throw new ArgumentNullException("name");
            }
            else if (extIds == null)
            {
                throw new ArgumentNullException("extIds");
            }

            if (extIds.Equals(entity.ID) == false || name.Equals(entity.Name) == false || 
                extIds.Count() != entity.IDs.Count())
            {
                return false;
            }

            foreach (var extId in extIds)
            {
                if (entity.IDs.Contains(extId) == false)
                {
                    return false;
                }
            }

            return true;
        }


        #endregion

        #region Instance Methods

        internal bool Add(
            Entity child
            )
        {
            if (child == null)
            {
                throw new ArgumentNullException("child");
            }
            else
            {
                foreach (var id in child.IDs)
                {
                    if (_ids.Contains(id) == true)
                    {
                        throw new ArgumentException("cannot add entity to itself as a child");
                    }
                }
            }

            if (_children.ContainsKey(child.ID) == true)
            {
                return false;
            }

            _children.Add(child.ID, child);
            return true;
        }

        internal int Add(
            IEnumerable<Entity> children
            )
        {
            if (children == null)
            {
                throw new ArgumentNullException("children");
            }

            int count = 0;
            foreach (var child in children)
            {
                count += (this.Add(child) == true) ? 1 : 0;
            }

            return count;
        }

        internal void CopyChildren(
            Entity entity
            )
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            _children.Clear();
            _children.AddMany(entity._children);
        }

        public bool Equals(
            Entity other
            )
        {
            return Equals(this, other.ID, other.Name, other.IDs);
        }

        public IEnumerator<Entity> GetEnumerator()
        {
            return _children.Values.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _children.Values.GetEnumerator();
        }

        internal void Update(
            Entity other
            )
        {
            if (other == null)
            {
                throw new ArgumentNullException("other");
            }

            this.Update(other.ID, other.Name, other.IDs);
        }

        internal void Update(
            string id,
            string name,
            IEnumerable<string> ids
            )
        {
            if (id == null)
            {
                throw new ArgumentNullException("id");
            }
            else if (name == null)
            {
                throw new ArgumentNullException("name");
            }
            else if (ids == null)
            {
                throw new ArgumentNullException("extIds");
            }

            _id = id;
            if (ids.Count() > 1)
            {
                _ids = new HashSet<string>(ids);
            }
            else
            {
                _ids = new string[] { id };
            }
            _name = name;
        }

        #endregion

        #region Instance Data

        private readonly OrderedDictionary<string, Entity> _children = new OrderedDictionary<string, Entity>((x, y) => { return x.CompareTo(y); });
        private IEnumerable<string> _ids;
        private string _id;
        private string _name;

        #endregion
    }
}
