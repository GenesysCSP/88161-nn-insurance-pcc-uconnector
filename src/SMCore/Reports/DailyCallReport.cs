﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GenUtils;
using Wintellect.PowerCollections;
using System.Xml;
using System.Globalization;
using System.IO;
using Genesys.SpeechMiner.Objects;
using GenUtils.Collections;
using GenUtils.DateTimes;

namespace Genesys.SpeechMiner.Reports
{
    public class DailyCallReport : IEnumerable<PeriodCallReport>
    {
        #region Instance Properties

        public int CallCount
        {
            get
            {
                int totalCallCount = 0;
                this.ForEach((report) => { totalCallCount += report.CallCount; });
                return totalCallCount;
            }
        }

        public IEnumerable<Call> Calls
        {
            get
            {
                var temp = new List<IEnumerable<Call>>();
                _periodReports.ForEach((report) => { temp.Add(report); });
                return new CompositeEnumerable<Call>(temp);
            }
        }

        public DateTime Generated
        {
            get
            {
                return _generated;
            }
        }

        public DateTime Date
        {
            get
            {
                return _date;
            }
        }

        public Program Program
        {
            get
            {
                return _program;
            }
        }

        #endregion

        #region Constructors

        private DailyCallReport(
            DateTime date,
            IEnumerable<PeriodCallReport> periodReports,
            Program program,
            DateTime generated
            )
        {
            if (periodReports == null)
            {
                throw new ArgumentNullException("timePeriods");
            }
            else if (periodReports.Any() == false)
            {
                throw new ArgumentException("no time periods specified");
            }

            _date = date;
            _generated = generated;
            _periodReports.AddMany(periodReports);
            _program = program;
        }

        #endregion

        #region Class Methods

        public static DailyCallReport Create(
            DateTime date,
            TimeSpan? interval = null,
            Program program = null,
            DateTime? generated = null
            )
        {
            generated = (generated.HasValue == false) ? DateTime.Now : generated;
            interval = (interval.HasValue == false) ? TimeSpan.FromDays(1) : interval;

            var timePeriodCallReports = new List<PeriodCallReport>();
            DateTimes.SplitDay(date, interval.Value).ForEach(timePeriod =>
                {
                    timePeriodCallReports.Add(new PeriodCallReport(timePeriod));
                });

            return new DailyCallReport(date, timePeriodCallReports, program, generated.Value);
        }

        #endregion

        #region Instance Methods

        public long AddCalls(
            IEnumerable<Call> calls
            )
        {
            if (calls == null)
            {
                throw new ArgumentNullException("calls");
            }
            else if (calls.Any() == false)
            {
                return 0;
            }

            long callsAdded = 0;
            calls.ForEach((call) => 
            {
                Func<PeriodCallReport, bool> addToPeriodReport = (periodReport) =>
                {
                    if (periodReport.Period.IsWithin(call.StartTime) == false)
                    {
                        return true;
                    }

                    callsAdded += (periodReport.AddCall(call) == true) ? 1 : 0;
                    return false;
                };

                _periodReports.ForEach(addToPeriodReport);
            });

            return callsAdded;
        }

        public IEnumerator<PeriodCallReport> GetEnumerator()
        {
            return _periodReports.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _periodReports.GetEnumerator();
        }

        public void WriteAsXml(
            XmlWriter writer,
            bool writeDoc = true,
            string dateFormat = DateTimes.DATE_FORMAT,
            string timeFormat = DateTimes.TIME_FORMAT,
            string stylesheet = null
            )
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            if (writeDoc == true)
            {
                writer.WriteStartDocument(true);
                if (String.IsNullOrEmpty(stylesheet) == false)
                {
                    writer.WriteProcessingInstruction("xml-stylesheet",
                        String.Format("type=\"text/xsl\" href=\"{0}\"", stylesheet));
                }
            }

            try
            {
                writer.WriteStartElement("CallReport");
                if (this.Program != null)
                {
                    writer.WriteAttributeString("program", this.Program.ToString());
                }

                writer.WriteAttributeString("call-count", this.CallCount.ToString());
                if (this.Any() == false)
                {
                    return;
                }

                var dateTimeFormat = String.Format("{0} {1}", dateFormat, timeFormat);
                writer.WriteAttributeString("date", this.Date.ToString(dateFormat));
                writer.WriteAttributeString("generated", this.Generated.ToString(dateTimeFormat));
                writer.WriteAttributeString("start", this.First().Period.Start.ToString(dateTimeFormat));
                writer.WriteAttributeString("end", this.Last().Period.End.ToString(dateTimeFormat));

                // Write out each of the period call reports
                _periodReports.ForEach((report) => { report.WriteAsXml(writer, timeFormat, false); });
            }
            finally
            {
                writer.WriteEndElement();
            }

            if (writeDoc == true)
            {
                writer.WriteEndDocument();
            }
        }

        public void WriteCallDataAsCSV(
            StreamWriter writer,
            IEnumerable<string> metaFields = null,
            IDictionary<string, string> metaFieldHeaders = null,
            bool ouputLocalTZ = false,
            string separator = ","
            )
        {
            Call.WriteCSVHeader(writer, metaFields: metaFields, metaFieldHeaders: metaFieldHeaders, separator: separator);
            this.Calls.ForEach(call =>
                call.WriteAsCSV(writer, metaFields: metaFields, outputLocalTZ: ouputLocalTZ, separator: separator));
        }

        #endregion

        #region Instance Data

        private readonly DateTime _date;
        private readonly DateTime _generated;
        private readonly OrderedBag<PeriodCallReport> _periodReports = new OrderedBag<PeriodCallReport>((x, y) =>
            { return x.Period.Start.CompareTo(y.Period.Start); });
        private readonly Program _program;

        #endregion
    }
}
