﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GenUtils;
using Wintellect.PowerCollections;
using System.Xml;
using System.Globalization;
using Genesys.SpeechMiner.Objects;
using GenUtils.DateTimes;

namespace Genesys.SpeechMiner.Reports
{
    public class PeriodCallReport : IEnumerable<Call>
    {
        #region Instance Properties

        public int CallCount
        {
            get
            {
                return _calls.Count;
            }
        }

        public Range Period
        {
            get
            {
                return _period;
            }
        }

        #endregion

        #region Constructors

        public PeriodCallReport(
            Range timePeriod
            )
        {
            if (timePeriod.TotalSeconds == 0)
            {
                throw new ArgumentException("time period is empty");
            }

            _period = timePeriod;
        }

        #endregion

        #region Instance Methods

        public bool AddCall(
            Call call
            )
        {
            if (call == null)
            {
                throw new ArgumentException("call");
            }
            else if (this.Period.IsWithin(call.StartTime) == false)
            {
                throw new ArgumentException("call does not fall within time period");
            }

            if (_callIndex.ContainsKey(call.ID) == true)
            {
                return false;
            }

            _callIndex.Add(call.ID, call);
            _calls.Add(call);
            return true;
        }

        public IEnumerator<Call> GetEnumerator()
        {
            return _calls.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public void WriteAsXml(
            XmlWriter writer,
            string timeFormat = DateTimes.DATETIME_FORMAT,
            bool writeDoc = true
            )
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            if (writeDoc == true)
            {
                writer.WriteStartDocument(true);
            }

            try
            {
                writer.WriteStartElement("PeriodCallReport");
                writer.WriteAttributeString("call-count", this.CallCount.ToString());
                writer.WriteAttributeString("start", this.Period.Start.ToString(timeFormat));
                writer.WriteAttributeString("end", this.Period.End.ToString(timeFormat));
            }
            finally
            {
                writer.WriteEndElement();
            }

            if (writeDoc == true)
            {
                writer.WriteEndDocument();
            }
        }

        #endregion

        #region Instance Data

        private readonly IDictionary<int, Call> _callIndex = new Dictionary<int, Call>();
        private readonly OrderedBag<Call> _calls = new OrderedBag<Call>((x, y) => 
            { return x.StartTime.CompareTo(y.StartTime); });
        private Range _period;

        #endregion
    }
}
