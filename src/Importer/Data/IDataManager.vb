﻿Namespace Data
    ''' <summary>
    ''' Manages list of interactions to be imported, and gets their data
    ''' </summary>
    ''' <remarks></remarks>
    Public Interface IDataManager

        ''' <summary>
        ''' Retrieves the next set of interactions to process
        ''' </summary>
        ''' <param name="limitTo">The maximum number of interactions to retrieve.</param>
        ''' <returns>An enumerable collection containing the interaction data.</returns>
        Function GetNextInteractions(ByVal limitTo As Integer) As IEnumerable(Of IInteractionData)

        ''' <summary>
        ''' Determines if interactions are available to be processed.
        ''' </summary>
        ''' <returns>True if there are; otherwise, False</returns>
        Function IxnsAvailable() As Boolean

        ''' <summary>
        ''' Callback for when one interaction is handled.
        ''' </summary>
        ''' <param name="interaction">The interaction data</param>
        ''' <param name="status">Success/failure information</param>
        Sub InteractionHandled(ByVal interaction As IInteractionData, ByVal status As IItemStatus)
    End Interface
End Namespace