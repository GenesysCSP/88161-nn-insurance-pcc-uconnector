﻿Namespace Data
  ''' <summary>
  ''' Utility class for data operations
  ''' </summary>
  ''' <remarks></remarks>
  Public NotInheritable Class DataUtils

    Private Sub New()
    End Sub

    ''' <summary>
    ''' Parse a CSV (comma-separated-values) line into its fields
    ''' </summary>
    ''' <param name="line">The line to parse</param>
    ''' <returns>An array of the field values</returns>
    ''' <remarks></remarks>
    Public Shared Function ParseCSVLine(ByVal line As String) As String()
      Dim csvRegEx As Text.RegularExpressions.Regex = New Text.RegularExpressions.Regex(",(?=(?:[^\""]*\""[^\""]*\"")*(?![^\""]*\""))")
      Dim fields As String() = csvRegEx.Split(line)
      For i As Integer = 0 To fields.Count - 1
        Dim field As String = fields(i)
        If field.Length > 1 AndAlso field.First = """" AndAlso field.Last = """" Then
          fields(i) = field.Substring(1, field.Length - 2)
        End If
      Next
      Return fields
    End Function
  End Class
End Namespace
