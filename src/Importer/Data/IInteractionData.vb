﻿Namespace Data
    ''' <summary>
    ''' Data for an interaction
    ''' </summary>
    ''' <remarks></remarks>
    Public Interface IInteractionData
        ''' <summary>
        ''' The interaction ID
        ''' </summary>
        ''' <value>Interaction ID</value>
        ''' <returns>Interaction ID</returns>
        ''' <remarks></remarks>
        ReadOnly Property ID As String
        ''' <summary>
        ''' Accessor for the interaction's properties
        ''' </summary>
        ''' <param name="name">The name of the property</param>
        ''' <value>The value of the property</value>
        ''' <returns>The value of the property</returns>
        ''' <remarks></remarks>
        Default Property Value(name As String) As String
        ''' <summary>
        ''' The path to the file containing the interaction content (e.g. audio)
        ''' </summary>
        ''' <value>Path to the content file</value>
        ''' <returns>Path to the content file</returns>
        ''' <remarks></remarks>
        Property ContentFilePath As String
        ''' <summary>
        ''' The path to the file containing the interaction metadata
        ''' </summary>
        ''' <value>Path to the metadata file</value>
        ''' <returns>Path to the metadata file</returns>
        ''' <remarks></remarks>
        Property MetaFilePath As String
        ''' <summary>
        ''' The date and time when the interaction started
        ''' </summary>
        ''' <value>Date and time of the interaction</value>
        ''' <returns>Date and time of the interaction</returns>
        ''' <remarks></remarks>
        Property StartTime As DateTime
        ''' <summary>
        ''' The ID of the SpeechMiner program that should handle the interaction
        ''' </summary>
        ''' <value>Program's external ID</value>
        ''' <returns>Program's external ID</returns>
        ''' <remarks></remarks>
        Property Program As String
        ''' <summary>
        ''' The speakers who participated in the interaction
        ''' </summary>
        ''' <value>A collection of the speakers</value>
        ''' <returns>A collection of the speakers</returns>
        ''' <remarks></remarks>
        Property Speakers As ICollection(Of Speaker)
        ''' <summary>
        ''' The ID of the agent in the interaction
        ''' </summary>
        ''' <value>Agent ID</value>
        ''' <remarks></remarks>
        Property AgentID As String
        ''' <summary>
        ''' The workgroup of the agent in the interaction
        ''' </summary>
        ''' <value>workgroup</value>
        ''' <remarks></remarks>
        Property Workgroup As String
    End Interface
End Namespace