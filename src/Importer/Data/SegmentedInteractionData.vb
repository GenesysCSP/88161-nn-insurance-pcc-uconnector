﻿Namespace Data
    Public Class SegmentedInteractionData
        Implements ISegmentedInteractionData

        ''' <summary>
        ''' Create a new segmented interaction data object
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            m_interactionContentFiles = New List(Of ISegmentedContentFile)
            m_interactionMetaFiles = New List(Of IAudioInteractionData)
        End Sub

        ''' <summary>
        '''  A collection of the segmented interaction data files
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_interactionMetaFiles As List(Of IAudioInteractionData)
        ''' <summary>
        '''  A collection of the segmented interaction content files
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_interactionContentFiles As List(Of ISegmentedContentFile)

        ''' <summary>
        ''' Get a list of the segmented interaction content files.
        ''' </summary>
        ''' <returns>A collection of the segmented interaction content files</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ContentFiles As IList(Of ISegmentedContentFile) Implements ISegmentedInteractionData.ContentFiles
            Get
                If m_interactionContentFiles Is Nothing Then
                    m_interactionContentFiles = New List(Of ISegmentedContentFile)()
                End If
                m_interactionContentFiles.Sort(New contentFilesComparer())
                Return m_interactionContentFiles
            End Get
        End Property
        ''' <summary>
        ''' Get a list of the segmented interaction data files.
        ''' </summary>
        ''' <returns>A collection of the segmented interaction data files</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property InteractionMetaFiles() As IList(Of IAudioInteractionData) Implements ISegmentedInteractionData.InteractionMetaFiles
            Get
                If m_interactionMetaFiles Is Nothing Then
                    m_interactionMetaFiles = New List(Of IAudioInteractionData)()
                End If
                m_interactionMetaFiles.Sort(New InteractionDataComparer())
                Return m_interactionMetaFiles
            End Get
        End Property

        Private Class contentFilesComparer
            Implements IComparer(Of ISegmentedContentFile)

            Public Function Compare(ByVal x As ISegmentedContentFile, ByVal y As ISegmentedContentFile) As Integer Implements System.Collections.Generic.IComparer(Of ISegmentedContentFile).Compare
                Return Date.Compare(x.StartTime, y.StartTime)
            End Function
        End Class

        Private Class InteractionDataComparer
            Implements IComparer(Of IAudioInteractionData)

            Public Function Compare(ByVal x As IAudioInteractionData, ByVal y As IAudioInteractionData) As Integer Implements System.Collections.Generic.IComparer(Of IAudioInteractionData).Compare
                Return Date.Compare(x.StartTime, y.StartTime)
            End Function
        End Class
        ''' <summary>
        ''' The audio format of the call
        ''' </summary>
        ''' <value>The internal value for the format</value>
        ''' <returns>The internal value for the format</returns>
        ''' <remarks>This is retrieved from the first segment of the interation</remarks>
        Public Property AudioFormat() As utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat Implements IAudioInteractionData.AudioFormat
            Get
                Return m_interactionMetaFiles(0).AudioFormat
            End Get
            Set(ByVal value As utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat)

            End Set
        End Property

        ''' <summary>
        ''' The ID of the key used to encrypt the audio
        ''' </summary>
        ''' <value>The key ID</value>
        ''' <returns>The key ID</returns>
        ''' <remarks>This is retrieved from the first segment of the interation</remarks>
        Public Property EncryptionKeyID() As Integer Implements IAudioInteractionData.EncryptionKeyID
            Get
                Return m_interactionMetaFiles(0).EncryptionKeyID
            End Get
            Set(ByVal value As Integer)
            End Set
        End Property

        ''' <summary>
        ''' The type of speaker on the left audio channel
        ''' </summary>
        ''' <value>The type of speaker</value>
        ''' <returns>The type of speaker</returns>
        ''' <remarks>This is retrieved from the first segment of the interation</remarks>
        Public Property LeftChannelSpeaker() As String Implements IAudioInteractionData.LeftChannelSpeaker
            Get
                Return m_interactionMetaFiles(0).LeftChannelSpeaker
            End Get
            Set(ByVal value As String)

            End Set
        End Property

        ''' <summary>
        ''' The type of speaker on the right audio channel
        ''' </summary>
        ''' <value>The type of speaker</value>
        ''' <returns>The type of speaker</returns>
        ''' <remarks>This is retrieved from the first segment of the interation</remarks>
        Public Property RightChannelSpeaker() As String Implements IAudioInteractionData.RightChannelSpeaker
            Get
                Return m_interactionMetaFiles(0).RightChannelSpeaker
            End Get
            Set(ByVal value As String)

            End Set
        End Property

        ''' <summary>
        ''' The ID of the agent in the interaction
        ''' </summary>
        ''' <value>Agent ID</value>
        ''' <remarks>Only to be used when there is a single agent. Can be used for the speaker.</remarks>
        Public Property AgentID() As String Implements IInteractionData.AgentID
            Get
                Return Nothing
            End Get
            Set(ByVal value As String)
            End Set
        End Property

        ''' <summary>
        ''' The path to the file containing the interaction content (e.g. audio)
        ''' </summary>
        ''' <value>Path to the content file</value>
        ''' <returns>Path to the content file</returns>
        ''' <remarks>This is retrieved from the first segment of the interation</remarks>
        Public Property ContentFilePath As String Implements IInteractionData.ContentFilePath
            Get
                Return m_interactionMetaFiles(0).ContentFilePath
            End Get
            Set(ByVal value As String)
            End Set
        End Property

        ''' <summary>
        ''' The interaction ID
        ''' </summary>
        ''' <value>Interaction ID</value>
        ''' <returns>Interaction ID</returns>
        ''' <remarks>This is retrieved from the first segment of the interation</remarks>
        Public ReadOnly Property ID As String Implements IInteractionData.ID
            Get
                Return m_interactionMetaFiles(0).ID
            End Get
        End Property

        ''' <summary>
        ''' The path to the file containing the interaction metadata
        ''' </summary>
        ''' <value>Path to the metadata file</value>
        ''' <returns>Path to the metadata file</returns>
        ''' <remarks>This is retrieved from the first segment of the interation</remarks>
        Public Property MetaFilePath As String Implements IInteractionData.MetaFilePath
            Get
                Return m_interactionMetaFiles(0).MetaFilePath
            End Get
            Set(ByVal value As String)

            End Set
        End Property

        ''' <summary>
        ''' The ID of the SpeechMiner program that should handle the interaction
        ''' </summary>
        ''' <value>Program's external ID</value>
        ''' <returns>Program's external ID</returns>
        ''' <remarks>This is retrieved from the first segment of the interation</remarks>
        Public Property ProgramID As String Implements IInteractionData.Program
            Get
                Return m_interactionMetaFiles(0).Program
            End Get
            Set(ByVal value As String)
            End Set
        End Property

        ''' <summary>
        ''' The speakers who participated in the interaction
        ''' </summary>
        ''' <value>A collection of the speakers</value>
        ''' <returns>A collection of the speakers</returns>
        ''' <remarks>The start time and end time of each speaker is determined by the start time and end time of the segment</remarks>
        Public Property Speakers As ICollection(Of Speaker) Implements IInteractionData.Speakers
            Get
                Dim ret As New List(Of Speaker)
                Dim index As Integer = 0
                For Each segment As AudioInteractionData In m_interactionMetaFiles
                    If segment.Speakers Is Nothing Then
                        Continue For
                    End If

                    For Each speaker As Speaker In segment.Speakers
                        speaker.StartTime = Convert.ToDecimal((segment.StartTime - StartTime).TotalSeconds)
                        speaker.EndTime = Convert.ToDecimal((m_interactionContentFiles(index).EndTime - StartTime).TotalSeconds)
                        ret.Add(speaker)
                    Next
                    index += 1
                Next
                Return ret
            End Get
            Set(ByVal value As ICollection(Of Speaker))
            End Set
        End Property

        ''' <summary>
        ''' The date and time when the interaction started
        ''' </summary>
        ''' <value>Date and time of the interaction</value>
        ''' <returns>Date and time of the interaction</returns>
        ''' <remarks>This is retrieved from the first segment of the interaction</remarks>
        Public Property StartTime As Date Implements IInteractionData.StartTime
            Get
                Return m_interactionMetaFiles(0).StartTime
            End Get
            Set(ByVal value As Date)
            End Set
        End Property

        ''' <summary>
        ''' Accessor for the interaction's properties
        ''' </summary>
        ''' <param name="name">The name of the property</param>
        ''' <value>The value of the property</value>
        ''' <returns>The value of the property from the dictionary</returns>
        ''' <remarks>This is retrieved from the first segment of the interaction</remarks>
        Default Public Property Value(ByVal name As String) As String Implements IInteractionData.Value
            Get
                Return m_interactionMetaFiles(0).Value(name)
            End Get
            Set(ByVal value As String)
            End Set
        End Property

        ''' <summary>
        ''' The workgroup of the agent in the interaction
        ''' </summary>
        ''' <value>workgroup</value>
        ''' <remarks>Only to be used when there is a single agent. Can be used for the speaker.</remarks>
        Public Property Workgroup As String Implements IInteractionData.Workgroup
            Get
                Return Nothing
            End Get
            Set(ByVal value As String)
            End Set
        End Property

        ''' <summary>
        ''' The duration of the call
        ''' </summary>
        ''' <value>Duration in seconds</value>
        ''' <returns>Duration in seconds</returns>
        ''' <remarks></remarks>
        Public Property Duration As Decimal Implements IAudioInteractionData.Duration
            Get
                Dim lastEnd As DateTime = DateTime.MinValue
                For Each data As IAudioInteractionData In m_interactionMetaFiles
                    Dim endTime As DateTime = data.StartTime.AddSeconds(data.Duration)
                    If endTime > lastEnd Then
                        lastEnd = endTime
                    End If
                Next
                Return CDec((lastEnd - StartTime).TotalSeconds)
            End Get
            Set(ByVal value As Decimal)
            End Set
        End Property
    End Class
End Namespace