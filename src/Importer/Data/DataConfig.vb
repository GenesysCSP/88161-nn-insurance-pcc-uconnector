﻿Imports System.IO

Namespace Data
    ''' <summary>
    ''' Configuration for interaction data processing
    ''' </summary>
    ''' <remarks></remarks>
    Public Class DataConfig
        Inherits ConfigSection
        Implements IConfigSection

        ''' <summary>
        ''' Create a new data configuration object.
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            MyBase.New(configuration, XML_SETTINGS_DATA_SECTION_NAME)

            Dim backupFolder = Me(XML_SETTINGS_BACKUP_FILES_FOLDER)
            backupFolder = If(backupFolder IsNot Nothing, backupFolder.Trim(), Nothing)
            If String.IsNullOrEmpty(backupFolder) = False And Directory.Exists(backupFolder) = True Then
                _backupFolders.Add(backupFolder)
            End If

            Dim value = CStr(Me(XML_SETTINGS_BACKUP_FOLDERS))
            If String.IsNullOrEmpty(value) = False Then
                For Each folder As String In value.Split("|"c)
                    _backupFolders.Add(folder)
                Next
            End If

            _limitToAgentsInHierarchy = Me(LIMIT_TO_AGENTS_IN_HIERARCHY) IsNot Nothing
        End Sub

        ''' <summary>
        ''' The class of Data Manager to create.
        ''' <seealso cref="XML_SETTINGS_DATA_MANAGER_TYPE"/>
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <value>The class of Data Manager to create</value>
        ''' <returns>The Type to create</returns>
        ''' <remarks>The class must implement <see cref="IDataManager"/> and have a constructor that accepts <see cref="IConfigSection"/>
        ''' or one that accepts an <see cref="IConfigSection"/> and an <see cref="IAgentManager"/>.</remarks>
        Public Shared ReadOnly Property ManagerType(ByVal configuration As IConfigSection) As Type
            Get
                Dim mgrType As String = configuration(XML_SETTINGS_DATA_MANAGER_TYPE)
                Return If(String.IsNullOrWhiteSpace(mgrType) = False, GetMgrType(mgrType), Nothing)
            End Get
        End Property
        ''' <summary>
        ''' The class of Inner Data Manager to create.
        ''' <seealso cref="XML_SETTINGS_INNER_DATA_MANAGER_TYPE"/>
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <value>The class of Inner Data Manager to create</value>
        ''' <returns>The Type to create</returns>
        ''' <remarks>The class must implement <see cref="IDataManager"/> and have a constructor that accepts <see cref="IConfigSection"/>
        ''' or one that accepts an <see cref="IConfigSection"/> and an <see cref="IAgentManager"/>.</remarks>
        Public Shared ReadOnly Property InnerManagerType(ByVal configuration As IConfigSection) As Type
            Get
                Dim mgrType As String = configuration(XML_SETTINGS_INNER_DATA_MANAGER_TYPE)
                If String.IsNullOrWhiteSpace(mgrType) = True Then
                    Return Nothing
                End If

                Return GetMgrType(mgrType)
            End Get
        End Property

        Private Shared Function GetMgrType(
            manager As String
            ) As Type

            If String.IsNullOrWhiteSpace(manager) = True Then
                Throw If(manager = Nothing,
                          New ArgumentNullException("manager"),
                          New ArgumentException("no manager type specified"))
            End If

            Return If(String.Equals(manager, "database", StringComparison.InvariantCultureIgnoreCase) = True,
                      GetType(DatabaseDataManager), Type.GetType(manager))
        End Function

        ''' <summary>
        ''' The connection string name for the database from which we get the interactions.
        ''' <seealso cref="XML_SETTINGS_DATA_CONNECTION_NAME"/>
        ''' </summary>
        ''' <value>Connection string name</value>
        ''' <returns>Connection string name</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property DataConnectionName() As String
            Get
                Return Me(XML_SETTINGS_DATA_CONNECTION_NAME)
            End Get
        End Property

        ''' <summary>
        ''' The SQL query to be used for getting the list of interactions and their data.
        ''' <seealso cref="XML_SETTINGS_DATA_QUERY"/>
        ''' </summary>
        ''' <value>SQL query</value>
        ''' <returns>SQL query</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property DataQuery() As String
            Get
                Return Me(XML_SETTINGS_DATA_QUERY)
            End Get
        End Property

        ''' <summary>
        ''' The SQL query to be used to retrieve the number of new interactions waiting to be processed
        ''' </summary>
        ''' <value>The SQL query</value>
        ''' <returns>The SQL query</returns>
        ''' <remarks>The default query uses the regular data query, and counts from it.</remarks>
        Public Overridable ReadOnly Property DataQueueQuery() As String
            Get
                Dim query As String = If(Me(XML_SETTINGS_DATA_QUEUE_QUERY) IsNot Nothing, _
                                         Me(XML_SETTINGS_DATA_QUEUE_QUERY).ToString(), String.Empty)

                Return If(String.IsNullOrEmpty(query) = True, _
                          "SELECT COUNT(*) FROM (" & DataQuery & ") AS q", query)
            End Get
        End Property

        ''' <summary>
        ''' The last processed interaction.
        ''' <seealso cref="XML_SETTINGS_LAST_HANDLED_INTERACTION"/>
        ''' </summary>
        ''' <value>Property value for the last interaction processed</value>
        ''' <returns>Property value for the last interaction processed</returns>
        ''' <remarks>The property used depends on <see cref="InteractionOrderField"/></remarks>
        Public Overridable Property LastHandledInteraction() As String
            Get
                Return Me(XML_SETTINGS_LAST_HANDLED_INTERACTION)
            End Get
            Set(ByVal value As String)
                Me(XML_SETTINGS_LAST_HANDLED_INTERACTION) = value
            End Set
        End Property

        Public Property LimitToAgentsInHierarchy As Boolean
            Get
                Return _limitToAgentsInHierarchy
            End Get
            Set(value As Boolean)
                _limitToAgentsInHierarchy = value
            End Set
        End Property

        ''' <summary>
        ''' The field by which to sort interactions.
        ''' <seealso cref="XML_SETTINGS_INTERACTION_ORDER_FIELD"/>
        ''' </summary>
        ''' <value>The name of the field</value>
        ''' <returns>The name of the field</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property InteractionOrderField() As String
            Get
                Return Me(XML_SETTINGS_INTERACTION_ORDER_FIELD)
            End Get
        End Property

        ''' <summary>
        ''' The placeholder to be used for the last processed interaction.
        ''' </summary>
        ''' <value>Placeholder string</value>
        ''' <returns>Placeholder string</returns>
        ''' <remarks>The string should appear in <see cref="DataQuery"/> for replacement.</remarks>
        Public ReadOnly Property LastInteractionHolder() As String
            Get
                Return "@LastInteraction@"
            End Get
        End Property

        ''' <summary>
        ''' Default placeholder for the last processed interaction, in case it is not specified in the configuration.
        ''' </summary>
        ''' <value>Placeholder string</value>
        ''' <returns><c>[LastInteraction]</c></returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property LastInteractionHolderDefault() As String
            Get
                Return "[LastInteraction]"
            End Get
        End Property

        ''' <summary>
        ''' The folder from which to get interaction files.
        ''' <seealso cref="XML_SETTINGS_DATA_FOLDER"/>
        ''' </summary>
        ''' <value>Folder path</value>
        ''' <returns>Folder path</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property DataFolder() As String
            Get
                Return Me(XML_SETTINGS_DATA_FOLDER)
            End Get
        End Property

        ''' <summary>
        ''' The pattern of file names to take from the data folder.
        ''' <seealso cref="XML_SETTINGS_DATA_FILE_PATTERN"/>
        ''' </summary>
        ''' <value>Filename pattern</value>
        ''' <returns>Filename pattern</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property DataFilePattern() As String
            Get
                If String.IsNullOrEmpty(Me(XML_SETTINGS_DATA_FILE_PATTERN)) Then
                    Return "*.xml"
                Else
                    Return Me(XML_SETTINGS_DATA_FILE_PATTERN)
                End If
            End Get
        End Property

        ''' <summary>
        ''' Whether to recursively go into folders inside the data folder.
        ''' <seealso cref="XML_SETTINGS_DATA_FOLDER_RECURSIVE"/>
        ''' </summary>
        ''' <value>Whether to go into subfolders</value>
        ''' <returns>Whether to go into subfolders</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property ReadSubfolders() As Boolean
            Get
                If String.IsNullOrEmpty(Me(XML_SETTINGS_DATA_FOLDER_RECURSIVE)) Then
                    Return False
                Else
                    Return CBool(Me(XML_SETTINGS_DATA_FOLDER_RECURSIVE))
                End If
            End Get
        End Property

        ''' <summary>
        ''' The folder to which to backup files for successfully handled interactions.
        ''' <seealso cref="XML_SETTINGS_BACKUP_FILES_FOLDER"/>
        ''' </summary>
        ''' <value>Backup folder path</value>
        ''' <returns>Backup folder path</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property BackupFilesFolder() As String
            Get
                Return Me(XML_SETTINGS_BACKUP_FILES_FOLDER)
            End Get
        End Property

        ''' <summary>
        ''' The folder to which to copy files for interactions whose processing has failed.
        ''' <seealso cref="XML_SETTINGS_FAILED_FILES_FOLDER"/>
        ''' </summary>
        ''' <value>Failed files folder path</value>
        ''' <returns>Failed files folder path</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property FailedFilesFolder() As String
            Get
                Return Me(XML_SETTINGS_FAILED_FILES_FOLDER)
            End Get
        End Property

        ''' <summary>
        ''' Whether to ask the agent manager for the workgroup based on the agent's ID.
        ''' <seealso cref="XML_SETTINGS_USE_AGENT_MANAGER_FOR_WORKGROUP"/>
        ''' </summary>
        ''' <value>Whether to use agent manager</value>
        ''' <returns>Whether to use agent manager</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property UseAgentManagerForWorkgroups() As Boolean
            Get
                If String.IsNullOrEmpty(Me(XML_SETTINGS_USE_AGENT_MANAGER_FOR_WORKGROUP)) Then
                    Return False
                Else
                    Return CBool(Me(XML_SETTINGS_USE_AGENT_MANAGER_FOR_WORKGROUP))
                End If
            End Get
        End Property

        ''' <summary>
        ''' Whether to encrypt audio files.
        ''' <seealso cref="XML_SETTINGS_ENCRYPT_AUDIO"/>
        ''' </summary>
        ''' <value>Whether to encrypt audio files.</value>
        ''' <returns>Whether to encrypt audio files.</returns>
        ''' <remarks>Encryption will only happen if it is turned on in SpeechMiner.</remarks>
        Public Overridable ReadOnly Property EncryptAudio() As Boolean
            Get
                If String.IsNullOrEmpty(Me(XML_SETTINGS_ENCRYPT_AUDIO)) Then
                    Return False
                Else
                    Return CBool(Me(XML_SETTINGS_ENCRYPT_AUDIO))
                End If
            End Get
        End Property

        ''' <summary>
        ''' The SpeechMiner program to assign to interactions.
        ''' <seealso cref="XML_SETTINGS_PROGRAM"/>
        ''' </summary>
        ''' <value>Program ID to use</value>
        ''' <returns>Program ID to use</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property Program() As String
            Get
                Return Me(XML_SETTINGS_PROGRAM)
            End Get
        End Property

        ''' <summary>
        ''' The workgroup to assign to interaction agents
        ''' <seealso cref="XML_SETTINGS_WORKGROUP"/>
        ''' </summary>
        ''' <value>Agent workgroup</value>
        ''' <returns>Agent workgroup</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property Workgroup() As String
            Get
                Return Me(XML_SETTINGS_WORKGROUP)
            End Get
        End Property

        ''' <summary>
        ''' Interaction data fields to add as extra metadata.
        ''' <seealso cref="XML_SETTINGS_EXTRA_META_FIELDS"/>
        ''' </summary>
        ''' <value>Mapping of fields to new names</value>
        ''' <returns>Mapping of fields to new names</returns>
        ''' <remarks>Fields are comma separated. To use a different tag than the interaction property use the format <c>property=tag</c></remarks>
        Public Overridable ReadOnly Property ExtraMetaFields() As IDictionary(Of String, String)
            Get
                Return ConfigurationUtils.ParseKeyValuePairs(Me(XML_SETTINGS_EXTRA_META_FIELDS))
            End Get
        End Property

        ''' <summary>
        '''  Interaction id field as the identifier of the call.
        ''' <seealso cref="XML_SETTINGS_SEGMENTED_INTERACTION_ID_FIELD"/>
        ''' </summary>
        ''' <value>Name of the field holding the identifier of segmented call</value>
        ''' <returns>Name of the field holding the identifier of segmented call</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property SegmentedInteractionIdField() As String
            Get
                Return Me(XML_SETTINGS_SEGMENTED_INTERACTION_ID_FIELD)
            End Get
        End Property
        ''' <summary>
        ''' The class of Segmented Interaction Data Type to create.
        ''' <seealso cref="XML_SETTINGS_SEGMENTED_INTERACTION_DATA_TYPE"/>
        ''' </summary>
        ''' <value>The class of Segmented Interaction Data Type to create</value>
        ''' <returns>The Type to create</returns>
        ''' <remarks>Normally only used if the interaction arrives in segments which need stitching.</remarks>
        Public Shared ReadOnly Property SegmentedInteractionDataType(ByVal configuration As IConfigSection) As Type
            Get
                Dim segType As String = configuration(XML_SETTINGS_SEGMENTED_INTERACTION_DATA_TYPE)
                If String.IsNullOrEmpty(segType) Then
                    Return Nothing
                Else
                    Return Type.GetType(segType)
                End If
            End Get
        End Property
        ''' <summary>
        ''' The speaker type to assign to the left audio channel.
        ''' <seealso cref="XML_SETTINGS_LEFT_SPEAKER"/>
        ''' </summary>
        ''' <value>Channel speaker type</value>
        ''' <returns>Channel speaker type</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property LeftChannelSpeaker() As String
            Get
                Return Me(XML_SETTINGS_LEFT_SPEAKER)
            End Get
        End Property

        ''' <summary>
        ''' The speaker type to assign to the right audio channel.
        ''' <seealso cref="XML_SETTINGS_RIGHT_SPEAKER"/>
        ''' </summary>
        ''' <value>Channel speaker type</value>
        ''' <returns>Channel speaker type</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property RightChannelSpeaker() As String
            Get
                Return Me(XML_SETTINGS_RIGHT_SPEAKER)
            End Get
        End Property

#Region "Class Data"

        ''' <summary>
        ''' Tag for the data handling section. Value: Data
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_SETTINGS_DATA_SECTION_NAME As String = "Data"

        ''' <summary>
        ''' Tag for the fully qualified class name of the Data Manager. Value: DataManagerType
        ''' </summary>
        ''' <remarks><para>The class must implement <see cref="IAgentManager"/> and have a constructor that accepts <see cref="IConfigSection"/>.</para>
        ''' <para>You can also use <c>Database</c> or <c>File</c>. The default type is <c>Database</c>.</para></remarks>
        Protected Const XML_SETTINGS_DATA_MANAGER_TYPE As String = XML_SETTINGS_DATA_SECTION_NAME & Config.SETTINGS_SEPARATOR & "DataManagerType"
        ''' <summary>
        ''' Tag for the fully qualified class name of the Inner Data Manager. Value: InnerDataManagerType
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_SETTINGS_INNER_DATA_MANAGER_TYPE As String = XML_SETTINGS_DATA_SECTION_NAME & Config.SETTINGS_SEPARATOR & "InnerDataManagerType"
        ''' <summary>
        ''' Tag for the connection string name for the database from which we get the interactions. Value: DataConnectionName
        ''' </summary>
        ''' <remarks>Only relevant for a database data manager.</remarks>
        Protected Const XML_SETTINGS_DATA_CONNECTION_NAME As String = "DataConnectionName"
        ''' <summary>
        ''' Tag for the SQL query to be used for getting the list of interactions and their data. Value: DataQuery
        ''' </summary>
        ''' <remarks>Only relevant for a database data manager.</remarks>
        Protected Const XML_SETTINGS_DATA_QUERY As String = "DataQuery"
        ''' <summary>
        ''' Tag for the SQL query to be used to retrieve the number of new interactions waiting to be processed. Value: DataQueueQuery
        ''' </summary>
        ''' <remarks>Only relevant for a database data manager.</remarks>
        Protected Const XML_SETTINGS_DATA_QUEUE_QUERY As String = "DataQueueQuery"
        ''' <summary>
        ''' Tag for the last processed interaction. Value: LastHandledInteraction
        ''' </summary>
        ''' <remarks>Mostly relevant for a database data manager.</remarks>
        Protected Const XML_SETTINGS_LAST_HANDLED_INTERACTION As String = "LastHandledInteraction"
        ''' <summary>
        ''' Tag for the field by which to sort interactions. Value: InteractionOrderField
        ''' </summary>
        ''' <remarks>Mostly relevant for a database data manager.</remarks>
        Protected Const XML_SETTINGS_INTERACTION_ORDER_FIELD As String = "InteractionOrderField"
        ''' <summary>
        ''' Tag for the folder from which to get interaction files. Value: DataFolder
        ''' </summary>
        ''' <remarks>Only relevant for a file system data manager.</remarks>
        Protected Const XML_SETTINGS_DATA_FOLDER As String = "DataFolder"
        ''' <summary>
        ''' Tag for the folder to which to backup files for successfully handled interactions. Value: BackupFilesFolder
        ''' </summary>
        ''' <remarks>Only relevant for a file system data manager.</remarks>
        Private Const XML_SETTINGS_BACKUP_FILES_FOLDER As String = "BackupFilesFolder"
        Private Const XML_SETTINGS_BACKUP_FOLDERS As String = "BackupFolders"

        ''' <summary>
        ''' Tag for the folder to which to copy files for interactions whose processing has failed. Value: FailedFilesFolder
        ''' </summary>
        ''' <remarks>Only relevant for a file system data manager.</remarks>
        Protected Const XML_SETTINGS_FAILED_FILES_FOLDER As String = "FailedFilesFolder"
        ''' <summary>
        ''' Tag for the pattern of file names to take from the data folder. Value: DataFilenamePattern
        ''' </summary>
        ''' <remarks><para>Only relevant for a file system data manager.</para>
        ''' <para>The default is <c>*.xml</c></para></remarks>
        Protected Const XML_SETTINGS_DATA_FILE_PATTERN As String = "DataFilenamePattern"
        ''' <summary>
        ''' Tag for whether to recursively go into folders inside the data folder. Value: DataFolderRecursive
        ''' </summary>
        ''' <remarks>Only relevant for a file system data manager.</remarks>
        Protected Const XML_SETTINGS_DATA_FOLDER_RECURSIVE As String = "DataFolderRecursive"
        ''' <summary>
        ''' Tag for whether to ask the agent manager for the workgroup based on the agent's ID. Value: UseAgentManagerForWorkgroup
        ''' </summary>
        ''' <remarks>The default is not to use it.</remarks>
        Protected Const XML_SETTINGS_USE_AGENT_MANAGER_FOR_WORKGROUP As String = "UseAgentManagerForWorkgroup"
        ''' <summary>
        ''' Tag for whether to encrypt audio files. Value: EncryptAudio
        ''' </summary>
        ''' <remarks><para>Only relevant for audio calls.</para>
        ''' <para>The default is not to encrypt.</para></remarks>
        Protected Const XML_SETTINGS_ENCRYPT_AUDIO As String = "EncryptAudio"
        ''' <summary>
        ''' Tag for the SpeechMiner program to assign to interactions. Value: Program
        ''' </summary>
        ''' <remarks>This is only used if the program is not set to the interaction directly.</remarks>
        Protected Const XML_SETTINGS_PROGRAM As String = "Program"
        ''' <summary>
        ''' Tag for the workgroup to assign to interaction agents. Value: Workgroup
        ''' </summary>
        ''' <remarks>Normally only used if there is no workgroup information from other sources.</remarks>
        Protected Const XML_SETTINGS_WORKGROUP As String = "Workgroup"
        ''' <summary>
        ''' Tag for the speaker type to assign to the left audio channel. Value: LeftChannelSpeaker
        ''' </summary>
        ''' <remarks><para>Only relevant for audio calls.</para>
        ''' <para>Normally only used if the channel information is not available from other sources.</para></remarks>
        Protected Const XML_SETTINGS_LEFT_SPEAKER As String = "LeftChannelSpeaker"
        ''' <summary>
        ''' Tag fo the speaker type to assign to the right audio channel. Value: RightChannelSpeaker
        ''' </summary>
        ''' <remarks><para>Only relevant for audio calls.</para>
        ''' <para>Normally only used if the channel information is not available from other sources.</para></remarks>
        Protected Const XML_SETTINGS_RIGHT_SPEAKER As String = "RightChannelSpeaker"
        ''' <summary>
        ''' Tag for interaction data fields to add as extra metadata. Value: ExtraMetaFields
        ''' </summary>
        ''' <remarks>Fields are comma separated. To use a different tag than the interaction property use the format <c>property=tag</c></remarks>
        Protected Const XML_SETTINGS_EXTRA_META_FIELDS As String = "ExtraMetaFields"

        ''' <summary>
        ''' Tag for the field holding the common identifier of the a call with segmentes. Value: SegmentedInteractionIdField
        ''' </summary>
        ''' <remarks>Normally only used if the interaction arrives in segments which need stitching</remarks>
        Protected Const XML_SETTINGS_SEGMENTED_INTERACTION_ID_FIELD As String = "SegmentedInteractionIdField"
        ''' <summary>
        ''' Tag for the fully qualified class name of the Segmented Interaction Data Type. Value: SegmentedInteractionDataType
        ''' </summary>
        ''' <remarks>Normally only used if the interaction arrives in segments which need stitching</remarks>
        Protected Const XML_SETTINGS_SEGMENTED_INTERACTION_DATA_TYPE As String = "SegmentedInteractionDataType"

        Private Const LIMIT_TO_AGENTS_IN_HIERARCHY = "LimitToAgentsInHierarchy"

#End Region

#Region "Instance Data"

        Private ReadOnly _backupFolders As New HashSet(Of String)
        Private _limitToAgentsInHierarchy As Boolean = False

#End Region

    End Class
End Namespace