﻿Namespace Data
  Public Interface ISegmentedContentFile
    ''' <summary>
    ''' The path to the file containing the segment content (e.g. audio)
    ''' </summary>
    ''' <value>Path to the content file</value>
    ''' <returns>Path to the content file</returns>
    ''' <remarks></remarks>
    Property ContentFilePath() As String
    ''' <summary>
    ''' The date and time when the segment started
    ''' </summary>
    ''' <value>Date and time when the segment started</value>
    ''' <returns>Date and time when the segment started</returns>
    ''' <remarks></remarks>
    Property StartTime() As DateTime
    ''' <summary>
    ''' The date and time when the segment ended
    ''' </summary>
    ''' <value>Date and time when the segment ended</value>
    ''' <returns>Date and time when the segment ended</returns>
    ''' <remarks></remarks>
    Property EndTime() As DateTime
  End Interface
End Namespace
