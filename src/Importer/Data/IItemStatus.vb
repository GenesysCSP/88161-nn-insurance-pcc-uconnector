﻿Namespace Data
    ''' <summary>
    ''' Status of a single item processing
    ''' </summary>
    ''' <remarks></remarks>
    Public Interface IItemStatus

        ''' <summary>
        ''' Whether processing was successful or not
        ''' </summary>
        ''' <value>True for successful processing</value>
        ''' <returns>True for successful processing</returns>
        ''' <remarks></remarks>
        Property Success() As Boolean
        ''' <summary>
        ''' Message describing why the processing failed
        ''' </summary>
        ''' <value>A message describing the failure</value>
        ''' <returns>A message describing the failure</returns>
        ''' <remarks></remarks>
        Property Message() As String
        ''' <summary>
        ''' Exception that caused the processing to fail
        ''' </summary>
        ''' <value>The exception causing the failure</value>
        ''' <returns>The exception causing the failure</returns>
        ''' <remarks></remarks>
        Property Exception() As Exception

        ''' <summary>
        ''' Return a string representation of the status, for logging purposes
        ''' </summary>
        ''' <returns>String for logging</returns>
        ''' <remarks></remarks>
        Function ToLogString() As String

    End Interface
End Namespace