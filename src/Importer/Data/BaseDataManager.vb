﻿Imports GenUtils

Namespace Data
    Public MustInherit Class BaseDataManager
        Implements IDataManager

#Region "Instance Properties"

        ''' <summary>
        ''' Key for the agent ID property
        ''' </summary>
        ''' <value>Property key</value>
        ''' <returns>Property key</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property AgentIdKey() As String
            Get
                Return "AgentId"
            End Get
        End Property

        Public ReadOnly Property AgentManager As IAgentManager
            Get
                Return _agentManager
            End Get
        End Property

        Public ReadOnly Property Config As DataConfig
            Get
                Return _config
            End Get
        End Property

        ''' <summary>
        ''' Key for the interaction ID property
        ''' </summary>
        ''' <value>Property key</value>
        ''' <returns>Property key</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property InteractionIdKey() As String
            Get
                Return "InteractionID"
            End Get
        End Property

        Public ReadOnly Property LimitToAgentsInHierarchy As Boolean
            Get
                Return Me.Config.LimitToAgentsInHierarchy
            End Get
        End Property

        ''' <summary>
        ''' Key for the start time property
        ''' </summary>
        ''' <value>Property key</value>
        ''' <returns>Property key</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property StartTimeKey() As String
            Get
                Return "StartTime"
            End Get
        End Property

        ''' <summary>
        ''' Key for the workgroup property
        ''' </summary>
        ''' <value>Property key</value>
        ''' <returns>Property key</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property WorkgroupKey() As String
            Get
                Return "Workgroup"
            End Get
        End Property

#End Region

#Region "Constructors"

        Public Sub New(
            config As IConfigSection,
            Optional agentManager As IAgentManager = Nothing
            )
            _config = New DataConfig(config)
            _agentManager = agentManager
        End Sub

#End Region

#Region "Instance Methods"

        ''' <summary>
        ''' Create a data object for the interaction.
        ''' </summary>
        ''' <param name="id">The ID of the interaction</param>
        ''' <returns>The interaction data</returns>
        ''' <remarks>Creates an audio call data object.</remarks>
        Protected Overridable Function CreateInteractionData(ByVal id As String) As IInteractionData
            Dim ret As New AudioInteractionData(id)
            If Me.Config.EncryptAudio Then
                ret.EncryptionKeyID = utopy.bDBOffline.AudioKeys.currentKeyId
            End If
            If Not String.IsNullOrEmpty(Me.Config.LeftChannelSpeaker) AndAlso Not String.IsNullOrEmpty(Me.Config.RightChannelSpeaker) Then
                ret.LeftChannelSpeaker = Me.Config.LeftChannelSpeaker
                ret.RightChannelSpeaker = Me.Config.RightChannelSpeaker
            End If
            Return ret
        End Function

        Public Function GetNextInteractions(
            limitTo As Integer
            ) As IEnumerable(Of IInteractionData) Implements IDataManager.GetNextInteractions

            If Me.IxnsAvailable() = False Then
                Dim empty(-1) As IInteractionData
                Return empty
            End If

            Return Me.OnGetNextInteractions(limitTo)
        End Function

        ''' <summary>
        ''' Callback for when one interaction is handled.
        ''' </summary>
        ''' <param name="interaction">The interaction data</param>
        ''' <param name="status">Success/failure information</param>
        ''' <remarks></remarks>
        Public MustOverride Sub InteractionHandled(ByVal interaction As IInteractionData, ByVal status As IItemStatus) Implements IDataManager.InteractionHandled

        Public Function IsCallFromAgentAllowed(
            agentId As String
            ) As Boolean

            If agentId Is Nothing Then
                Throw New ArgumentNullException("agentId")
            ElseIf Me.AgentManager Is Nothing Then
                Return True
            End If

            Return Me.Config.LimitToAgentsInHierarchy = False OrElse Me.AgentManager.IsAgentInHierarchy(agentId) = True
        End Function

        Public MustOverride Function IxnsAvailable() As Boolean Implements IDataManager.IxnsAvailable

        Protected MustOverride Function OnGetNextInteractions(limitTo As Integer) As IEnumerable(Of IInteractionData)

        ''' <summary>
        ''' Set the value for a property of an interaction
        ''' </summary>
        ''' <param name="interactionData">The interaction data object</param>
        ''' <param name="key">The property's key</param>
        ''' <param name="val">The property's value</param>
        ''' <remarks>Does additional processing for some keys. E.g., when setting an agent, it may also set the workgroup, using the agent manager.</remarks>
        Protected Overridable Sub SetInteractionValue(ByVal interactionData As IInteractionData, ByVal key As String, ByVal val As String)
            Select Case key.ToLower()
                Case AgentIdKey.ToLower()
                    interactionData.AgentID = val
                    If Me.Config.UseAgentManagerForWorkgroups Then
                        If Me.AgentManager IsNot Nothing Then
                            Dim workgroup As String = Me.AgentManager.GetWorkgroup(val)
                            If Not String.IsNullOrEmpty(workgroup) Then
                                interactionData.Workgroup = workgroup
                            End If
                        End If
                    End If
                Case WorkgroupKey.ToLower()
                    interactionData.Workgroup = val
                Case StartTimeKey.ToLower()
                    interactionData.StartTime = Date.Parse(val)
                Case Else
                    interactionData(key) = val
            End Select
        End Sub

#End Region

#Region "Instance Data"

        Private ReadOnly _agentManager As IAgentManager
        Private ReadOnly _config As DataConfig

#End Region

    End Class
End Namespace