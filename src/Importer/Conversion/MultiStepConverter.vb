﻿Imports System.IO
Imports System.Reflection
Imports GenUtils.TraceTopic


Namespace Conversion
    ''' <summary>
    ''' Runs multiple converters sequentially
    ''' </summary>
    ''' <remarks>To configure this converter, use the following format:
    ''' <example>
    ''' <code language="xml" title="Converter configuration">
    ''' <![CDATA[
    ''' <Conversion>
    '''   <AudioConverterType>UConnector.Conversion.MultiStepConverter</AudioConverterType>
    '''   <Converters>WMEConverter,FFMpegConverter</Converters>
    '''   <WMEConverter>
    '''     <Conversion>
    '''       <AudioConverterType>External</AudioConverterType>
    '''       <AudioConverterWorkingDirectory>c:\temp</AudioConverterWorkingDirectory>
    '''       <AudioConverterPath>C:\Windows\SysWOW64\cscript.exe</AudioConverterPath>
    '''       <AudioConverterArguments>"c:\Program Files (x86)\Windows Media Components\Encoder\WMCmd.vbs" -input %INPUT% -output %OUTPUT% -profile a64</AudioConverterArguments>
    '''     </Conversion>
    '''   </WMEConverter>
    '''   <FFMpegConverter>
    '''     <Conversion>
    '''       <AudioConverterType>External</AudioConverterType>
    '''       <AudioConverterWorkingDirectory>c:\temp</AudioConverterWorkingDirectory>
    '''       <AudioConverterPath>C:\utils\ffmpeg.exe</AudioConverterPath>
    '''       <AudioConverterArguments>-i %INPUT% %OUTPUT%</AudioConverterArguments>
    '''     </Conversion>
    '''   </FFMpegConverter>
    ''' </Conversion>
    ''' ]]>
    ''' </code>
    ''' </example>
    ''' </remarks>
    Public Class MultiStepConverter
        Implements IAudioConverter

#Region "Instance Properties"

        Public ReadOnly Property SourceExtension() As String Implements IAudioConverter.SourceExtension
            Get
                Return If(_converters.Count > 0, _converters.First.SourceExtension, Nothing)
            End Get
        End Property

        Public ReadOnly Property TargetExtension() As String Implements IAudioConverter.TargetExtension
            Get
                Return If(_converters.Count > 0, _converters.Last.TargetExtension, Nothing)
            End Get
        End Property

#End Region

#Region "Constructors"

        ''' <summary>
        ''' Create a converter with an empty list of converters.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New(
            ParamArray converters() As IAudioConverter
            )
            Array.ForEach(converters, Sub(converter) Me.AddConverter(converter))
        End Sub

        Public Sub New(
            configuration As IConfigSection
            )
            _configuration = New ConversionConfig(configuration)
            Dim convertersList = _configuration("Converters")
            If String.IsNullOrEmpty(convertersList) = True Then
                Return
            End If

            For Each converterName As String In convertersList.Split(","c)
                Dim config As New SubConverterConfiguration(_configuration, converterName)
                Dim convType = ConversionConfig.ConverterType(config)
                If convType Is Nothing Then
                    Continue For
                End If

                Dim ci = convType.GetConstructor(New Type() {GetType(IConfigSection)})
                If ci Is Nothing Then
                    Throw New MissingMethodException("Constructor not found for " & convType.Name)
                End If
                Dim converter As IAudioConverter = CType(ci.Invoke(New Object() {config}), IAudioConverter)
                If converter IsNot Nothing Then
                    _converters.Add(converter)
                End If
            Next
        End Sub

#End Region

#Region "Instance Methods"

        Public Sub AddConverter(
            converter As IAudioConverter
            )
            If converter Is Nothing Then
                Throw New ArgumentNullException("converter")
            End If

            _converters.Add(converter)
        End Sub

        ''' <summary>
        ''' Convert the audio file
        ''' </summary>
        ''' <param name="sourcePath">Path to the file to be converted</param>
        ''' <param name="targetPath">Path for the converted file</param>
        ''' <param name="sourceEncryption">Encryption key used for the source file</param>
        ''' <param name="targetEncryption">Encryption key to use for the target</param>
        ''' <returns>Whether the conversion succeeded or not</returns>
        ''' <remarks>The converters are run in order. The output of each converter serves as the input for the next one.</remarks>
        Public Function Convert(
            sourcePath As String,
            targetPath As String,
            sourceEncryption As Integer,
            ByRef targetEncryption As Integer
            ) As Boolean Implements IAudioConverter.Convert

            Using (TraceTopic.importerTopic.scope())
                Dim tempPath = sourcePath
                Dim fromPath As String
                Dim sourceKey = sourceEncryption
                For Each converter As IAudioConverter In _converters
                    fromPath = tempPath
                    If converter.SourceExtension IsNot Nothing Then
                        If Path.GetExtension(fromPath) <> converter.SourceExtension AndAlso fromPath <> sourcePath Then
                            Dim newPath As String = Path.ChangeExtension(fromPath, SourceExtension)
                            File.Move(fromPath, newPath)
                            fromPath = newPath
                        End If
                    End If
                    tempPath = FileUtils.GetTempPath(converter.TargetExtension)
                    Try
                        If Not converter.Convert(fromPath, tempPath, sourceKey, targetEncryption) Then
                            Return False
                        End If
                        sourceKey = targetEncryption
                    Finally
                        If fromPath <> sourcePath Then
                            Try
                                File.Delete(fromPath)
                            Catch ex As Exception
                                TraceTopic.importerTopic.warning("[{}] WARN - Error deleting {} (Reason: {}), {}",
                                    Me.GetType().Name, fromPath, ex.Message, ex)
                            End Try
                        End If
                    End Try
                Next

                Try
                    File.Move(tempPath, targetPath)
                    Return True
                Catch ex As Exception
                    TraceTopic.importerTopic.error("[{}] ERROR - Error moving converted file from {} to {} (Reason: {}), {}",
                        Me.GetType().Name, tempPath, targetPath, ex.Message, ex)
                    Return False
                End Try
            End Using
        End Function

#End Region

#Region "Types"

        Private NotInheritable Class SubConverterConfiguration
            Inherits ConversionConfig

            Public Sub New(configuration As IConfigSection, sectionName As String)
                MyBase.New(configuration)
                m_sectionName = sectionName
            End Sub
        End Class

#End Region

#Region "Instance Data"

        Private ReadOnly _configuration As ConversionConfig
        Private ReadOnly _converters As IList(Of IAudioConverter) = New List(Of IAudioConverter)

#End Region

    End Class
End Namespace