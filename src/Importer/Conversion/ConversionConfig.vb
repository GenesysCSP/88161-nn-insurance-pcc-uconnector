﻿Namespace Conversion
    ''' <summary>
    ''' Configuration for audio conversion
    ''' </summary>
    ''' <remarks></remarks>
    Public Class ConversionConfig
        Inherits ConfigSection
        Implements IConfigSection

        ''' <summary>
        ''' Tag for the audio conversion section. Value: Conversion
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_SETTINGS_CONVERSION_SECTION_NAME As String = "Conversion"

        ''' <summary>
        ''' Tag for the fully qualified class name for the audio converter. Value: AudioConverterType
        ''' </summary>
        ''' <remarks><para>The class must implement <see cref="IAudioConverter"/> and have a constructor that accepts <see cref="IConfigSection"/>.</para>
        ''' <para>You can also use <c>Internal</c> or <c>External</c>. If missing, audio will not be converted.</para></remarks>
        Protected Const XML_SETTINGS_AUDIO_CONVERTER_TYPE As String = XML_SETTINGS_CONVERSION_SECTION_NAME & Config.SETTINGS_SEPARATOR & "AudioConverterType"
        ''' <summary>
        ''' Tag for the audio format for the file to be converted. Value: AudioSourceFormat
        ''' </summary>
        ''' <remarks>The default is <c>WAV_MULAW</c>.</remarks>
        Protected Const XML_SETTINGS_AUDIO_SOURCE_FORMAT As String = "AudioSourceFormat"
        ''' <summary>
        ''' Tag for the format to which to convert the audio, e.g. WAV_PCM. Value: AudioFormat
        ''' </summary>
        ''' <remarks>The default is <c>WAV_PCM</c>.</remarks>
        Protected Const XML_SETTINGS_AUDIO_FORMAT As String = "AudioFormat"
        ''' <summary>
        ''' Tag for the directory in which to run the conversion. Value: AudioConverterWorkingDirectory
        ''' </summary>
        ''' <remarks>Not relevant for internal conversion.</remarks>
        Protected Const XML_SETTINGS_AUDIO_CONVERTER_WORKING_DIR As String = "AudioConverterWorkingDirectory"
        ''' <summary>
        ''' Tag for the path to the converter executable. Value: AudioConverterPath
        ''' </summary>
        ''' <remarks>Not relevant for internal conversion.</remarks>
        Protected Const XML_SETTINGS_AUDIO_CONVERTER_PATH As String = "AudioConverterPath"
        ''' <summary>
        ''' Tag for the arguments to give the converter executable. Value: AudioConverterArguments
        ''' </summary>
        ''' <remarks>Not relevant for internal conversion.</remarks>
        Protected Const XML_SETTINGS_AUDIO_CONVERTER_ARGUMENTS As String = "AudioConverterArguments"
        ''' <summary>
        ''' Tag for the maximum time to wait for the conversion process to complete. Value: AudioConversionTimeout
        ''' </summary>
        ''' <remarks><para>Not relevant for internal conversion.</para>
        ''' <para>Value is in seconds.</para>
        ''' <para>The default is to time out after 2 minutes.</para></remarks>
        Protected Const XML_SETTINGS_AUDIO_CONVERSION_TIMEOUT As String = "AudioConversionTimeout"

        ''' <summary>
        ''' Create a new audio conversion configuration object.
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            MyBase.New(configuration, XML_SETTINGS_CONVERSION_SECTION_NAME)
        End Sub

        ''' <summary>
        ''' The class of audio converter to create.
        ''' <seealso cref="XML_SETTINGS_AUDIO_CONVERTER_TYPE"/>
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <value>The class of audio converter to create</value>
        ''' <returns>The Type to create</returns>
        ''' <remarks>The class must implement <see cref="IAudioConverter"/> and have a constructor that accepts <see cref="IConfigSection"/>.</remarks>
        Public Shared ReadOnly Property ConverterType(ByVal configuration As IConfigSection) As Type
            Get
                Dim convType As String = configuration(XML_SETTINGS_AUDIO_CONVERTER_TYPE)
                If String.IsNullOrEmpty(convType) Then
                    Return Nothing
                ElseIf convType.Equals("Internal", StringComparison.InvariantCultureIgnoreCase) Then
                    Return GetType(InternalConverter)
                ElseIf convType.Equals("External", StringComparison.InvariantCultureIgnoreCase) Then
                    Return GetType(ExternalConverter)
                Else
                    Return Type.GetType(convType)
                End If
            End Get
        End Property

        ''' <summary>
        ''' The audio format for the file to be converted.
        ''' <seealso cref="XML_SETTINGS_AUDIO_SOURCE_FORMAT"/>
        ''' </summary>
        ''' <value>The audio format</value>
        ''' <returns>The internal value for the audio format</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property SourceFormat() As utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat
            Get
                Return GetFormat(Me(XML_SETTINGS_AUDIO_SOURCE_FORMAT), utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat.WAV_MULAW)
            End Get
        End Property

        ''' <summary>
        ''' The audio format for the converted file.
        ''' <seealso cref="XML_SETTINGS_AUDIO_FORMAT"/>
        ''' </summary>
        ''' <value>The audio format</value>
        ''' <returns>The internal value for the audio format</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property TargetFormat() As utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat
            Get
                Return GetFormat(Me(XML_SETTINGS_AUDIO_FORMAT), utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat.WAV_PCM)
            End Get
        End Property

        ''' <summary>
        ''' The extension of the file to be converted.
        ''' </summary>
        ''' <value>The file extension</value>
        ''' <returns>The file extension based on the source format</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property SourceExtension() As String
            Get
                Return FormatToExtension(SourceFormat)
            End Get
        End Property

        ''' <summary>
        ''' The extension of the converted file.
        ''' </summary>
        ''' <value>The file extension</value>
        ''' <returns>The file extension based on the target format</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property TargetExtension() As String
            Get
                Return FormatToExtension(TargetFormat)
            End Get
        End Property

        ''' <summary>
        ''' Converts from the audio format string to the internal value.
        ''' </summary>
        ''' <param name="formatStr">The audio format string</param>
        ''' <param name="defaultValue">The value to return if the format string is not specified.</param>
        ''' <returns>The audio format internal value</returns>
        ''' <remarks>If the format string is specified, but not recogized, the function returns <c>UNKNOWN</c>.</remarks>
        Protected Function GetFormat(ByVal formatStr As String, ByVal defaultValue As utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat) As utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat
            If String.IsNullOrEmpty(formatStr) Then
                Return defaultValue
            Else
                If [Enum].IsDefined(GetType(utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat), formatStr) Then
                    Return CType([Enum].Parse(GetType(utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat), formatStr, True), utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat)
                Else
                    Return utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat.UNKNOWN
                End If
            End If
        End Function

        ''' <summary>
        ''' Converts from the audio format to the file extension.
        ''' </summary>
        ''' <param name="format">The internal audio format value</param>
        ''' <returns>The file extension</returns>
        ''' <remarks></remarks>
        Protected Function FormatToExtension(ByVal format As utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat) As String
            Select Case format
                Case utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat.MP3
                    Return "mp3"
                Case utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat.VOX
                    Return "vox"
                Case Else
                    Return "wav"
            End Select
        End Function

        ''' <summary>
        ''' The directory in which to run the conversion.
        ''' <seealso cref="XML_SETTINGS_AUDIO_CONVERTER_WORKING_DIR"/>
        ''' </summary>
        ''' <value>The working directory</value>
        ''' <returns>The working directory</returns>
        ''' <remarks>Not relevant for internal conversion.</remarks>
        Public Overridable ReadOnly Property WorkingDirectory() As String
            Get
                If Me(XML_SETTINGS_AUDIO_CONVERTER_WORKING_DIR) Is Nothing Then
                    Return ""
                Else
                    Return Me(XML_SETTINGS_AUDIO_CONVERTER_WORKING_DIR)
                End If
            End Get
        End Property

        ''' <summary>
        ''' The path to the converter executable.
        ''' <seealso cref="XML_SETTINGS_AUDIO_CONVERTER_PATH"/>
        ''' </summary>
        ''' <value>Converter executable's path</value>
        ''' <returns>Converter executable's path</returns>
        ''' <remarks>Not relevant for internal conversion.</remarks>
        Public Overridable ReadOnly Property ConverterPath() As String
            Get
                Return Me(XML_SETTINGS_AUDIO_CONVERTER_PATH)
            End Get
        End Property

        ''' <summary>
        ''' The arguments to give the converter executable.
        ''' <seealso cref="XML_SETTINGS_AUDIO_CONVERTER_ARGUMENTS"/>
        ''' </summary>
        ''' <value>Converter executable's arguments</value>
        ''' <returns>Converter executable's arguments</returns>
        ''' <remarks>Not relevant for internal conversion.</remarks>
        Public Overridable ReadOnly Property ConversionArguments() As String
            Get
                Return Me(XML_SETTINGS_AUDIO_CONVERTER_ARGUMENTS)
            End Get
        End Property

        ''' <summary>
        ''' The maximum time to wait for the conversion process to complete.
        ''' <seealso cref="XML_SETTINGS_AUDIO_CONVERSION_TIMEOUT"/>
        ''' </summary>
        ''' <value>Conversion process timeout</value>
        ''' <returns>Conversion process timeout</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property ConversionTimeout() As TimeSpan
            Get
                If Me(XML_SETTINGS_AUDIO_CONVERSION_TIMEOUT) Is Nothing Then
                    Return DefaultConversionTimeout
                Else
                    Return TimeSpan.FromSeconds(CDbl(Me(XML_SETTINGS_AUDIO_CONVERSION_TIMEOUT)))
                End If
            End Get
        End Property

        ''' <summary>
        ''' Override this to change the default conversion process timeout, used if the configuration does not specify it.
        ''' </summary>
        ''' <value>Default timeout for conversion process</value>
        ''' <returns>2 minutes</returns>
        ''' <remarks>Not relevant for internal conversion.</remarks>
        Protected Overridable ReadOnly Property DefaultConversionTimeout() As TimeSpan
            Get
                Return TimeSpan.FromMinutes(2)
            End Get
        End Property
    End Class
End Namespace