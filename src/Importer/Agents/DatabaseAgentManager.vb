﻿Imports utopy.bAgentsAgents

Namespace Agents
    ''' <summary>
    ''' An agent manager implementation that gets the agents using a database query.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class DatabaseAgentManager
        Inherits BaseAgentManager

        ''' <summary>
        ''' The database object to use
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_agentsDB As utopy.bDBServices.DBServices

        ''' <summary>
        ''' Constructor to create the agent manager with its configuration
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            MyBase.New(configuration)
            m_agentsDB = m_configuration.AgentDatabase()
            If m_agentsDB Is Nothing Then
                Throw New Exception("Agents database was not set")
            End If
        End Sub

        ''' <summary>
        ''' Get the list of agents.
        ''' </summary>
        ''' <returns>A hierarchical collection of utopy.bAgentsAgents.Agent objects</returns>
        ''' <remarks></remarks>
        Protected Overrides Function GetAgents() As ICollection(Of Agent)
            Dim dt = m_agentsDB.getDataTable(m_configuration.AgentsQuery)
            Dim agentData As New List(Of IAgentData)
            For Each row As DataRow In dt.Rows
                Dim data = GetAgentData(row)
                If data IsNot Nothing Then
                    agentData.Add(data)
                End If
            Next
            Return GetAgents(agentData)
        End Function

        ''' <summary>
        ''' Get the agent data from a database row.
        ''' </summary>
        ''' <param name="row">The database row</param>
        ''' <returns>Agent data object</returns>
        ''' <remarks>The row must have defined the columns for agent ID, agent name and supervisor ID</remarks>
        Protected Overridable Function GetAgentData(ByVal row As DataRow) As IAgentData
            Dim data As New AgentData()
            data.ID = CStr(row(m_configuration.AgentIdColumn))
            data.Name = CStr(row(m_configuration.AgentNameColumn))
            data.SupervisorID = CStr(row(m_configuration.SupervisorIdColumn))
            Return data
        End Function
    End Class
End Namespace