﻿Namespace Agents
    ''' <summary>
    ''' Configuration for the agent hierarchy updates
    ''' </summary>
    ''' <remarks></remarks>
    Public Class AgentConfig
        Inherits ConfigSection

        ''' <summary>
        ''' Tag for the agent hierarcy update section. Value: Agents
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_SETTINGS_AGENT_SECTION_NAME As String = "Agents"

        ''' <summary>
        ''' Tag for the fully qualified class name of the Agent Manager. Value: AgentManagerType
        ''' </summary>
        ''' <remarks><para>The class must implement <see cref="IAgentManager"/> and have a constructor that accepts <see cref="IConfigSection"/>.</para>
        ''' <para>You can also use <c>Database</c> or <c>File</c>. The default type is <c>Database</c>.</para></remarks>
        Protected Const XML_SETTINGS_AGENT_MANAGER_TYPE As String = XML_SETTINGS_AGENT_SECTION_NAME & Config.SETTINGS_SEPARATOR & "AgentManagerType"
        ''' <summary>
        ''' Tag for the fully qualified class name of the Agent Service. Value: AgentServiceType
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_SETTINGS_AGENT_SERVICE_TYPE As String = "AgentServiceType"
        ''' <summary>
        ''' Tag for how often the agent update should run. Value: AgentUpdateIntervalMinutes
        ''' </summary>
        ''' <remarks><para>Value is in minutes.</para>
        ''' <para>The default is to update once a day.</para></remarks>
        Protected Const XML_SETTINGS_AGENT_UPDATE_INTERVAL As String = "AgentUpdateIntervalMinutes"
        ''' <summary>
        ''' Tag for whether to update the entire hierarchy. Value: KeepOtherSiteAgents
        ''' </summary>
        ''' <remarks><para>If true, only the agent hierarchy from the current site/s will be updated, and the rest will stay as is.</para>
        ''' <para>The default is to update the entire hierarchy, i.e. not keep other sites.</para></remarks>
        Protected Const XML_SETTINGS_KEEP_OTHER_SITE_AGENTS As String = "KeepOtherSiteAgents"
        ''' <summary>
        ''' Tag for whether agent names must be kept unique. Value: ForceUniqueAgentNames
        ''' </summary>
        ''' <remarks><para>If this is false, you must make sure there is no uniqueness constraint in the AgentEntityTbl in the SpeechMiner database.</para>
        ''' <para>The default is not to force uniqueness.</para></remarks>
        Protected Const XML_SETTINGS_FORCE_UNIQUE_AGENT_NAMES As String = "ForceUniqueAgentNames"

        ''' <summary>
        ''' Tag for the database from which to get the agents. Value: AgentDatabaseName
        ''' </summary>
        ''' <remarks><para>Logical name, pointing to the connection string in the .config file.</para>
        ''' <para>Only relevant when the agent manager is reading from the database.</para></remarks>
        Protected Const XML_SETTINGS_AGENT_DATABASE_NAME As String = "AgentDatabaseName"

        Protected Const XML_SETTINGS_AGENTS_QUERY As String = "AgentQuery"
        ''' <summary>
        ''' Tag for the path to the file containing the agent list. Value: AgentFilePath
        ''' </summary>
        ''' <remarks>Only relevant when the agent manager is reading from a file.</remarks>
        Protected Const XML_SETTINGS_AGENT_FILE_PATH As String = "AgentFilePath"
        ''' <summary>
        ''' Tag for the separators to be used between columns in the agents file. Value: AgentFileColumnSeparators
        ''' </summary>
        ''' <remarks><para>The separators are the characters in the value.</para>
        ''' <para>The default separators are tabs and commas.</para>
        ''' <para>Only relevant when the agent manager is reading from a file.</para></remarks>
        Protected Const XML_SETTINGS_AGENT_COLUMN_SEPARATORS As String = "AgentFileColumnSeparators"

        ''' <summary>
        ''' Tag for the name of the agent ID column, in the database or in a file. Value: AgentIdColumnName
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_SETTINGS_AGENT_ID_COLUMN As String = "AgentIdColumnName"
        ''' <summary>
        ''' Tag for the name of the agent name column, in the database or in a file. Value: AgentNameColumnName
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_SETTINGS_AGENT_NAME_COLUMN As String = "AgentNameColumnName"
        ''' <summary>
        ''' Tag for the name of the agent's supervisor's ID column, in the database or in a file. Value: SupervisorIdColumnName
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_SETTINGS_SUPERVISOR_ID_COLUMN As String = "SupervisorIdColumnName"

        ''' <summary>
        ''' Create a new agent configuration object.
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            MyBase.New(configuration, XML_SETTINGS_AGENT_SECTION_NAME)
        End Sub

        ''' <summary>
        ''' The class of Agent Manager to create.
        ''' <seealso cref="XML_SETTINGS_AGENT_MANAGER_TYPE"/>
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <value>The class of Agent Manager to create</value>
        ''' <returns>The Type to create</returns>
        ''' <remarks>The class must implement <see cref="IAgentManager"/> and have a constructor that accepts <see cref="IConfigSection"/>.</remarks>
        Public Shared ReadOnly Property ManagerType(ByVal configuration As IConfigSection) As Type
            Get
                Dim mgrType As String = configuration(XML_SETTINGS_AGENT_MANAGER_TYPE)
                If String.IsNullOrEmpty(mgrType) Then
                    Return Nothing
                ElseIf mgrType.Equals("Database", StringComparison.InvariantCultureIgnoreCase) Then
                    Return GetType(DatabaseAgentManager)
                ElseIf mgrType.Equals("File", StringComparison.InvariantCultureIgnoreCase) Then
                    Return GetType(FileAgentManager)
                Else
                    Return Type.GetType(mgrType)
                End If
            End Get
        End Property


        ''' <summary>
        ''' The class of Agent Manager to create.
        ''' <seealso cref="XML_SETTINGS_AGENT_MANAGER_TYPE"/>
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <value>The class of Agent Service to create</value>
        ''' <returns>The Type to create</returns>
        ''' <remarks></remarks>
        Public Shared ReadOnly Property ServiceType(ByVal configuration As IConfigSection) As Type
            Get
                Dim serType As String = configuration(XML_SETTINGS_AGENT_SERVICE_TYPE)
                If String.IsNullOrEmpty(serType) Then
                    Return Nothing
                Else
                    Return Type.GetType(serType)
                End If
            End Get
        End Property



        ''' <summary>
        ''' How often should the agent update run?
        ''' <seealso cref="XML_SETTINGS_AGENT_UPDATE_INTERVAL"/>
        ''' </summary>
        ''' <value>The interval for updates</value>
        ''' <returns>A TimeSpan containing the update interval</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property AgentUpdateInterval() As TimeSpan
            Get
                If String.IsNullOrEmpty(Me(XML_SETTINGS_AGENT_UPDATE_INTERVAL)) Then
                    Return DEFAULT_UPDATE_INTERVAL
                Else
                    Return TimeSpan.FromMinutes(CDbl(Me(XML_SETTINGS_AGENT_UPDATE_INTERVAL)))
                End If
            End Get
        End Property


        ''' <summary>
        ''' Whether to update the entire hierarchy or just specific sites?
        ''' <seealso cref="XML_SETTINGS_KEEP_OTHER_SITE_AGENTS"/>
        ''' </summary>
        ''' <value>Whether to keep agents from other sites</value>
        ''' <returns>Whether to keep agents from other sites</returns>
        ''' <remarks>If true, only the agent hierarchy from the current site/s will be updated, and the rest will stay as is.</remarks>
        Public Overridable ReadOnly Property KeepOtherSiteAgents() As Boolean
            Get
                If String.IsNullOrEmpty(Me(XML_SETTINGS_KEEP_OTHER_SITE_AGENTS)) Then
                    Return False
                Else
                    Return CBool(Me(XML_SETTINGS_KEEP_OTHER_SITE_AGENTS))
                End If
            End Get
        End Property

        ''' <summary>
        ''' Whether agent names must be kept unique?
        ''' <seealso cref="XML_SETTINGS_FORCE_UNIQUE_AGENT_NAMES"/>
        ''' </summary>
        ''' <value>Whether to force agent uniqueness</value>
        ''' <returns>Whether to force agent uniqueness</returns>
        ''' <remarks>If this is false, you must make sure there is no uniqueness constraint in the AgentEntityTbl in the SpeechMiner database.</remarks>
        Public Overridable ReadOnly Property ForceUniqueAgentNames() As Boolean
            Get
                If String.IsNullOrEmpty(Me(XML_SETTINGS_FORCE_UNIQUE_AGENT_NAMES)) Then
                    Return False
                Else
                    Return CBool(Me(XML_SETTINGS_FORCE_UNIQUE_AGENT_NAMES))
                End If
            End Get
        End Property

        Public ReadOnly Property AgentDatabase() As utopy.bDBServices.DBServices
            Get
                Dim dbName = Me.AgentDatabaseName

                Return If(String.IsNullOrWhiteSpace(dbName) = True,
                          ConfigManager.GetDatabase(dbName), Nothing)
            End Get
        End Property

        Public ReadOnly Property AgentDatabaseName() As String
            Get
                Return Me(XML_SETTINGS_AGENT_DATABASE_NAME)
            End Get
        End Property

        Public Overridable ReadOnly Property AgentsQuery() As String
            Get
                Return Me(XML_SETTINGS_AGENTS_QUERY)
            End Get
        End Property

        ''' <summary>
        ''' The path to the file containing the agent list.
        ''' <seealso cref="XML_SETTINGS_AGENT_FILE_PATH"/>
        ''' </summary>
        ''' <value>The agent file path</value>
        ''' <returns>The agent file path</returns>
        ''' <remarks>Only relevant when the agent manager is reading from a file.</remarks>
        Public Overridable ReadOnly Property AgentFile() As String
            Get
                Return Me(XML_SETTINGS_AGENT_FILE_PATH)
            End Get
        End Property

        ''' <summary>
        ''' The separators to be used between columns in the agents file.
        ''' <seealso cref="XML_SETTINGS_AGENT_COLUMN_SEPARATORS"/>
        ''' </summary>
        ''' <value>The separator characters</value>
        ''' <returns>The separator characters</returns>
        ''' <remarks>Only relevant when the agent manager is reading from a file.</remarks>
        Public Overridable ReadOnly Property AgentFileColumnSeparators() As Char()
            Get
                If String.IsNullOrEmpty(Me(XML_SETTINGS_AGENT_COLUMN_SEPARATORS)) Then
                    Return DefaultColumnSeparators
                Else
                    Return Me(XML_SETTINGS_AGENT_COLUMN_SEPARATORS).ToCharArray()
                End If
            End Get
        End Property

        ''' <summary>
        ''' Override this to change the default agents file column separators, when the configuration does not specify them.
        ''' </summary>
        ''' <value>The column separators</value>
        ''' <returns>A tab and a comma</returns>
        ''' <remarks>Only relevant when the agent manager is reading from a file.</remarks>
        Public Overridable ReadOnly Property DefaultColumnSeparators() As Char()
            Get
                Return New Char() {vbTab(0), ","c}
            End Get
        End Property

        ''' <summary>
        ''' The name of the agent ID column, in the database or in a file.
        ''' <seealso cref="XML_SETTINGS_AGENT_ID_COLUMN"/>
        ''' </summary>
        ''' <value>The name of the column</value>
        ''' <returns>The value in the configuration, or the default value</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property AgentIdColumn() As String
            Get
                If String.IsNullOrEmpty(Me(XML_SETTINGS_AGENT_ID_COLUMN)) Then
                    Return AgentIdDefaultColumn
                Else
                    Return Me(XML_SETTINGS_AGENT_ID_COLUMN)
                End If
            End Get
        End Property

        ''' <summary>
        ''' Override this to change the default agent ID column name, when the configuration does not specify it.
        ''' </summary>
        ''' <value>The column name</value>
        ''' <returns>"AgentId"</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property AgentIdDefaultColumn() As String
            Get
                Return "AgentId"
            End Get
        End Property

        ''' <summary>
        ''' The name of the agent name column, in the database or in a file.
        ''' <seealso cref="XML_SETTINGS_AGENT_NAME_COLUMN"/>
        ''' </summary>
        ''' <value>The name of the column</value>
        ''' <returns>The value in the configuration, or the default value</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property AgentNameColumn() As String
            Get
                If String.IsNullOrEmpty(Me(XML_SETTINGS_AGENT_NAME_COLUMN)) Then
                    Return AgentNameDefaultColumn
                Else
                    Return Me(XML_SETTINGS_AGENT_NAME_COLUMN)
                End If
            End Get
        End Property

        ''' <summary>
        ''' Override this to change the default agent name column name, when the configuration does not specify it.
        ''' </summary>
        ''' <value>The column name</value>
        ''' <returns>"AgentName"</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property AgentNameDefaultColumn() As String
            Get
                Return "AgentName"
            End Get
        End Property

        ''' <summary>
        ''' The name of the agent's supervisor's ID column, in the database or in a file.
        ''' <seealso cref="XML_SETTINGS_SUPERVISOR_ID_COLUMN"/>
        ''' </summary>
        ''' <value>The name of the column</value>
        ''' <returns>The value in the configuration, or the default value</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property SupervisorIdColumn() As String
            Get
                If String.IsNullOrEmpty(Me(XML_SETTINGS_SUPERVISOR_ID_COLUMN)) Then
                    Return SupervisorIdDefaultColumn
                Else
                    Return Me(XML_SETTINGS_SUPERVISOR_ID_COLUMN)
                End If
            End Get
        End Property

        ''' <summary>
        ''' Override this to change the default agent's supervisor's ID column name, when the configuration does not specify it.
        ''' </summary>
        ''' <value>The column name</value>
        ''' <returns>"SupervisorId"</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property SupervisorIdDefaultColumn() As String
            Get
                Return "SupervisorId"
            End Get
        End Property

#Region "Class Data"

        Private ReadOnly DEFAULT_UPDATE_INTERVAL As TimeSpan = TimeSpan.FromDays(1)

#End Region

    End Class
End Namespace