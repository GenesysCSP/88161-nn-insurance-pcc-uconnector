﻿Namespace Agents
    ''' <summary>
    ''' Holds information about a single agent.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class AgentData
        Implements IAgentData

        ' ''' <summary>
        ' ''' Key for holding the agent's ID
        ' ''' </summary>
        ' ''' <remarks></remarks>
        'Protected Const DATA_AGENT_ID As String = "AgentId"
        ' ''' <summary>
        ' ''' Key for holding the agent's name
        ' ''' </summary>
        ' ''' <remarks></remarks>
        'Protected Const DATA_AGENT_NAME As String = "AgentName"
        ' ''' <summary>
        ' ''' Key for holding the agent's supervisor's ID
        ' ''' </summary>
        ' ''' <remarks></remarks>
        'Protected Const DATA_SUPERVISOR_ID As String = "SupervisorId"

        ' ''' <summary>
        ' ''' Dictionary to hold the agent's properties
        ' ''' </summary>
        ' ''' <remarks></remarks>
        'Protected m_data As IDictionary(Of String, String)

        Public Sub New()
            'm_data = New Dictionary(Of String, String)
        End Sub

        Public Property ID As String Implements IAgentData.ID
            Get
                Return _id
            End Get
            Set(value As String)
                _id = value
            End Set
        End Property

        Public Property Name As String Implements IAgentData.Name
            Get
                Return _name
            End Get
            Set(value As String)
                _name = value
            End Set
        End Property

        Public Property SupervisorID() As String Implements IAgentData.SupervisorID
            Get
                Return _supervisorId
            End Get
            Set(ByVal value As String)
                _supervisorId = value
            End Set
        End Property

        ' ''' <summary>
        ' ''' Accessor for the agent's properties
        ' ''' </summary>
        ' ''' <param name="name">The name of the property</param>
        ' ''' <value>The value of the property</value>
        ' ''' <returns>The value of the property from the dictionary</returns>
        ' ''' <remarks></remarks>
        'Default Public Property Value(ByVal name As String) As String Implements IAgentData.Value
        '  Get
        '    If m_data.ContainsKey(name) Then
        '      Return m_data(name)
        '    Else
        '      Return Nothing
        '    End If
        '  End Get
        '  Set(ByVal value As String)
        '    m_data(name) = value
        '  End Set
        'End Property

#Region "Instance Data"

        Private _id As String
        Private _name As String
        Private _supervisorId As String

#End Region

    End Class
End Namespace