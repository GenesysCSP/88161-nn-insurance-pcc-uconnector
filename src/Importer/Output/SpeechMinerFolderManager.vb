﻿Namespace Output
  ''' <summary>
  ''' Manage the UConnector's output folders based on SpeechMiner's input folders
  ''' </summary>
  ''' <remarks></remarks>
  Public Class SpeechMinerFolderManager
    Inherits FolderManagerImpl

    ''' <summary>
    ''' The SpeechMiner database
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_db As utopy.bDBServices.DBServices

    ''' <summary>
    ''' Create a folder manager, reading the folders from the SpeechMiner database.
    ''' </summary>
    ''' <param name="configuration">The whole UConnector configuration</param>
    ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            MyBase.New(configuration)
            Dim importerConfig As New ImporterConfig(configuration)
            m_db = ConfigManager.GetDatabase(importerConfig.SpeechMinerDBName)
        End Sub

    Protected Overrides Sub Init()
      Dim spaceDt As DataTable = m_db.getDataTable("select minimumFolderSpaceMB, desiredFolderSpaceMB from monitorTbl with (nolock)")
      Dim dr As DataRow = spaceDt.Rows(0)
      Init(m_db.getColumnAsArray(Of String)("select path from inputFolderTbl"), CDbl(dr("minimumFolderSpaceMB")) / 1024, CDbl(dr("desiredFolderSpaceMB")) / 1024)
    End Sub
  End Class
End Namespace