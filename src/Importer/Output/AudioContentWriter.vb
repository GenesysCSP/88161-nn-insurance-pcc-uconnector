﻿Imports System.IO
Imports GenUtils.TraceTopic


Namespace Output
    ''' <summary>
    ''' Writes a call's audio content to the UConnector's output folder
    ''' </summary>
    ''' <remarks></remarks>
    Public Class AudioContentWriter
        Implements IContentWriter

        Private m_downloader As IContentDownloader
        Private m_converter As IAudioConverter
        ''' <summary>
        ''' Create an audio content writer
        ''' </summary>
        ''' <param name="downloader">Downloader for getting the audio content file</param>
        ''' <param name="converter">Converter for changing the format of the audio file</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal downloader As IContentDownloader, ByVal converter As IAudioConverter)
            m_downloader = downloader
            m_converter = converter
        End Sub

        ''' <summary>
        ''' Write the audio content file for the interaction to the output folder
        ''' </summary>
        ''' <param name="folder">The output folder</param>
        ''' <param name="interaction">The interaction data</param>
        ''' <returns>True if the file was sucessfully written</returns>
        ''' <remarks></remarks>
        Public Overridable Function GenerateContentFile(ByVal folder As String, ByVal interaction As IInteractionData) As Boolean Implements IContentWriter.GenerateContentFile
            Dim segmentedInteraction As ISegmentedInteractionData = TryCast(interaction, ISegmentedInteractionData)
            If segmentedInteraction IsNot Nothing Then
                Return GenerateSegmentedContentFile(folder, segmentedInteraction)
            End If
            Dim audioInteraction As IAudioInteractionData = TryCast(interaction, IAudioInteractionData)
            Dim targetPath As String = Path.Combine(folder, audioInteraction.ID) & "." & AudioExtension
            Dim downloadTarget As String
            If m_converter Is Nothing Then
                downloadTarget = targetPath
            Else
                downloadTarget = FileUtils.GetTempPath(m_converter.SourceExtension)
            End If
            If Not m_downloader.GetFile(audioInteraction, downloadTarget) Then
                Return False
            End If
            If m_converter Is Nothing Then
                Return True
            Else
                Return m_converter.Convert(downloadTarget, targetPath, 0, audioInteraction.EncryptionKeyID)
            End If
        End Function
        ''' <summary>
        ''' Write the segmented audio content file for the interaction to the output folder
        ''' </summary>
        ''' <param name="folder">The output folder</param>
        ''' <param name="segmentedInteraction">The segmented interaction data</param>
        ''' <returns>True if the file was sucessfully written</returns>
        ''' <remarks></remarks>
        Protected Overridable Function GenerateSegmentedContentFile(ByVal folder As String, ByVal segmentedInteraction As ISegmentedInteractionData) As Boolean
            Dim ret As Boolean
            Dim targetPath As String
            Dim downloadTarget As String
            Dim audioInteraction As IAudioInteractionData
            Dim index As Integer = 0
            Dim encryptionKeys As New List(Of Integer)()
            For Each segment As IInteractionData In segmentedInteraction.InteractionMetaFiles
                audioInteraction = CType(segment, IAudioInteractionData)
                targetPath = FileUtils.GetTempPath(m_converter.SourceExtension)
                If m_converter Is Nothing Then
                    downloadTarget = targetPath
                Else
                    downloadTarget = FileUtils.GetTempPath(m_converter.SourceExtension)
                End If
                If Not m_downloader.GetFile(audioInteraction, downloadTarget) Then
                    Return False
                End If
                If m_converter IsNot Nothing Then
                    Dim encryptionKeyID As Integer = 0
                    ret = m_converter.Convert(downloadTarget, targetPath, 0, encryptionKeyID)
                    If Not ret Then
                        Return False
                    End If
                    encryptionKeys.Add(encryptionKeyID)
                End If
                segmentedInteraction.ContentFiles(index).ContentFilePath = targetPath
                index += 1
            Next
            ret = ret AndAlso StitchAudios(segmentedInteraction.ContentFiles, folder, IO.Path.ChangeExtension(segmentedInteraction.ID, AudioExtension), _
                                             encryptionKeys, segmentedInteraction.InteractionMetaFiles(0).EncryptionKeyID)
            Return ret
        End Function
        ''' <summary>
        ''' Stitch the segmented audio content files together, and generate the full content file for the interaction in the output folder 
        ''' </summary>
        ''' <param name="audioFilesToMerge">Collection od audios to get stitched</param>
        ''' <param name="folder">The output folder</param>
        ''' <param name="targetFile">The target file name</param>
        ''' <param name="keyToReadWith">The list of encryption keys to read the content files with</param>
        ''' <param name="keyToWriteWith">The encryption key to write the full content file with</param>
        ''' <returns>True if the file was sucessfully written</returns>
        ''' <remarks></remarks>
        Protected Overridable Function StitchAudios(ByVal audioFilesToMerge As ICollection(Of ISegmentedContentFile), ByVal folder As String, ByVal targetFile As String, ByVal keyToReadWith As IList(Of Integer), ByVal keyToWriteWith As Integer) As Boolean
            Dim targetPath As String = Path.Combine(folder, targetFile)
            Dim fullAudio As New utopy.aAudioUaudio.UAudio()
            Dim prevEndTime As DateTime
            Dim sourceFolder As String = ""
            Dim index As Integer = 0
            Using (TraceTopic.importerTopic.scope())
                For Each segment As ISegmentedContentFile In audioFilesToMerge
                    sourceFolder = Path.GetDirectoryName(segment.ContentFilePath)
                    Dim partialAudio As New utopy.aAudioUaudio.UAudio()
                    Dim partialAudioPath As String = segment.ContentFilePath
                    If partialAudio.readFile(partialAudioPath, utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat.WAV_PCM, 8000, keyToReadWith(index)) = utopy.aAudioUaudio.FileStatus.OK Then
                        segment.EndTime = segment.StartTime.AddSeconds(partialAudio.lengthInSec)
                        If index = 0 Then
                            fullAudio = partialAudio
                        Else
                            Dim gap As Double = (segment.StartTime - prevEndTime).TotalSeconds
                            If gap < 0 Then
                                TraceTopic.importerTopic.warning("Found overlap in {}", partialAudioPath)
                            Else
                                fullAudio = utopy.aAudioUaudio.UAudio.stitchAudio(fullAudio, partialAudio, gap)
                            End If
                        End If
                        prevEndTime = segment.EndTime
                    Else
                        TraceTopic.importerTopic.warning("Failed to read {}", partialAudioPath)
                    End If
                    Try
                        File.Delete(partialAudioPath)
                    Catch
                        TraceTopic.importerTopic.warning("Failed to delete {}", partialAudioPath)
                    End Try
                    index += 1
                Next
                If fullAudio.lengthInSec > 0 Then
                    Return fullAudio.writeFile(targetPath, keyToWriteWith)
                End If
            End Using
            Return False
        End Function

        ''' <summary>
        ''' The extension for the audio file.
        ''' </summary>
        ''' <value><c>wav</c></value>
        ''' <returns><c>wav</c></returns>
        ''' <remarks>Since SpeechMiner almost always accepts wav files, we just return this extension. Override if you need anything else.</remarks>
        Protected Overridable ReadOnly Property AudioExtension() As String
            Get
                Return "wav"
            End Get
        End Property
    End Class
End Namespace