﻿Namespace Monitoring
  ''' <summary>
  ''' Configuration for UConnector monitor
  ''' </summary>
  ''' <remarks></remarks>
  Public Class MonitorConfiguration
    Inherits ConfigurationSection
        Implements IConfigSection

    ''' <summary>
    ''' Tag for the monitoring section. Value: Monitor
    ''' </summary>
    ''' <remarks></remarks>
    Protected Const XML_SETTINGS_MONITOR_SECTION_NAME As String = "Monitor"

    ''' <summary>
    ''' Tag for the name of the table to be used for logging interactions. Value: InteractionLogTable
    ''' </summary>
    ''' <remarks></remarks>
    Protected Const XML_SETTINGS_INTERACTION_LOG_TABLE As String = "InteractionLogTable"

    ''' <summary>
    ''' Tag for the list of interaction fields to log. List is comma separated. Value: InteractionLogFields
    ''' </summary>
    ''' <remarks></remarks>
    Protected Const XML_SETTINGS_INTERACTION_LOG_FIELDS As String = "InteractionLogFields"

    ''' <summary>
    ''' Tag for how long to keep the interaction log. Value: InteractionLogPeriod
    ''' </summary>
    ''' <remarks><para>Value is in days.</para>
    ''' <para>The default is 1 week.</para></remarks>
    Protected Const XML_SETTINGS_INTERACTION_LOG_PERIOD As String = "InteractionLogPeriod"

    ''' <summary>
    ''' The name of the configuration
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_configurationName As String

    ''' <summary>
    ''' The class of the importer
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_importerType As String

    ''' <summary>
    ''' Create a new monitoring configuration object.
    ''' </summary>
    ''' <param name="configuration">The entire UConnector configuration</param>
    ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfig)
            MyBase.New(configuration, XML_SETTINGS_MONITOR_SECTION_NAME)
            m_configurationName = configuration.Name
            m_importerType = configuration.ImporterType
        End Sub

    ''' <summary>
    ''' The name of the configuration
    ''' </summary>
    ''' <value>The name of the configuration</value>
    ''' <returns>The name of the configuration</returns>
    ''' <remarks></remarks>
    Public Overridable ReadOnly Property Name() As String
      Get
        Return m_configurationName
      End Get
    End Property

    ''' <summary>
    ''' The fully qualified class name of the importer
    ''' </summary>
    ''' <value>The importer type</value>
    ''' <returns>The importer type</returns>
    ''' <remarks></remarks>
    Public Overridable ReadOnly Property ImporterType() As String
      Get
        Return m_importerType
      End Get
    End Property

    ''' <summary>
    ''' The name of the table to be used for logging interactions.
    ''' <seealso cref="XML_SETTINGS_INTERACTION_LOG_TABLE"/>
    ''' </summary>
    ''' <value>The interaction log table name</value>
    ''' <returns>The interaction log table name</returns>
    ''' <remarks></remarks>
    Public Overridable ReadOnly Property InteractionLogTable() As String
      Get
        Return Me(XML_SETTINGS_INTERACTION_LOG_TABLE)
      End Get
    End Property

    ''' <summary>
    ''' The list of interaction fields to log.
    ''' </summary>
    ''' <value>Comma-separated list of fields</value>
    ''' <returns>Comma-separated list of fields</returns>
    ''' <remarks></remarks>
    Public Overridable ReadOnly Property InteractionLogFields() As ICollection(Of String)
      Get
        If String.IsNullOrEmpty(Me(XML_SETTINGS_INTERACTION_LOG_FIELDS)) Then
          Return Nothing
        Else
          Return Me(XML_SETTINGS_INTERACTION_LOG_FIELDS).Split(","c)
        End If
      End Get
    End Property

    ''' <summary>
    ''' How long to keep the interaction log.
    ''' <seealso cref="XML_SETTINGS_INTERACTION_LOG_PERIOD"/>
    ''' </summary>
    ''' <value>The period of time to keep the log</value>
    ''' <returns>A TimeSpan containing the log period</returns>
    ''' <remarks></remarks>
    Public Overridable ReadOnly Property InteractionLogPeriod() As TimeSpan
      Get
        If String.IsNullOrEmpty(Me(XML_SETTINGS_INTERACTION_LOG_PERIOD)) Then
          Return DefaultInteractionLogPeriod
        Else
          Return TimeSpan.FromDays(CDbl(Me(XML_SETTINGS_INTERACTION_LOG_PERIOD)))
        End If
      End Get
    End Property

    ''' <summary>
    ''' Override this to change the default amount of time to keep interaction logs, used if the configuration does nto specify it.
    ''' </summary>
    ''' <value>The default time to keep the log</value>
    ''' <returns>One week</returns>
    ''' <remarks></remarks>
    Protected Overridable ReadOnly Property DefaultInteractionLogPeriod() As TimeSpan
      Get
        Return TimeSpan.FromDays(7)
      End Get
    End Property
  End Class
End Namespace
