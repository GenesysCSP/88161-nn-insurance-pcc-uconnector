﻿Namespace Monitoring
  ''' <summary>
  ''' Monitor the UConnector state
  ''' </summary>
  ''' <remarks></remarks>
  Public Interface IMonitor
    ''' <summary>
    ''' Update when the UConnector state changes
    ''' </summary>
    ''' <param name="status">The new importer state</param>
    ''' <param name="description">A textual description of the state</param>
    ''' <remarks></remarks>
    Sub UpdateStatus(ByVal status As IImporter.Status, ByVal description As String)
    ''' <summary>
    ''' Update when the importer has handled an interaction
    ''' </summary>
    ''' <param name="interaction">The interaction's data</param>
    ''' <param name="status">The status of the interaction's processing</param>
    ''' <remarks></remarks>
    Sub UpdateInteraction(ByVal interaction As IInteractionData, ByVal status As IItemStatus)
    ''' <summary>
    ''' Start monitoring the UConnector
    ''' </summary>
    ''' <remarks></remarks>
    Sub StartMonitor()
    ''' <summary>
    ''' Stop monitoring the UConnector
    ''' </summary>
    ''' <remarks></remarks>
    Sub StopMonitor()
  End Interface
End Namespace