﻿Imports System.ComponentModel

Class TestImporter
    Inherits Importer

    Private m_folder As String
    Private m_fileIndex As Integer = 0
    Private m_dataTable As DataTable

    Private Const XML_SETTINGS_INPUT_FOLDER As String = "InputFolder"

    Public Sub New(ByVal isi As ISynchronizeInvoke, ByVal _configFile As String)
        MyBase.New(isi, _configFile)
        m_dataTable = New DataTable
        m_dataTable.Columns.Add(RESULTS_COLUMN_OUT_PATH)
        m_dataTable.Columns.Add(RESULTS_COLUMN_CALL_ID)
        m_dataTable.Columns.Add(RESULTS_COLUMN_STARTED_AT)
        m_dataTable.Columns.Add(RESULTS_COLUMN_SOURCE_PATH)
    End Sub

    Public Overrides Function generateAudioFile(ByVal _filename As String, Optional ByVal _outPath As String = "", Optional ByVal _inum As String = "", Optional ByVal _recorderPath As String = "", Optional ByVal _callTime As String = Nothing, Optional ByVal m_dataRow As System.Data.DataRow = Nothing) As Boolean
        copyFile(_filename, _outPath)
        Return True
    End Function

    Private Sub copyFile(ByVal _source As String, ByVal _target As String)
        If System.IO.File.Exists(_source) Then
            Dim file As New System.IO.FileInfo(_source)
            Dim audioFileInfo As IO.FileInfo
            If m_AudioKeyId > 0 Then
                utopy.bSecurityFilecrypto.FileCrypto.encryptPath(_source, _target, m_AudioKeyId)
                audioFileInfo = New IO.FileInfo(_target)
            Else
                audioFileInfo = file.CopyTo(_target, True)
            End If
            audioFileInfo.Attributes = IO.FileAttributes.Normal
            file.Refresh()
        End If
    End Sub

    Public Overrides Sub generateMetaFile(ByVal _filename As String, ByVal _callRow As DataRow)
        Dim f As New IO.FileInfo(_filename)
        Dim xmlFileName As String = f.FullName.Replace(f.Extension, ".xml")
        Dim metaFile As Xml.XmlTextWriter = Nothing
        Try
            metaFile = New Xml.XmlTextWriter(xmlFileName, System.Text.Encoding.ASCII)
            m_leftChannelSpeaker = XML_OUTPUT_SPEAKER_TYPE_AGENT
            m_rightChannelSpeaker = "customer"
            generateMetaFile(metaFile, _callRow, "")

            metaFile.WriteElementString(XML_OUTPUT_CALLTIME, CDate(_callRow(RESULTS_COLUMN_STARTED_AT)).ToString("s"))
            metaFile.WriteElementString(XML_OUTPUT_AUDIOFORMAT, utopy.bDBOffline.audioConversionType.InternalSupportedAudioFormat.WAV_PCM.ToString())

            metaFile.WriteEndElement() 'callInformation
            metaFile.WriteEndDocument()
            metaFile.Close()
        Catch ex As Exception
            m_logger.WARNING("Error in generating " & xmlFileName, ex)
            If Not metaFile Is Nothing Then
                metaFile.Close()
            End If
        End Try
    End Sub

    Protected Overrides Sub loadXML()
        MyBase.loadXML()
        Dim xmlDoc As New Xml.XmlDocument
        xmlDoc.Load(m_configFile)

        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_WORKGROUP) IsNot Nothing Then
            m_workgroup = CStr(xmlDoc(XML_SETTINGS)(XML_SETTINGS_WORKGROUP).InnerText)
        End If

        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_INPUT_FOLDER).InnerText Is Nothing Then
            logOnce("Config file does not contain InputFolder tag")
        Else
            m_folder = CStr(xmlDoc(XML_SETTINGS)(XML_SETTINGS_INPUT_FOLDER).InnerText)
        End If
    End Sub

    Public Overrides Function getNumberOfWaitingCalls() As Integer
        Dim di As New IO.DirectoryInfo(m_folder)
        Return di.GetFiles().Count()
    End Function

    Private Function getDataRow(ByVal _file As IO.FileInfo) As DataRow
        Dim row As DataRow = m_dataTable.NewRow()
        row(RESULTS_COLUMN_OUT_PATH) = m_outputFolder(0) & "\" & _file.Name
        row(RESULTS_COLUMN_CALL_ID) = _file.Name.Substring(0, _file.Name.Length - _file.Extension.Length)
        row(RESULTS_COLUMN_STARTED_AT) = DateTime.Now
        row(RESULTS_COLUMN_SOURCE_PATH) = _file.FullName
        Return row
    End Function

    Protected Overrides Function importNextBatch() As Boolean
        Dim di As New IO.DirectoryInfo(m_folder)
        Dim files As IO.FileInfo() = di.GetFiles()
        For i As Integer = 0 To m_batchSize
            If m_fileIndex >= files.Count Then
                Return i > 0
            End If
            Dim row As DataRow = getDataRow(files(i))
            generateFromDataRow(row)
            m_fileIndex += 1
        Next
        Return True
    End Function

    Public Overrides Sub quit()

    End Sub
End Class
