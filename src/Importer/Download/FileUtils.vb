﻿Imports System.IO

Namespace Download
  ''' <summary>
  ''' Utility class for file related operations
  ''' </summary>
  ''' <remarks></remarks>
  Public NotInheritable Class FileUtils

    Private Shared m_tempLock As New Object()

    Private Sub New()
    End Sub

    ''' <summary>
    ''' Get a file path for temporary storage
    ''' </summary>
    ''' <param name="extension">The requested extension for the file</param>
    ''' <returns>Temporary file path</returns>
    ''' <remarks></remarks>
    Public Shared Function GetTempPath(ByVal extension As String) As String
      Dim tempPath As String
      SyncLock m_tempLock
        tempPath = Path.GetTempFileName()
      End SyncLock
      File.Delete(tempPath)
      If extension IsNot Nothing Then
        tempPath = Path.ChangeExtension(tempPath, extension)
      End If
      Return tempPath
    End Function

  End Class
End Namespace