﻿Namespace Download
    ''' <summary>
    ''' Configuration for interaction content download
    ''' </summary>
    ''' <remarks></remarks>
    Public Class DownloadConfig
        Inherits ConfigSection
        Implements IConfigSection

        ''' <summary>
        ''' Tag for the interaction content download section. Value: Download
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_SETTINGS_DOWNLOAD_SECTION_NAME As String = "Download"

        ''' <summary>
        ''' Tag for the fully qualified class name for the interaction content downloader. Value: DownloaderType
        ''' </summary>
        ''' <remarks><para>The class must implement <see cref="IContentDownloader"/> and have a constructor that accepts <see cref="IConfigSection"/>.</para>
        ''' <para>You can also use <c>File</c> for copying from the file system. This is also the default type.</para></remarks>
        Protected Const XML_SETTINGS_DOWNLOADER_TYPE As String = XML_SETTINGS_DOWNLOAD_SECTION_NAME & Config.SETTINGS_SEPARATOR & "DownloaderType"

        ''' <summary>
        ''' Tag for the file server. Value: Host
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_SETTINGS_DOWNLOAD_HOST As String = "Host"

        ''' <summary>
        ''' Tag for the user name with which to connect to the file server. Value: User
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_SETTINGS_DOWNLOAD_USER As String = "User"

        ''' <summary>
        ''' Tag for the file server user's password. Value: Password
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_SETTINGS_DOWNLOAD_PASSWORD As String = "Password"

        ''' <summary>
        ''' Create a new interaction content downloader.
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            MyBase.New(configuration, XML_SETTINGS_DOWNLOAD_SECTION_NAME)
        End Sub

        ''' <summary>
        ''' The class of interaction content downloader to create.
        ''' <seealso cref="XML_SETTINGS_DOWNLOADER_TYPE"/>
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <value>The class of interaction content downloader to create</value>
        ''' <returns>The Type to create</returns>
        ''' <remarks>The class must implement <see cref="IContentDownloader"/> and have a constructor that accepts <see cref="IConfigSection"/>.</remarks>
        Public Shared ReadOnly Property ConverterType(ByVal configuration As IConfigSection) As Type
            Get
                Dim dlType As String = configuration(XML_SETTINGS_DOWNLOADER_TYPE)
                If String.IsNullOrEmpty(dlType) OrElse dlType.Equals("File", StringComparison.InvariantCultureIgnoreCase) Then
                    Return GetType(FileSystemDownloader)
                Else
                    Return Type.GetType(dlType)
                End If
            End Get
        End Property

        ''' <summary>
        ''' The file server
        ''' </summary>
        ''' <value>File server name or IP</value>
        ''' <returns>File server name or IP</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property Host() As String
            Get
                Return Me(XML_SETTINGS_DOWNLOAD_HOST)
            End Get
        End Property

        ''' <summary>
        ''' The user name with which to connect to the file server
        ''' </summary>
        ''' <value>User name</value>
        ''' <returns>The user name</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property User() As String
            Get
                Return Me(XML_SETTINGS_DOWNLOAD_USER)
            End Get
        End Property

        ''' <summary>
        ''' The password for the user with which to connect to the file server
        ''' </summary>
        ''' <value>Password</value>
        ''' <returns>The password</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property Password() As String
            Get
                Return Me(XML_SETTINGS_DOWNLOAD_PASSWORD)
            End Get
        End Property
    End Class
End Namespace