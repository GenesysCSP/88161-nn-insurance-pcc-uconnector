﻿Imports System.Threading

Public Class CountdownLatch
  Private m_remain As Integer
  Private m_event As EventWaitHandle

  Public Sub New(ByVal count As Integer)
    m_remain = count
    m_event = New ManualResetEvent(False)
  End Sub

  Public Sub Signal()
    ' The last thread to signal also sets the event.
    If Interlocked.Decrement(m_remain) = 0 Then
      m_event.Set()
    End If
  End Sub

  Public Sub Wait()
    m_event.WaitOne()
  End Sub
End Class
