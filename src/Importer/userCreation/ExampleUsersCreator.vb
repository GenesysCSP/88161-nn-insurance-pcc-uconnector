Public Class ExampleUsersCreator
  Inherits UsersCreator

  Public Sub New(ByVal _dbMaster As utopy.bDBServices.DBServices, ByVal _roleProviderName As String, ByVal _membershipProviderName As String, ByVal _profileProviderName As String)
    MyBase.New(_dbMaster, _roleProviderName, _membershipProviderName, _profileProviderName, False)
  End Sub

  Protected Overrides Sub GetUserDetails(ByVal _person As String, ByRef _firstName As String, ByRef _lastName As String, ByRef _email As String, Optional ByVal _row As DataRow = Nothing)
    GetName(_person, _firstName, _lastName)
    _email = GetLogin(_person) & "@company.com"
  End Sub

  Protected Overrides Sub GetUserLogin(ByVal _person As String, ByRef _login As String, ByRef _mode As utopy.bSecurityManager.Manager.AuthenticationType, ByRef _password As String, Optional ByVal _row As DataRow = Nothing)
    _login = GetLogin(_person)
    _mode = utopy.bSecurityManager.Manager.AuthenticationType.SPEECHMINER
    _password = _login
  End Sub

  Private Function GetLogin(ByVal _person As String) As String
    Dim firstName As String = Nothing
    Dim lastName As String = Nothing
    GetName(_person, firstName, lastName)
    Dim login As String
    If firstName = "" Then
      login = lastName
    Else
      login = firstName.Substring(0, 1) & lastName
    End If
    Return login
  End Function

  Private Sub GetName(ByVal _person As String, ByRef _firstName As String, ByRef _lastName As String)
    Dim spaceIndex As Integer = _person.IndexOf(" "c)
    If spaceIndex < 0 Then
      _lastName = _person
      _firstName = ""
    Else
      _lastName = _person.Substring(0, spaceIndex)
      _firstName = _person.Substring(spaceIndex + 1)
    End If
  End Sub

  Protected Overrides Function GetUserRoles(ByVal _person As String, Optional ByVal _row As DataRow = Nothing) As String()
    Return New String() {"Regular User"}
  End Function

  Protected Overrides Function GetUserGroups(ByVal _person As String, Optional ByVal _row As DataRow = Nothing) As String()
    Return New String() {"Default"}
  End Function
End Class
