﻿Namespace Configuration
  ''' <summary>
  ''' Utility class for configuration related operations
  ''' </summary>
  ''' <remarks></remarks>
    Public Module ConfigurationUtils

        ''' <summary>
        ''' Parse a setting in the format key[=value][,key[=value]...] to a mapping dictionary.
        ''' </summary>
        ''' <param name="setting">The mapping as a string</param>
        ''' <returns>A dictionary containing the mapping</returns>
        ''' <remarks>When the value is not specified, it is the same as the key</remarks>
        Public Function ParseKeyValuePairs(
            setting As String
            ) As IDictionary(Of String, String)

            If setting Is Nothing Then
                Return Nothing
            End If

            Dim ret As New Dictionary(Of String, String)
            Dim fields As String() = setting.Split(New Char() {","c}, StringSplitOptions.RemoveEmptyEntries)
            For Each f As String In fields
                Dim parts As String() = f.Split(New Char() {"="c}, 2, StringSplitOptions.RemoveEmptyEntries)
                Dim key As String = parts(0).Trim()
                Dim newName As String = key
                If parts.Length > 1 Then
                    newName = parts(1)
                End If
                ret(key) = newName
            Next

            Return ret
        End Function

    End Module
End Namespace