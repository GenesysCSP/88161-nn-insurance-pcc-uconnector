﻿Namespace Configuration
    ''' <summary>
    ''' A partial configuration, allowing access to the settings under a specific element.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class ConfigSection
        Implements IConfigSection

        ''' <summary>
        ''' The entire UConnector configuration
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_configuration As IConfigSection
        ''' <summary>
        ''' The name of the section (the section element's tag)
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_sectionName As String

        ''' <summary>
        ''' Constructor for the configuration section
        ''' </summary>
        ''' <param name="configuration">The whole configuration</param>
        ''' <param name="sectionName">The name of the section being created</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection, ByVal sectionName As String)
            m_configuration = configuration
            m_sectionName = sectionName
        End Sub

        ''' <summary>
        ''' Accessor for the configuration properties
        ''' </summary>
        ''' <param name="name">The name of the property</param>
        ''' <value>The value of the property</value>
        ''' <returns>The value of the property from the settings dictionary</returns>
        ''' <remarks></remarks>
        Default Public Property Value(ByVal name As String) As String Implements IConfigSection.Value
            Get
                Return m_configuration(m_sectionName & Config.SETTINGS_SEPARATOR & name)
            End Get
            Set(ByVal value As String)
                m_configuration(m_sectionName & Config.SETTINGS_SEPARATOR & name) = value
            End Set
        End Property

        ''' <summary>
        ''' Accessor for a property with multiple values
        ''' </summary>
        ''' <param name="name">The name of the property</param>
        ''' <value>The list of values</value>
        ''' <returns>The list of values</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ValueList(ByVal name As String) As System.Collections.Generic.IList(Of String) Implements IConfigSection.ValueList
            Get
                Dim val As String = Me(name)
                If String.IsNullOrEmpty(val) Then
                    Return New List(Of String)
                End If
                Return val.Split(New String() {Config.SETTINGS_MULTI_VALUE_SEPARATOR}, StringSplitOptions.RemoveEmptyEntries)
            End Get
        End Property
    End Class
End Namespace