﻿Namespace Users
  ''' <summary>
  ''' Manager for updating users in the SpeechMiner system.
  ''' </summary>
  ''' <remarks></remarks>
  Public Interface IUserManager

    ''' <summary>
    ''' Start periodic updating of the SpeechMiner users.
    ''' </summary>
    ''' <remarks></remarks>
    Sub StartUpdating()
    ''' <summary>
    ''' Stop periodic updating of the SpeechMiner users.
    ''' </summary>
    ''' <remarks></remarks>
    Sub StopUpdating()
    ''' <summary>
    ''' Perform the SpeechMiner users update.
    ''' </summary>
    ''' <returns>True if it succeeds</returns>
    ''' <remarks></remarks>
    Function UpdateUsers() As Boolean

    ''' <summary>
    ''' Event raised when the update starts
    ''' </summary>
    ''' <remarks></remarks>
    Event UpdateStarted()
    ''' <summary>
    ''' Event raised if the update succeeds
    ''' </summary>
    ''' <remarks>Note that some of the user updates may have failed. Use <see cref="UserHandled"/> events to get this information.</remarks>
    Event UpdateSuccessful()
    ''' <summary>
    ''' Event raised if the update fails
    ''' </summary>
    ''' <param name="exception">The exception causing the failure</param>
    ''' <remarks></remarks>
    Event UpdateFailed(ByVal exception As Exception)
    ''' <summary>
    ''' Event raised when a user is updated in SpeechMiner
    ''' </summary>
    ''' <param name="user">The user updated</param>
    ''' <param name="status">Success/failure information</param>
    ''' <remarks></remarks>
    Event UserHandled(ByVal user As UserData, ByVal status As IItemStatus)

  End Interface
End Namespace