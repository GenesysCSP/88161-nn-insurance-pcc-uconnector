﻿Namespace Users
  ''' <summary>
  ''' Data for a SpeechMiner user
  ''' </summary>
  ''' <remarks></remarks>
  Public Class UserData
    Implements IUserData

    ''' <summary>
    ''' Constructor for user data
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
      m_data = New Dictionary(Of String, String)
    End Sub

    ''' <summary>
    ''' A dictionary containing the user's profile properties
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_data As IDictionary(Of String, String)
    ''' <summary>
    ''' Whether or not the user can log in
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_active As Boolean
    ''' <summary>
    ''' The user's login name
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_login As String
    ''' <summary>
    ''' Key for the type of authentication to be used
    ''' </summary>
    ''' <remarks></remarks>
    Protected Const PROFILE_AUTHENTICATION As String = "AuthenType"
    ''' <summary>
    ''' The user's password
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_password As String
    ''' <summary>
    ''' Key for whether the user must change the password when they log in
    ''' </summary>
    ''' <remarks></remarks>
    Protected Const PROFILE_MUST_CHANGE_PASSWORD As String = "ChangePasswordNextLogin"
    ''' <summary>
    ''' Key for the user's first name
    ''' </summary>
    ''' <remarks></remarks>
    Protected Const PROFILE_FIRST_NAME As String = "FirstName"
    ''' <summary>
    ''' Key for the user's last name
    ''' </summary>
    ''' <remarks></remarks>
    Protected Const PROFILE_LAST_NAME As String = "LastName"
    ''' <summary>
    ''' Key for the user's title
    ''' </summary>
    ''' <remarks></remarks>
    Protected Const PROFILE_TITLE As String = "Title"
    ''' <summary>
    ''' Key for the organization that the user belongs to
    ''' </summary>
    ''' <remarks></remarks>
    Protected Const PROFILE_ORGANIZATION As String = "Organization"
    ''' <summary>
    ''' Key for the user's phone number
    ''' </summary>
    ''' <remarks></remarks>
    Protected Const PROFILE_PHONE As String = "Phone"
    ''' <summary>
    ''' Key for the user's email address
    ''' </summary>
    ''' <remarks></remarks>
    Protected Const PROFILE_EMAIL As String = "Email"
    ''' <summary>
    ''' Key for the user's home page URL
    ''' </summary>
    ''' <remarks></remarks>
    Protected Const PROFILE_HOMEPAGE As String = "defaultHomePage"
    ''' <summary>
    ''' Key for comments about the user
    ''' </summary>
    ''' <remarks></remarks>
    Protected Const PROFILE_COMMENTS As String = "Comments"
    ''' <summary>
    ''' The ID in the agent hierarchy that matches this user
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_agentId As String
    ''' <summary>
    ''' SpeechMiner roles to which the user belongs
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_roles As ICollection(Of String)
    ''' <summary>
    ''' SpeechMiner groups to which the user belongs
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_groups As ICollection(Of String)
    ''' <summary>
    ''' SpeechMiner workgroups and partitions to which the user has access
    ''' </summary>
    ''' <remarks></remarks>
    Protected m_partitions As ICollection(Of String)

    ''' <summary>
    ''' Accessor for the user profile properties
    ''' </summary>
    ''' <param name="name">The name of the property</param>
    ''' <value>The value of the property</value>
    ''' <returns>The value of the property from the dictionary</returns>
    ''' <remarks></remarks>
    Default Public Overridable Property Value(ByVal name As String) As String Implements IUserData.Value
      Get
        If m_data.ContainsKey(name) Then
          Return m_data(name)
        Else
          Return Nothing
        End If
      End Get
      Set(ByVal value As String)
        m_data(name) = value
      End Set
    End Property

    ''' <summary>
    ''' Whether or not the user can log in
    ''' </summary>
    ''' <value>User is active</value>
    ''' <returns>Whether or not the user is active</returns>
    ''' <remarks></remarks>
    Public Overridable Property Active() As Boolean Implements IUserData.Active
      Get
        Return m_active
      End Get
      Set(ByVal value As Boolean)
        m_active = value
      End Set
    End Property

    ''' <summary>
    ''' The user's login name
    ''' </summary>
    ''' <value>Login name</value>
    ''' <returns>The user's login name</returns>
    ''' <remarks></remarks>
    Public Overridable Property Login() As String Implements IUserData.Login
      Get
        Return m_login
      End Get
      Set(ByVal value As String)
        m_login = value
      End Set
    End Property

    ''' <summary>
    ''' The type of authentication to be used
    ''' </summary>
    ''' <value>Authentication type</value>
    ''' <returns>The user's authentication type</returns>
    ''' <remarks>Valid values are <c>SPEECHMINER</c> and <c>WINDOWS</c>. 
    ''' <c>GENESYS</c> should not be used, as these users are not maintained in Speechminer.</remarks>
    Public Overridable Property Authentication() As utopy.bSecurityManager.Manager.AuthenticationType Implements IUserData.Authentication
      Get
        If String.IsNullOrEmpty(Me(PROFILE_AUTHENTICATION)) Then
          Return DefaultAuthentication
        End If
        If Not [Enum].IsDefined(GetType(utopy.bSecurityManager.Manager.AuthenticationType), Me(PROFILE_AUTHENTICATION)) Then
          Return DefaultAuthentication
        End If
        Return CType([Enum].Parse(GetType(utopy.bSecurityManager.Manager.AuthenticationType), Me(PROFILE_AUTHENTICATION)), utopy.bSecurityManager.Manager.AuthenticationType)
      End Get
      Set(ByVal value As utopy.bSecurityManager.Manager.AuthenticationType)
        Me(PROFILE_AUTHENTICATION) = value.ToString()
      End Set
    End Property

    ''' <summary>
    ''' The default authentication type, in case it is not specified
    ''' </summary>
    ''' <value>Default authentication type</value>
    ''' <returns>Windows authentication</returns>
    ''' <remarks></remarks>
    Protected Overridable ReadOnly Property DefaultAuthentication() As utopy.bSecurityManager.Manager.AuthenticationType
      Get
        Return utopy.bSecurityManager.Manager.AuthenticationType.WINDOWS
      End Get
    End Property

    ''' <summary>
    ''' The user's password
    ''' </summary>
    ''' <value>User password</value>
    ''' <returns>The user's password</returns>
    ''' <remarks>Not needed for Windows authentication</remarks>
    Public Overridable Property Password() As String Implements IUserData.Password
      Get
        Return m_password
      End Get
      Set(ByVal value As String)
        m_password = value
      End Set
    End Property

    ''' <summary>
    ''' Whether the user must change the password when they log in
    ''' </summary>
    ''' <value>Whether the user must change their password</value>
    ''' <returns>True if the password must be changed</returns>
    ''' <remarks></remarks>
    Public Overridable Property MustChangePassword() As Boolean Implements IUserData.MustChangePassword
      Get
        Return CBool(Me(PROFILE_MUST_CHANGE_PASSWORD))
      End Get
      Set(ByVal value As Boolean)
        Me(PROFILE_MUST_CHANGE_PASSWORD) = CStr(value)
      End Set
    End Property

    ''' <summary>
    ''' The user's first name
    ''' </summary>
    ''' <value>First name</value>
    ''' <returns>The user's first name</returns>
    ''' <remarks></remarks>
    Public Overridable Property FirstName() As String Implements IUserData.FirstName
      Get
        Return Me(PROFILE_FIRST_NAME)
      End Get
      Set(ByVal value As String)
        Me(PROFILE_FIRST_NAME) = value
      End Set
    End Property

    ''' <summary>
    ''' The user's last name
    ''' </summary>
    ''' <value>Last name</value>
    ''' <returns>The user's last name</returns>
    ''' <remarks></remarks>
    Public Overridable Property LastName() As String Implements IUserData.LastName
      Get
        Return Me(PROFILE_LAST_NAME)
      End Get
      Set(ByVal value As String)
        Me(PROFILE_LAST_NAME) = value
      End Set
    End Property

    ''' <summary>
    ''' The user's title
    ''' </summary>
    ''' <value>Title</value>
    ''' <returns>The user's title</returns>
    ''' <remarks></remarks>
    Public Overridable Property Title() As String Implements IUserData.Title
      Get
        Return Me(PROFILE_TITLE)
      End Get
      Set(ByVal value As String)
        Me(PROFILE_TITLE) = value
      End Set
    End Property

    ''' <summary>
    ''' The organization that the user belongs to
    ''' </summary>
    ''' <value>User's organization</value>
    ''' <returns>The user's organization</returns>
    ''' <remarks></remarks>
    Public Overridable Property Organization() As String Implements IUserData.Organization
      Get
        Return Me(PROFILE_ORGANIZATION)
      End Get
      Set(ByVal value As String)
        Me(PROFILE_ORGANIZATION) = value
      End Set
    End Property

    ''' <summary>
    ''' The user's phone number
    ''' </summary>
    ''' <value>User's phone</value>
    ''' <returns>The user's phone</returns>
    ''' <remarks></remarks>
    Public Overridable Property Phone() As String Implements IUserData.Phone
      Get
        Return Me(PROFILE_PHONE)
      End Get
      Set(ByVal value As String)
        Me(PROFILE_PHONE) = value
      End Set
    End Property

    ''' <summary>
    ''' The user's email address
    ''' </summary>
    ''' <value>User's email</value>
    ''' <returns>The user's email</returns>
    ''' <remarks></remarks>
    Public Overridable Property Email() As String Implements IUserData.Email
      Get
        Return Me(PROFILE_EMAIL)
      End Get
      Set(ByVal value As String)
        Me(PROFILE_EMAIL) = value
      End Set
    End Property

    ''' <summary>
    ''' The user's home page URL
    ''' </summary>
    ''' <value>User's home page</value>
    ''' <returns>The user's home page</returns>
    ''' <remarks>This is the page the user goes to when they log in</remarks>
    Public Overridable Property HomePage() As String Implements IUserData.HomePage
      Get
        Return Me(PROFILE_HOMEPAGE)
      End Get
      Set(ByVal value As String)
        Me(PROFILE_HOMEPAGE) = value
      End Set
    End Property

    ''' <summary>
    ''' Comments about the user
    ''' </summary>
    ''' <value>Comments</value>
    ''' <returns>Comments about the user</returns>
    ''' <remarks></remarks>
    Public Overridable Property Comments() As String Implements IUserData.Comments
      Get
        Return Me(PROFILE_COMMENTS)
      End Get
      Set(ByVal value As String)
        Me(PROFILE_COMMENTS) = value
      End Set
    End Property

    ''' <summary>
    ''' The ID in the agent hierarchy that matches this user
    ''' </summary>
    ''' <value>User's agent ID</value>
    ''' <returns>The user's agent ID</returns>
    ''' <remarks></remarks>
    Public Overridable Property AgentID() As String Implements IUserData.AgentID
      Get
        Return m_agentId
      End Get
      Set(ByVal value As String)
        m_agentId = value
      End Set
    End Property

    ''' <summary>
    ''' SpeechMiner roles to which the user belongs
    ''' </summary>
    ''' <value>User's roles</value>
    ''' <returns>The user's roles</returns>
    ''' <remarks></remarks>
    Public Overridable Property Roles() As System.Collections.Generic.ICollection(Of String) Implements IUserData.Roles
      Get
        If m_roles Is Nothing Then
          m_roles = New List(Of String)
        End If
        Return m_roles
      End Get
      Set(ByVal value As System.Collections.Generic.ICollection(Of String))
        m_roles = value
      End Set
    End Property

    ''' <summary>
    ''' SpeechMiner groups to which the user belongs
    ''' </summary>
    ''' <value>User's groups</value>
    ''' <returns>The user's groups</returns>
    ''' <remarks></remarks>
    Public Overridable Property Groups() As System.Collections.Generic.ICollection(Of String) Implements IUserData.Groups
      Get
        If m_groups Is Nothing Then
          m_groups = New List(Of String)
        End If
        Return m_groups
      End Get
      Set(ByVal value As System.Collections.Generic.ICollection(Of String))
        m_groups = value
      End Set
    End Property

    ''' <summary>
    ''' SpeechMiner workgroups and partitions to which the user has access
    ''' </summary>
    ''' <value>User's paritions</value>
    ''' <returns>The user's paritions</returns>
    ''' <remarks></remarks>
    Public Overridable Property Partitions() As System.Collections.Generic.ICollection(Of String) Implements IUserData.Partitions
      Get
        If m_partitions Is Nothing Then
          m_partitions = New List(Of String)
        End If
        Return m_partitions
      End Get
      Set(ByVal value As System.Collections.Generic.ICollection(Of String))
        m_partitions = value
      End Set
    End Property
  End Class
End Namespace