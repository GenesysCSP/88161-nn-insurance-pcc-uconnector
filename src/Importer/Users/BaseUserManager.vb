﻿Imports System.Web.Profile
Imports System.Web.Security
Imports System.Data.SqlClient

Namespace Users
    ''' <summary>
    ''' Manager for updating users in the SpeechMiner system.
    ''' </summary>
    ''' <remarks>Role, Membership and Profile Providers need to be set in the .config file</remarks>
    Public MustInherit Class BaseUserManager
        Implements IUserManager

        ''' <summary>
        ''' Event raised when the update starts
        ''' </summary>
        ''' <remarks></remarks>
        Public Event UpdateStarted() Implements IUserManager.UpdateStarted

        ''' <summary>
        ''' Event raised if the update succeeds
        ''' </summary>
        ''' <remarks></remarks>
        Public Event UpdateSuccessful() Implements IUserManager.UpdateSuccessful

        ''' <summary>
        ''' Event raised if the update fails
        ''' </summary>
        ''' <param name="exception">The exception causing the failure</param>
        ''' <remarks></remarks>
        Public Event UpdateFailed(ByVal exception As System.Exception) Implements IUserManager.UpdateFailed

        ''' <summary>
        ''' Event raised when a user is updated in SpeechMiner
        ''' </summary>
        ''' <param name="user">The user updated</param>
        ''' <param name="status">Success/failure information</param>
        ''' <remarks></remarks>
        Public Event UserHandled(ByVal user As UserData, ByVal status As IItemStatus) Implements IUserManager.UserHandled

        ''' <summary>
        ''' The configuration that specifies parameters for reading and updating the users.
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_configuration As UsersConfiguration
        ''' <summary>
        ''' The SpeechMiner database to update.
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_db As utopy.bDBServices.DBServices
        ''' <summary>
        ''' Provider for user roles
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_roleProvider As RoleProvider
        ''' <summary>
        ''' Provider for user membership
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_membershipProvider As MembershipProvider
        ''' <summary>
        ''' Provider for user profile
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_profileProvider As ProfileProvider

        ''' <summary>
        ''' A timer for periodically updating the users.
        ''' </summary>
        ''' <remarks></remarks>
        Protected WithEvents m_timer As Timers.Timer
        ''' <summary>
        ''' A lock to ensure only one update is going on at once.
        ''' </summary>
        ''' <remarks></remarks>
        Protected Shared m_userUpdateLockObj As New Object()

        ''' <summary>
        ''' Dictionary to cache roles that have been checked and whether they exist or not
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_checkedRoles As Generic.IDictionary(Of String, Boolean)

        ''' <summary>
        ''' Dictionary to cache IDs of user groups by their names
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_groupIds As Generic.IDictionary(Of String, Integer)

        ''' <summary>
        ''' Dictionary to cache IDs of partitions by their names
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_partitionIds As Generic.IDictionary(Of String, Integer)

        ''' <summary>
        ''' Create the user manager with its configuration
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigurationSection)
            m_configuration = New UsersConfiguration(configuration)
            m_db = ConfigManager.GetDatabase(New ImporterConfiguration(configuration).SpeechMinerDBName)
            m_roleProvider = CreateRoleProvider()
            m_membershipProvider = CreateMembershipProvider()
            m_profileProvider = CreateProfileProvider()
        End Sub

        ''' <summary>
        ''' Start periodic updating of the SpeechMiner users.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub StartUpdating() Implements IUserManager.StartUpdating
            UpdateUsers()
            InitTimer()
        End Sub

        ''' <summary>
        ''' Stop periodic updating of the SpeechMiner users.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub StopUpdating() Implements IUserManager.StopUpdating
            m_timer.Stop()
            m_timer.Close()
        End Sub

        ''' <summary>
        ''' Perform the SpeechMiner users update.
        ''' </summary>
        ''' <returns>True if it succeeds</returns>
        ''' <remarks></remarks>
        Public Function UpdateUsers() As Boolean Implements IUserManager.UpdateUsers
            RaiseEvent UpdateStarted()
            Try
                m_checkedRoles = New Dictionary(Of String, Boolean)
                m_groupIds = New Dictionary(Of String, Integer)
                m_partitionIds = New Dictionary(Of String, Integer)
                Dim users As ICollection(Of UserData) = GetUsers()
                UpdateUsers(users)
                RaiseEvent UpdateSuccessful()
                Return True
            Catch ex As Exception
                RaiseEvent UpdateFailed(ex)
                Return False
            End Try
        End Function

        ''' <summary>
        ''' Starts the timer for periodic updating of the agent hierarchy.
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub InitTimer()
            m_timer = New Timers.Timer(m_configuration.UsersUpdateInterval.TotalMilliseconds)
            m_timer.AutoReset = False
            m_timer.Start()
        End Sub

        ''' <summary>
        ''' Handle the event specifying that the update interval finished, and restart the timer afterwards.
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub TimerElapsed() Handles m_timer.Elapsed
            Try
                UpdateUsers()
            Finally
                m_timer.Start()
            End Try
        End Sub

        ''' <summary>
        ''' Get the list of users.
        ''' </summary>
        ''' <returns>A collection of UserData</returns>
        ''' <remarks></remarks>
        Protected MustOverride Function GetUsers() As ICollection(Of UserData)

        ''' <summary>
        ''' Update the users in the SpeechMiner database.
        ''' </summary>
        ''' <param name="users">A collection of Userdata</param>
        ''' <remarks></remarks>
        Protected Overridable Sub UpdateUsers(ByVal users As ICollection(Of UserData))
            SyncLock m_userUpdateLockObj
                If users.Count > 0 Then
                    Dim en As IEnumerator = ProfileBase.Properties.GetEnumerator()
                    While en.MoveNext
                        Dim pr As System.Configuration.SettingsProperty = CType(en.Current, System.Configuration.SettingsProperty)
                        pr.Provider = m_profileProvider
                    End While
                    For Each user As UserData In users
                        Try
                            Dim ret As Boolean = UpdateUser(user)
                            RaiseEvent UserHandled(user, CreateUserStatus(ret))
                        Catch ex As Exception
                            RaiseEvent UserHandled(user, CreateUserStatus(False, , ex))
                        End Try
                    Next
                End If
            End SyncLock
        End Sub

        ''' <summary>
        ''' Create the provider for user roles
        ''' </summary>
        ''' <returns>Role provider</returns>
        ''' <remarks></remarks>
        Protected Overridable Function CreateRoleProvider() As RoleProvider
            If String.IsNullOrEmpty(m_configuration.RoleProviderName) Then
                Return Roles.Provider
            Else
                Return Roles.Providers(m_configuration.RoleProviderName)
            End If
        End Function

        ''' <summary>
        ''' Create the provider for user membership
        ''' </summary>
        ''' <returns>Membership provider</returns>
        ''' <remarks></remarks>
        Protected Overridable Function CreateMembershipProvider() As MembershipProvider
            If String.IsNullOrEmpty(m_configuration.MembershipProviderName) Then
                Return Membership.Provider
            Else
                Return Membership.Providers(m_configuration.MembershipProviderName)
            End If
        End Function

        ''' <summary>
        ''' Create the provider for user profile
        ''' </summary>
        ''' <returns>Profile provider</returns>
        ''' <remarks></remarks>
        Protected Overridable Function CreateProfileProvider() As ProfileProvider
            If String.IsNullOrEmpty(m_configuration.ProfileProviderName) Then
                Return ProfileManager.Provider
            Else
                Return ProfileManager.Providers(m_configuration.ProfileProviderName)
            End If
        End Function

        ''' <summary>
        ''' Create a new user status object
        ''' </summary>
        ''' <param name="success">Whether the user processing succeeded or not</param>
        ''' <param name="message">In case of failure, the error message</param>
        ''' <param name="exception">In case of failure, the exception that caused the failure</param>
        ''' <returns>The user status</returns>
        ''' <remarks></remarks>
        Protected Overridable Function CreateUserStatus(ByVal success As Boolean, Optional ByVal message As String = Nothing, Optional ByVal exception As Exception = Nothing) As IItemStatus
            Return New ItemStatus(success, message, exception)
        End Function

        ''' <summary>
        ''' Create or update a single user in SpeechMiner
        ''' </summary>
        ''' <param name="user">The user's data</param>
        ''' <returns>True if the update succeeded</returns>
        ''' <remarks></remarks>
        Protected Overridable Function UpdateUser(ByVal user As UserData) As Boolean
            If String.IsNullOrEmpty(user.Login) Then
                Throw New Exception("User login must be specified")
            End If
            If user.Authentication = utopy.bSecurityManager.Manager.AuthenticationType.GENESYS Then
                Throw New NotImplementedException("Updating users with Genesys authentication is not supported")
            End If
            If user.Authentication = utopy.bSecurityManager.Manager.AuthenticationType.SPEECHMINER AndAlso String.IsNullOrEmpty(user.Password) Then
                Throw New Exception("Password must be specified when using SpeechMiner authentication")
            End If
            If user.FirstName Is Nothing OrElse user.LastName Is Nothing Then
                Throw New Exception("User name is missing")
            End If
            Dim mUser As MembershipUser
            Dim password As String
            If user.Authentication = utopy.bSecurityManager.Manager.AuthenticationType.WINDOWS Then
                password = "password" ' Temporary
            Else
                password = user.Password
            End If
            Dim loginSuffix As Integer = 2
            Dim baseLogin As String = user.Login
            Dim status As MembershipCreateStatus
            Dim login As String = user.Login
            If m_membershipProvider.GetUser(user.Login, False) IsNot Nothing Then
                EditUser(user)
            Else
                Try
                    Do
                        mUser = m_membershipProvider.CreateUser(login, password, user.Email, Nothing, Nothing, True, Nothing, status)
                        If status = MembershipCreateStatus.DuplicateUserName Then
                            login = baseLogin & "_" & loginSuffix
                            loginSuffix += 1
                        End If
                    Loop While status = MembershipCreateStatus.DuplicateUserName

                    If status <> MembershipCreateStatus.Success Then
                        Throw New Exception(status.ToString())
                    Else
                        UpdateUserInfo(mUser, user)
                    End If
                Catch ex As Exception
                    utopy.bSecurityUsers.Users.setUserPartitions(m_db, Nothing, login, New Integer() {}, Nothing)
                    utopy.bSecurityGroups.Groups.resetUserGroups(m_db, login)
                    RemoveUserRoles(login)
                    m_membershipProvider.DeleteUser(login, True)
                    Return False
                End Try
            End If

            Return True
        End Function

        ''' <summary>
        ''' Edit an existing user in the system
        ''' </summary>
        ''' <param name="user">The user data</param>
        ''' <remarks></remarks>
        Protected Overridable Sub EditUser(ByVal user As UserData)

            Dim mUser As MembershipUser = m_membershipProvider.GetUser(user.Login, False)
            If mUser Is Nothing Then
                Throw New Exception("User " & user.Login & " does not exist, can't be updated.")
            Else
                UpdateUserInfo(mUser, user)
            End If
        End Sub

        ''' <summary>
        ''' Safe setting of a property in the user profile
        ''' </summary>
        ''' <param name="profile">The user profile</param>
        ''' <param name="name">The name of the property to set</param>
        ''' <param name="value">The value to set in the property</param>
        ''' <remarks>If the profile does not contain the property, this does not throw an exception, but simply does nothing</remarks>
        ''' <returns>True if the property value was set</returns>
        Protected Overridable Function SetProfileProperty(ByVal profile As ProfileBase, ByVal name As String, ByVal value As String) As Boolean
            Try
                profile.SetPropertyValue(name, value)
                Return True
            Catch ex As System.Configuration.SettingsPropertyNotFoundException
                Return False
            End Try
        End Function

        ''' <summary>
        ''' Save the user information for a Membership user
        ''' </summary>
        ''' <param name="user">The Membership user</param>
        ''' <param name="data">The user data</param>
        ''' <remarks></remarks>
        Protected Overridable Sub UpdateUserInfo(ByVal user As MembershipUser, ByVal data As UserData)
            user.Email = data.Email
            Dim userProfile As ProfileBase = ProfileBase.Create(data.Login)
            For Each kvp As KeyValuePair(Of String, String) In m_configuration.UserProfileProperties
                SetProfileProperty(userProfile, kvp.Value, data(kvp.Key))
            Next
            utopy.bSecurityUsers.Users.setUserAgentPath(m_db, data.Login, data.AgentID)
            userProfile.Save()
            m_membershipProvider.UpdateUser(user)
            If data.Roles IsNot Nothing Then
                SetRoles(data.Login, data.Roles)
            End If
            If data.Groups IsNot Nothing Then
                SetGroups(data.Login, data.Groups)
            End If
            If data.Partitions IsNot Nothing Then
                SetPartitions(data.Login, data.Partitions)
            End If
        End Sub

        ''' <summary>
        ''' Add the given roles to the user
        ''' </summary>
        ''' <param name="login">The user's login</param>
        ''' <param name="roles">The roles to set</param>
        ''' <remarks>If a role doesn't exist it will be skipped.</remarks>
        Protected Overridable Sub SetRoles(ByVal login As String, ByVal roles As ICollection(Of String))
            RemoveUserRoles(login)
            Dim rolesToAdd As New List(Of String)
            For Each role As String In roles
                If ValidateRoleName(role) Then
                    rolesToAdd.Add(role)
                End If
            Next
            If rolesToAdd.Count > 0 Then
                m_roleProvider.AddUsersToRoles(New String() {login}, rolesToAdd.ToArray())
            End If
        End Sub

        Private Sub RemoveUserRoles(ByVal login As String)
            Dim curRoles As String() = m_roleProvider.GetRolesForUser(login)
            If curRoles IsNot Nothing AndAlso curRoles.Count > 0 Then
                m_roleProvider.RemoveUsersFromRoles(New String() {login}, curRoles)
            End If
        End Sub

        ''' <summary>
        ''' Validate that a role with the given name exists
        ''' </summary>
        ''' <param name="role">Role name</param>
        ''' <returns>True if the role exists</returns>
        ''' <remarks></remarks>
        Protected Overridable Function ValidateRoleName(ByVal role As String) As Boolean
            If Not m_checkedRoles.ContainsKey(role) Then
                m_checkedRoles(role) = m_roleProvider.RoleExists(role)
                If Not m_checkedRoles(role) Then
                    m_logger.WARNING("Role " & role & " does not exist, and won't be added to users")
                End If
            End If
            Return m_checkedRoles(role)
        End Function

        ''' <summary>
        ''' Set the given groups to the user
        ''' </summary>
        ''' <param name="login">The user's login</param>
        ''' <param name="groups">The groups to set</param>
        ''' <remarks>If a group doesn't exist it will be skipped.</remarks>
        Protected Overridable Sub SetGroups(ByVal login As String, ByVal groups As ICollection(Of String))
            Dim groupsToAdd As New List(Of Integer)
            For Each group As String In groups
                Dim id As Integer
                If Not m_groupIds.TryGetValue(group, id) Then
                    id = utopy.bSecurityGroups.Groups.getGroupId(m_db, group)
                    m_groupIds(group) = id
                    If id <= 0 Then
                        m_logger.WARNING("Group " & group & " does not exist, and won't be set for users")
                    End If
                End If
                If id > 0 Then
                    groupsToAdd.Add(id)
                End If
            Next
            utopy.bSecurityGroups.Groups.setUserGroups(m_db, Nothing, login, groupsToAdd.ToArray(), Nothing)
        End Sub

        ''' <summary>
        ''' Set the given partitions to the user
        ''' </summary>
        ''' <param name="login">The user's login</param>
        ''' <param name="partitions">The partitions to set</param>
        ''' <remarks></remarks>
        Protected Sub SetPartitions(ByVal login As String, ByVal partitions As ICollection(Of String))
            Dim sql As String = "SELECT ISNULL((SELECT id FROM partitionTbl WHERE name=@path), -1)"
            Dim partitionsToAdd As New List(Of Integer)
            For Each partition As String In partitions
                Dim id As Integer
                If Not m_partitionIds.TryGetValue(partition, id) Then
                    Dim pathParam As New SqlParameter("@path", partition)
                    id = m_db.getOneValue(Of Integer)(sql, , New SqlParameter() {pathParam})
                    m_partitionIds(partition) = id
                    If id < 0 Then
                        m_logger.WARNING("Partition " & partition & " does not exist, and won't be set for users")
                    End If
                End If
                If id >= 0 Then
                    partitionsToAdd.Add(id)
                End If
            Next
            utopy.bSecurityUsers.Users.setUserPartitions(m_db, Nothing, login, partitionsToAdd.ToArray(), Nothing)
        End Sub
    End Class
End Namespace