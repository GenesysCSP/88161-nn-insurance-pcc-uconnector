﻿Imports System.Threading
Imports utopy.bDBServices
Imports GenUtils.TraceTopic


Namespace Main

    ''' <summary>
    ''' Base implementation for the main class that kicks off everything done in the UConnector
    ''' </summary>
    ''' <remarks></remarks>
    Public Class BaseImporter
        Implements IImporter

#Region "Instance Properties"

        Protected ReadOnly Property Config As IConfig
            Get
                Return m_configuration
            End Get
        End Property

#End Region

        ''' <summary>
        ''' The configuration for the UConnector
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_configuration As IConfig
        ''' <summary>
        ''' The part of the configuration for the main importer process
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_importerConfiguration As ImporterConfig

        ''' <summary>
        ''' Raised when an interaction is handled
        ''' </summary>
        ''' <param name="interaction">The data about the interaction</param>
        ''' <param name="status">How the interaction was completed (success/failure and failure information)</param>
        ''' <remarks></remarks>
        Public Event InteractionHandled(ByVal interaction As IInteractionData, ByVal status As IItemStatus) Implements IImporter.InteractionHandled
        ''' <summary>
        ''' Raised when the importer changes its state
        ''' </summary>
        ''' <param name="status">The new state of the importer</param>
        ''' <param name="description">A textual description of the state</param>
        ''' <remarks></remarks>
        Public Event StatusChanged(ByVal status As IImporter.Status, ByVal description As String) Implements IImporter.StatusChanged

        ''' <summary>
        ''' The SpeechMiner database object
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_speechMinerDB As DBServices
        ''' <summary>
        ''' The SpeechMiner product version
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_speechMinerVersion As Version

        ' ''' <summary>
        ' ''' Monitors the status of the UConnector
        ' ''' </summary>
        ' ''' <remarks></remarks>
        'Protected m_monitor As IMonitor
        ''' <summary>
        ''' Manages the agent hierarchy and updates it in SpeechMiner
        ''' </summary>
        ''' <remarks></remarks>
        Protected WithEvents m_agentManager As IAgentManager

        ' ''' <summary>
        ' ''' Updates users in SpeechMiner
        ' ''' </summary>
        ' ''' <remarks></remarks>
        'Protected WithEvents m_userManager As IUserManager
        ''' <summary>
        ''' Manages the interaction list and gets their data
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_dataManager As IDataManager
        ''' <summary>
        ''' Responsible for monitoring the SpeechMiner folders and finding an available folder
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_folderManager As IFolderManager

        ''' <summary>
        ''' Timer for starting the import of batches of interactions
        ''' </summary>
        ''' <remarks></remarks>
        Protected WithEvents m_batchTimer As Timers.Timer
        ''' <summary>
        ''' Pool of threads for importing interactions
        ''' </summary>
        ''' <remarks></remarks>
        Protected WithEvents m_handlerPool As IHandlerThreadPool

        ''' <summary>
        ''' A marker for when the UConnector is actively in the process of importing interactions
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_importing As Boolean
        ''' <summary>
        ''' A marker for when the UConnector has been given the command to stop
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_stopping As Boolean

        ''' <summary>
        ''' Create a new importer
        ''' </summary>
        ''' <param name="configuration">The UConnector configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfig)
            m_configuration = configuration
            m_importerConfiguration = New ImporterConfig(configuration)
        End Sub

        ''' <summary>
        ''' Initialize the importer and start its components
        ''' </summary>
        ''' <remarks></remarks>
        Public Overridable Sub Init() Implements IImporter.Init
            ConnectToSpeechMinerDB()
            StartAgentManager()
            m_dataManager = CreateDataManager()
            If m_dataManager IsNot Nothing Then
                AddHandler InteractionHandled, AddressOf m_dataManager.InteractionHandled
                m_folderManager = CreateFolderManager()
                m_folderManager.Init()

                ChangeStatus(IImporter.Status.STARTING)
                InitializeHandlerThreads()
            End If
        End Sub

        ''' <summary>
        ''' Exit the importer
        ''' </summary>
        ''' <remarks>Called when the whole process is shut down.</remarks>
        Public Overridable Sub Quit() Implements IImporter.Quit
            Cleanup()
            m_batchTimer.Dispose()
        End Sub

        ''' <summary>
        ''' Check if the UConnector is still running
        ''' </summary>
        ''' <returns>False if the UConnector has been told to stop</returns>
        ''' <remarks></remarks>
        Public Overridable Function CheckRunning() As Boolean
            Return Not m_stopping
        End Function

        ''' <summary>
        ''' Check if the current time of day is within the time interval defined for importing interactions
        ''' </summary>
        ''' <returns>True if the current time is in the import period</returns>
        ''' <remarks></remarks>
        Protected Overridable Function InTimeWindow() As Boolean
            Dim currentTime As TimeSpan = TimeOfDay.TimeOfDay
            Dim startTime As TimeSpan = m_importerConfiguration.ImportStartTime
            Dim endTime As TimeSpan = m_importerConfiguration.ImportEndTime
            If startTime = TimeSpan.MinValue OrElse endTime = TimeSpan.MinValue Then
                Return True
            ElseIf startTime < endTime Then
                Return currentTime >= startTime AndAlso currentTime < endTime
            ElseIf startTime > endTime Then
                Return currentTime >= startTime OrElse currentTime < endTime
            Else
                Return True
            End If
        End Function

        ''' <summary>
        ''' Notify when the state of the UConnector has changed
        ''' </summary>
        ''' <param name="newStatus">The state that the UConnector has moved into</param>
        ''' <remarks></remarks>
        Protected Overridable Sub ChangeStatus(ByVal newStatus As IImporter.Status)
            Select Case newStatus
                Case IImporter.Status.STARTING
                    RaiseEvent StatusChanged(newStatus, "Starting")
                Case IImporter.Status.FETCHING
                    RaiseEvent StatusChanged(newStatus, "Fetching")
                Case IImporter.Status.STOPPED
                    RaiseEvent StatusChanged(newStatus, "Stopped")
                Case IImporter.Status.IDLE
                    RaiseEvent StatusChanged(newStatus, "Idle")
                Case IImporter.Status.ERROR
                    RaiseEvent StatusChanged(newStatus, "ERROR!")
                Case Else
                    RaiseEvent StatusChanged(newStatus, "")
            End Select
        End Sub

        ''' <summary>
        ''' Start importing the interactions
        ''' </summary>
        ''' <remarks></remarks>
        Public Overridable Sub Start() Implements IImporter.Start
            If m_dataManager IsNot Nothing Then
                If m_batchTimer Is Nothing Then
                    m_batchTimer = New Timers.Timer(m_importerConfiguration.BatchInterval.TotalMilliseconds)
                    m_batchTimer.AutoReset = False
                End If
                Dim t As New Thread(AddressOf BatchIntervalElapsed)
                t.Start()
            End If
        End Sub

        ''' <summary>
        ''' Stop importing the interactions.
        ''' </summary>
        ''' <remarks></remarks>
        Public Overridable Sub Cancel() Implements IImporter.Cancel
            Cleanup()
        End Sub

        ''' <summary>
        ''' Perform everything needed to stop the UConnector
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub Cleanup()

            m_stopping = True
            If m_agentManager IsNot Nothing Then
                m_agentManager.StopUpdating()
            End If

            If m_batchTimer IsNot Nothing Then
                m_batchTimer.Stop()
                SyncLock m_batchTimer
                    If Not m_importing Then
                        ChangeStatus(IImporter.Status.STOPPED)
                    End If
                End SyncLock
            Else
                ChangeStatus(IImporter.Status.STOPPED)
            End If
        End Sub

        ''' <summary>
        ''' Whether or not a periodic restart of the UConnector service is desired
        ''' </summary>
        ''' <value>Whether to restart</value>
        ''' <returns>True if the restart interval is set</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property DoRestart() As Boolean Implements IImporter.DoRestart
            Get
                Return RestartInterval > TimeSpan.Zero
            End Get
        End Property

        ''' <summary>
        ''' How often to restart the UConnector service
        ''' </summary>
        ''' <value>The interval between restarts</value>
        ''' <returns>The interval between restarts</returns>
        ''' <remarks>Only relevant when running as a service</remarks>
        Public Overridable ReadOnly Property RestartInterval() As TimeSpan Implements IImporter.RestartInterval
            Get
                Return m_importerConfiguration.RestartInterval
            End Get
        End Property

        ''' <summary>
        ''' Handle the batch timer elapsing, and start importing a new batch if needed
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub BatchIntervalElapsed() Handles m_batchTimer.Elapsed
            Try
                While CheckRunning() And InTimeWindow()
                    ' Keep importing while we have calls
                    ChangeStatus(IImporter.Status.FETCHING)
                    If Not ImportNextBatch() Then
                        Exit While
                    End If
                End While
                SyncLock m_batchTimer
                    If CheckRunning() Then
                        ChangeStatus(IImporter.Status.IDLE)
                        m_batchTimer.Start()
                    Else
                        ChangeStatus(IImporter.Status.STOPPED)
                        m_stopping = False
                    End If
                End SyncLock
            Catch ex As Exception
                HandleError("Error in handling batch", ex)
                If CheckRunning() Then
                    m_batchTimer.Start()
                End If
            End Try
        End Sub

        ''' <summary>
        ''' Get the next batch of interactions, and imports them
        ''' </summary>
        ''' <returns>True if there were any interactions to import</returns>
        ''' <remarks></remarks>
        Public Overridable Function ImportNextBatch() As Boolean Implements IImporter.ImportNextBatch
            Using (TraceTopic.importerTopic.scope())
                TraceTopic.importerTopic.always("Starting next batch...")
                m_importing = True
                Dim interactions = m_dataManager.GetNextInteractions(m_importerConfiguration.BatchSize)
                Dim count = If(interactions Is Nothing, 0, interactions.Count)
                TraceTopic.importerTopic.always("{} interactions to be processed.", count)

                Try
                    For Each data As IInteractionData In interactions
                        If m_stopping Then
                            Exit For
                        End If
                        Dim folder As String = m_folderManager.GetOutputFolder()
                        If folder Is Nothing Then
                            TraceTopic.importerTopic.warning("No free space for importing interactions")
                            Return False
                        End If
                        Dim handler As IInteractionHandlerThread = m_handlerPool.GetHandlerThread()
                        If handler IsNot Nothing Then
                            handler.SetData(data, folder)
                            handler.Go()
                        End If
                    Next

                    m_importing = False
                    Return interactions.Count > 0
                Finally
                    While m_handlerPool.NumberOfThreadsRunning > 0
                        Thread.Sleep(1000)
                    End While
                    m_configuration.Save()
                End Try
            End Using
        End Function

        ''' <summary>
        ''' Update the agent hierarchy
        ''' </summary>
        ''' <remarks>Called only when user explicitly asks for an update (via the application)</remarks>
        Public Overridable Sub UpdateAgents() Implements IImporter.UpdateAgents
            Dim manager As IAgentManager = CreateAgentManager()
            If manager IsNot Nothing Then
                AddHandler manager.UpdateStarted, AddressOf AgentUpdateStarted
                AddHandler manager.UpdateSuccessful, AddressOf AgentUpdateSucceeded
                AddHandler manager.UpdateFailed, AddressOf AgentUpdateFailed
                Try
                    manager.UpdateHierarchy()
                Finally
                    RemoveHandler manager.UpdateStarted, AddressOf AgentUpdateStarted
                    RemoveHandler manager.UpdateSuccessful, AddressOf AgentUpdateSucceeded
                    RemoveHandler manager.UpdateFailed, AddressOf AgentUpdateFailed
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Connect to the SpeechMiner database, and determine the product's version
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub ConnectToSpeechMinerDB()
            If m_importerConfiguration.SpeechMinerDBName IsNot Nothing Then
                m_speechMinerDB = ConfigManager.GetDatabase(m_importerConfiguration.SpeechMinerDBName)
                m_speechMinerVersion = New Version(m_speechMinerDB.getOneValue(Of String)("select version from versionTbl where resource='SM'"))
                utopy.bDBOffline.AudioKeys.init(m_speechMinerDB)
            End If
        End Sub

        ''' <summary>
        ''' Create the data manager for the application
        ''' </summary>
        ''' <returns>The data manager object</returns>
        ''' <remarks></remarks>
        Protected Overridable Function CreateDataManager() As IDataManager
            Dim mgrType As Type = DataConfig.ManagerType(m_configuration)
            If mgrType Is Nothing Then
                Return Nothing
            End If
            Dim ci As System.Reflection.ConstructorInfo = mgrType.GetConstructor(New Type() {GetType(IConfigSection), GetType(IAgentManager)})
            Dim mgr As IDataManager
            If ci IsNot Nothing Then
                mgr = CType(ci.Invoke(New Object() {m_configuration, m_agentManager}), IDataManager)
            Else
                ci = mgrType.GetConstructor(New Type() {GetType(IConfigSection)})
                If ci Is Nothing Then
                    Throw New MissingMethodException("Constructor not found for " & mgrType.Name)
                End If
                mgr = CType(ci.Invoke(New Object() {m_configuration}), IDataManager)
            End If
            Return mgr
        End Function

        ''' <summary>
        ''' Create the folder manager
        ''' </summary>
        ''' <returns>The SpeechMiner folder manager</returns>
        ''' <remarks></remarks>
        Protected Overridable Function CreateFolderManager() As IFolderManager
            Dim mgrType As Type = OutputConfig.FolderManagerType(m_configuration)
            If mgrType Is Nothing Then
                Return Nothing
            End If
            Dim ci As System.Reflection.ConstructorInfo = mgrType.GetConstructor(New Type() {GetType(IConfigSection)})
            If ci Is Nothing Then
                Throw New MissingMethodException("Constructor not found for " & mgrType.Name)
            End If
            Dim mgr As IFolderManager = CType(ci.Invoke(New Object() {m_configuration}), IFolderManager)
            Return mgr
        End Function

        ''' <summary>
        ''' Create the content writer, with its downloader and audio converter
        ''' </summary>
        ''' <returns>Writer for the interactions' content</returns>
        ''' <remarks></remarks>
        Protected Overridable Function CreateContentWriter() As IContentWriter
            Return New AudioContentWriter(CreateContentDownloader(), CreateAudioConverter())
        End Function

        ''' <summary>
        ''' Create the metadata writer
        ''' </summary>
        ''' <returns>Writer for the interactions' metadata</returns>
        ''' <remarks></remarks>
        Protected Overridable Function CreateDataWriter() As IDataWriter
            Dim writerType As Type = OutputConfig.DataWriterType(m_configuration)
            If writerType Is Nothing Then
                Return Nothing
            End If
            Dim ci As System.Reflection.ConstructorInfo = writerType.GetConstructor(New Type() {GetType(IConfigSection)})
            If ci Is Nothing Then
                Throw New MissingMethodException("Constructor not found for " & writerType.Name)
            End If
            Dim writer As IDataWriter = CType(ci.Invoke(New Object() {m_configuration}), IDataWriter)
            Return writer
        End Function

        ''' <summary>
        ''' Create the interaction content downloader
        ''' </summary>
        ''' <returns>The downloader</returns>
        ''' <remarks></remarks>
        Protected Overridable Function CreateContentDownloader() As IContentDownloader
            Dim dlType As Type = DownloadConfig.ConverterType(m_configuration)
            If dlType Is Nothing Then
                Return Nothing
            End If
            Dim ci As System.Reflection.ConstructorInfo = dlType.GetConstructor(New Type() {GetType(IConfigSection)})
            If ci Is Nothing Then
                Throw New MissingMethodException("Constructor not found for " & dlType.Name)
            End If
            Dim downloader As IContentDownloader = CType(ci.Invoke(New Object() {m_configuration}), IContentDownloader)
            Return downloader
        End Function

        ''' <summary>
        ''' Create the call audio converter
        ''' </summary>
        ''' <returns>The audio converter</returns>
        ''' <remarks></remarks>
        Protected Overridable Function CreateAudioConverter() As IAudioConverter
            Dim convType As Type = ConversionConfig.ConverterType(m_configuration)
            If convType Is Nothing Then
                Return Nothing
            End If
            Dim ci As System.Reflection.ConstructorInfo = convType.GetConstructor(New Type() {GetType(IConfigSection)})
            If ci Is Nothing Then
                Throw New MissingMethodException("Constructor not found for " & convType.Name)
            End If
            Dim converter As IAudioConverter = CType(ci.Invoke(New Object() {m_configuration}), IAudioConverter)
            Return converter
        End Function

        ''' <summary>
        ''' Create and start the agent manager
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub StartAgentManager()
            m_agentManager = CreateAgentManager()
            If m_agentManager IsNot Nothing Then
                m_agentManager.StartUpdating()
            End If
        End Sub

        ''' <summary>
        ''' Create the agent manager
        ''' </summary>
        ''' <returns>The agent manager</returns>
        ''' <remarks></remarks>
        Protected Overridable Function CreateAgentManager() As IAgentManager
            Dim mgrType As Type = AgentConfig.ManagerType(m_configuration)
            If mgrType Is Nothing Then
                Return Nothing
            End If
            Dim ci As System.Reflection.ConstructorInfo = mgrType.GetConstructor(New Type() {GetType(IConfigSection)})
            If ci Is Nothing Then
                Throw New MissingMethodException("Constructor not found for " & mgrType.Name)
            End If
            Dim manager As IAgentManager = CType(ci.Invoke(New Object() {m_configuration}), IAgentManager)
            Return manager
        End Function

        ''' <summary>
        ''' Log the start of the agent hierarchy update
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub AgentUpdateStarted() Handles m_agentManager.UpdateStarted
            Using (TraceTopic.importerTopic.scope())
                TraceTopic.importerTopic.note("Agent update starting")
            End Using
        End Sub

        ''' <summary>
        ''' Log success of the agent hierarchy update
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub AgentUpdateSucceeded() Handles m_agentManager.UpdateSuccessful
            Using (TraceTopic.importerTopic.scope())
                TraceTopic.importerTopic.note("Agents updated successfully")
            End Using
        End Sub

        ''' <summary>
        ''' Handle and log failure of the agent hierarchy update
        ''' </summary>
        ''' <param name="exception">The exception that caused the failure</param>
        ''' <remarks></remarks>
        Protected Overridable Sub AgentUpdateFailed(ByVal exception As Exception) Handles m_agentManager.UpdateFailed
            HandleError("Agent update failed", exception)
        End Sub

        ''' <summary>
        ''' Create a new interaction status object
        ''' </summary>
        ''' <param name="success">Whether the interaction processing succeeded or not</param>
        ''' <param name="message">In case of failure, the error message</param>
        ''' <param name="exception">In case of failure, the exception that caused the failure</param>
        ''' <returns>The interaction status</returns>
        ''' <remarks></remarks>
        Protected Overridable Function CreateInteractionStatus(ByVal success As Boolean, Optional ByVal message As String = Nothing, Optional ByVal exception As Exception = Nothing) As IItemStatus
            Return New ItemStatus(success, message, exception)
        End Function

        ''' <summary>
        ''' Initialize the threads that will handle import of single interactions
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub InitializeHandlerThreads()
            Dim contentWriter As IContentWriter = CreateContentWriter()
            Dim dataWriter As IDataWriter = CreateDataWriter()
            m_handlerPool = CreateHandlerThreadPool()
            m_handlerPool.InitializeHandlerThreads(contentWriter, dataWriter)
        End Sub

        ''' <summary>
        ''' Create the thread pool for handling transactions
        ''' </summary>
        ''' <returns>The thread pool</returns>
        ''' <remarks></remarks>
        Protected Overridable Function CreateHandlerThreadPool() As IHandlerThreadPool
            Return New HandlerThreadPoolImpl(m_configuration)
        End Function

        ''' <summary>
        ''' Handle the interaction content having been processed
        ''' </summary>
        ''' <param name="interaction">Data about the interaction</param>
        ''' <param name="success">Whether or not the processing succeeded</param>
        ''' <param name="ex">In case of failure, the exception that caused it</param>
        ''' <remarks>Logs the processing, and raises an <see cref="InteractionHandled"/> event if it failed.</remarks>
        Protected Overridable Sub ContentHandled(ByVal interaction As IInteractionData, ByVal success As Boolean, ByVal ex As Exception) Handles m_handlerPool.ContentHandled
            Using (TraceTopic.importerTopic.scope())
                If success Then
                    TraceTopic.importerTopic.note("Generated content for {}", interaction.ID)
                Else
                    TraceTopic.importerTopic.note("Failed to generate content for {}\n{}", interaction.ID, ex)
                    RaiseEvent InteractionHandled(interaction, CreateInteractionStatus(False, "Failed to generate content.", ex))
                End If
            End Using
        End Sub

        ''' <summary>
        ''' Handle the interaction metadata having been processed
        ''' </summary>
        ''' <param name="interaction">Data about the interaction</param>
        ''' <param name="success">Whether or not the processing succeeded</param>
        ''' <param name="ex">In case of failure, the exception that caused it</param>
        ''' <remarks>Logs the processing, and raises an <see cref="InteractionHandled"/>.</remarks>
        Protected Overridable Sub MetaHandled(ByVal interaction As IInteractionData, ByVal success As Boolean, ByVal ex As Exception) Handles m_handlerPool.MetaHandled
            Using (TraceTopic.importerTopic.scope())
                If success Then
                    TraceTopic.importerTopic.note("Generated meta for {}", interaction.ID)
                    RaiseEvent InteractionHandled(interaction, CreateInteractionStatus(True))
                Else
                    TraceTopic.importerTopic.note("Failed to generate meta for {}\n{}", interaction.ID, ex)
                    RaiseEvent InteractionHandled(interaction, CreateInteractionStatus(False, "Failed to generate meta.", ex))
                End If
            End Using
        End Sub

        ''' <summary>
        ''' Handle an error in processing
        ''' </summary>
        ''' <param name="msg">Error message</param>
        ''' <param name="ex">The exception that caused the error</param>
        ''' <remarks>Logs the error, and updates the UConnector status</remarks>
        Protected Overridable Sub HandleError(ByVal msg As String, Optional ByVal ex As Exception = Nothing)
            Using (TraceTopic.importerTopic.scope())
                TraceTopic.importerTopic.error("{}\n{}", msg, ex)
                ChangeStatus(IImporter.Status.ERROR)
            End Using
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Using (TraceTopic.importerTopic.scope())
                TraceTopic.importerTopic.always("Dispose")
            End Using
        End Sub

#Region "Instance Data"


#End Region

    End Class
End Namespace