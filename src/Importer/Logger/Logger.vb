﻿Imports GenUtils

Namespace Logging
    ''' <summary>
    ''' Logger for UConnector messages
    ''' </summary>
    ''' <remarks></remarks>
    <Obsolete("New code should be written using the log4net logging API")>
    Public Interface ILogger

        ''' <summary>
        ''' Write a fatal error message
        ''' </summary>
        ''' <param name="message">The error message</param>
        ''' <param name="exception">The exception that caused the error</param>
        ''' <remarks></remarks>
        Sub FATAL(ByVal message As String, Optional ByVal exception As Exception = Nothing)
        ''' <summary>
        ''' Write a warning message
        ''' </summary>
        ''' <param name="message">The warning message</param>
        ''' <param name="exception">The exception that caused the warning</param>
        ''' <remarks></remarks>
        Sub WARNING(ByVal message As String, Optional ByVal exception As Exception = Nothing)
        ''' <summary>
        ''' Write an informational message
        ''' </summary>
        ''' <param name="message">The message</param>
        ''' <param name="exception">The exception that caused the message</param>
        Sub LOG(ByVal message As String, Optional ByVal exception As Exception = Nothing)
        ''' <summary>
        ''' Write a debug message
        ''' </summary>
        ''' <param name="message">The message</param>
        ''' <param name="exception">The exception that caused the message</param>
        Sub LOG_DEBUG(ByVal message As String, Optional ByVal exception As Exception = Nothing)

    End Interface

    ''' <summary>
    ''' Global logger for the UConnector
    ''' </summary>
    ''' <remarks></remarks>
    Public Module Logger

        ''' <summary>
        ''' The logger's instance. Created by the configuration handler.
        ''' </summary>
        ''' <remarks></remarks>
        <Obsolete("Should use log4net's LogManager to obtain a logger")>
        Public Property m_logger As ILogger
            Get
                If _logger Is Nothing Then
                    Throw New InvalidOperationException("no logger configured")
                End If

                Return _logger
            End Get
            Set(value As ILogger)
                _logger = value
            End Set
        End Property

        Private _logger As ILogger
    End Module
End Namespace