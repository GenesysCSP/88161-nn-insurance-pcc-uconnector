﻿Namespace Threading
    ''' <summary>
    ''' Configuration for the interaction processing threads
    ''' </summary>
    ''' <remarks></remarks>
    Public Class ThreadingConfig
        Inherits ConfigSection
        Implements IConfigSection

        ''' <summary>
        ''' Tag for the threading section. Value: Threading
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_SETTINGS_THREADING_SECTION_NAME As String = "Threading"

        ''' <summary>
        ''' Tag for the number of threads to run. Value: NumberOfThreads
        ''' </summary>
        ''' <remarks></remarks>
        Protected Const XML_SETTINGS_NUM_THREADS As String = "NumberOfThreads"
        ''' <summary>
        ''' Tag for how long to wait when no thread is free, before checking again. Value: TimeToWaitForThread
        ''' </summary>
        ''' <remarks>The value is in milliseconds.</remarks>
        Protected Const XML_SETTINGS_THREAD_WAIT_TIME As String = "TimeToWaitForThread"

        ''' <summary>
        ''' Create a new threading configuration object.
        ''' </summary>
        ''' <param name="configuration">The entire UConnector configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            MyBase.New(configuration, XML_SETTINGS_THREADING_SECTION_NAME)
        End Sub

        ''' <summary>
        ''' The number of threads to run.
        ''' <seealso cref="XML_SETTINGS_NUM_THREADS"/>
        ''' </summary>
        ''' <value>Number of threads</value>
        ''' <returns>Number of threads</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property NumberOfHandlerThreads() As Integer
            Get
                If Me(XML_SETTINGS_NUM_THREADS) Is Nothing Then
                    Return DefaultNumberOfThreads
                Else
                    Return CInt(Me(XML_SETTINGS_NUM_THREADS))
                End If
            End Get
        End Property

        ''' <summary>
        ''' Default number of threads to run, used if the configuration does not specify a number.
        ''' </summary>
        ''' <value>The default number of threads.</value>
        ''' <returns>One thread</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property DefaultNumberOfThreads() As Integer
            Get
                Return 1
            End Get
        End Property

        ''' <summary>
        ''' How long to wait when no thread is free, before checking again.
        ''' <seealso cref="XML_SETTINGS_THREAD_WAIT_TIME"/>
        ''' </summary>
        ''' <value>Thread sleep time</value>
        ''' <returns>How long to sleep before looking for free thread again</returns>
        ''' <remarks></remarks>
        Public Overridable ReadOnly Property TimeToWaitForThread() As TimeSpan
            Get
                If Me(XML_SETTINGS_THREAD_WAIT_TIME) Is Nothing Then
                    Return DefaultTimeToWaitForThread
                Else
                    Return TimeSpan.FromMilliseconds(CDbl(Me(XML_SETTINGS_THREAD_WAIT_TIME)))
                End If
            End Get
        End Property

        ''' <summary>
        ''' Default length of time to wait before checking for a free thread.
        ''' </summary>
        ''' <value>The default length of time to wait</value>
        ''' <returns>One second</returns>
        ''' <remarks></remarks>
        Protected Overridable ReadOnly Property DefaultTimeToWaitForThread() As TimeSpan
            Get
                Return TimeSpan.FromSeconds(1)
            End Get
        End Property
    End Class
End Namespace