﻿Namespace Threading
  ''' <summary>
  ''' Thread to handle processing of an interaction
  ''' </summary>
  ''' <remarks></remarks>
  Public Interface IInteractionHandlerThread
    ''' <summary>
    ''' Set the processing data to the thread
    ''' </summary>
    ''' <param name="interaction">The interaction to process</param>
    ''' <param name="targetPath">The output folder for the interaction's files</param>
    ''' <remarks></remarks>
    Sub SetData(ByVal interaction As IInteractionData, ByVal targetPath As String)
    ''' <summary>
    ''' Is the thread currently processing
    ''' </summary>
    ''' <value>Is the thread being used</value>
    ''' <returns>Is the thread being used</returns>
    ''' <remarks></remarks>
    ReadOnly Property IsInUse() As Boolean
    ''' <summary>
    ''' Get the thread for using, and mark it as being used
    ''' </summary>
    ''' <returns>True if the thread was available</returns>
    ''' <remarks></remarks>
    Function TryToUse() As Boolean
    ''' <summary>
    ''' Start processing the interaction
    ''' </summary>
    ''' <remarks></remarks>
    Sub Go()
    ''' <summary>
    ''' Event raised when the interaction content is processed
    ''' </summary>
    ''' <param name="interaction">The interaction data</param>
    ''' <param name="success">Whether processing succeeded or not</param>
    ''' <param name="exception">In case of failure, the exception causing it</param>
    ''' <remarks></remarks>
    Event ContentHandled(ByVal interaction As IInteractionData, ByVal success As Boolean, ByVal exception As Exception)
    ''' <summary>
    ''' Event raised when the interaction metadata is processed
    ''' </summary>
    ''' <param name="interaction">The interaction data</param>
    ''' <param name="success">Whether processing succeeded or not</param>
    ''' <param name="exception">In case of failure, the exception causing it</param>
    ''' <remarks></remarks>
    Event MetaHandled(ByVal interaction As IInteractionData, ByVal success As Boolean, ByVal exception As Exception)
  End Interface
End Namespace