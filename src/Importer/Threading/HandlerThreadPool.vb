﻿Imports System.Threading

Namespace Threading
    ''' <summary>
    ''' Pool for threads to import interactions
    ''' </summary>
    ''' <remarks></remarks>
    Public Class HandlerThreadPoolImpl
        Implements IHandlerThreadPool

        ''' <summary>
        ''' Event raised when the interaction content is processed
        ''' </summary>
        ''' <param name="interaction">The interaction data</param>
        ''' <param name="success">Whether processing succeeded or not</param>
        ''' <param name="exception">In case of failure, the exception causing it</param>
        ''' <remarks></remarks>
        Public Event ContentHandled(ByVal interaction As IInteractionData, ByVal success As Boolean, ByVal exception As System.Exception) Implements IHandlerThreadPool.ContentHandled
        ''' <summary>
        ''' Event raised when the interaction metadata is processed
        ''' </summary>
        ''' <param name="interaction">The interaction data</param>
        ''' <param name="success">Whether processing succeeded or not</param>
        ''' <param name="exception">In case of failure, the exception causing it</param>
        ''' <remarks></remarks>
        Public Event MetaHandled(ByVal interaction As IInteractionData, ByVal success As Boolean, ByVal exception As System.Exception) Implements IHandlerThreadPool.MetaHandled

        ''' <summary>
        ''' Handler threads in the pool
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_handlerThreads As ICollection(Of IInteractionHandlerThread)
        ''' <summary>
        ''' The pool's configuration
        ''' </summary>
        ''' <remarks></remarks>
        Protected m_configuration As ThreadingConfig

        ''' <summary>
        ''' Create a new thread pool
        ''' </summary>
        ''' <param name="configuration">The entire UConnector's configuration</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal configuration As IConfigSection)
            m_configuration = New ThreadingConfig(configuration)
        End Sub

        ''' <summary>
        ''' Creates a thread for importing interactions
        ''' </summary>
        ''' <param name="contentWriter">The writer for interaction content files</param>
        ''' <param name="dataWriter">The writer for interaction metadata files</param>
        ''' <returns>Interaction processing thread</returns>
        ''' <remarks></remarks>
        Public Overridable Function CreateHandlerThread(ByVal contentWriter As IContentWriter, ByVal dataWriter As IDataWriter) As IInteractionHandlerThread Implements IHandlerThreadPool.CreateHandlerThread
            Return New InteractionHandlerThread(contentWriter, dataWriter)
        End Function

        ''' <summary>
        ''' Gets an available interaction processing thread
        ''' </summary>
        ''' <returns>Interaction processing thread</returns>
        ''' <remarks></remarks>
        Public Overridable Function GetHandlerThread() As IInteractionHandlerThread Implements IHandlerThreadPool.GetHandlerThread
            Dim ret As IInteractionHandlerThread = Nothing
            Dim success As Boolean = False

            While Not success
                For i As Integer = 0 To m_handlerThreads.Count - 1
                    If m_handlerThreads(i).TryToUse Then
                        ret = m_handlerThreads(i)
                        success = True
                        Exit For
                    End If
                Next
                If Not success Then
                    Thread.Sleep(m_configuration.TimeToWaitForThread)
                End If
            End While
            Return ret
        End Function

        ''' <summary>
        ''' Initialize the threads in the pool and register for their events
        ''' </summary>
        ''' <param name="contentWriter">The writer for interaction content files</param>
        ''' <param name="dataWriter">The writer for interaction metadata files</param>
        ''' <remarks></remarks>
        Public Overridable Sub InitializeHandlerThreads(ByVal contentWriter As IContentWriter, ByVal dataWriter As IDataWriter) Implements IHandlerThreadPool.InitializeHandlerThreads
            Dim pool As New List(Of IInteractionHandlerThread)
            For i As Integer = 0 To m_configuration.NumberOfHandlerThreads - 1
                Dim t As IInteractionHandlerThread = CreateHandlerThread(contentWriter, dataWriter)
                AddHandler t.ContentHandled, AddressOf RaiseContentHandled
                AddHandler t.MetaHandled, AddressOf RaiseMetaHandled
                pool.Add(t)
            Next
            m_handlerThreads = pool
        End Sub

        ''' <summary>
        ''' Notify listeners that the interaction content was processed
        ''' </summary>
        ''' <param name="interaction">The interaction data</param>
        ''' <param name="success">Whether processing succeeded or not</param>
        ''' <param name="exception">In case of failure, the exception causing it</param>
        ''' <remarks></remarks>
        Protected Overridable Sub RaiseContentHandled(ByVal interaction As IInteractionData, ByVal success As Boolean, ByVal exception As Exception)
            RaiseEvent ContentHandled(interaction, success, exception)
        End Sub

        ''' <summary>
        ''' Notify listeners that the interaction metadata was processed
        ''' </summary>
        ''' <param name="interaction">The interaction data</param>
        ''' <param name="success">Whether processing succeeded or not</param>
        ''' <param name="exception">In case of failure, the exception causing it</param>
        ''' <remarks></remarks>
        Protected Overridable Sub RaiseMetaHandled(ByVal interaction As IInteractionData, ByVal success As Boolean, ByVal exception As Exception)
            RaiseEvent MetaHandled(interaction, success, exception)
        End Sub

        ''' <summary>
        ''' Number of threads currently running
        ''' </summary>
        ''' <value>Number of threads in use</value>
        ''' <returns>Number of threads in use</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property NumberOfThreadsRunning() As Integer Implements IHandlerThreadPool.NumberOfThreadsRunning
            Get
                Dim ret As Integer = 0
                For Each handler As IInteractionHandlerThread In m_handlerThreads
                    If handler.IsInUse Then
                        ret += 1
                    End If
                Next
                Return ret
            End Get
        End Property
    End Class
End Namespace