Imports utopy.bAgentsAgents
Imports System.ComponentModel
Imports System.Configuration
Imports System.Data
Imports System.Object
Imports System.Threading
Imports System.Data.SqlClient
Imports System.Xml
Imports GenUtils.TraceTopic
Imports ININ.PSO.Tools.PSOTrace


Public MustInherit Class Importer
    Inherits utopy.bGeneralThread.AsyncOperation
    Implements IImporter

    Public Event callImported(ByVal interaction As IInteractionData, ByVal status As IItemStatus) Implements IImporter.InteractionHandled

    Public Event statusChanged(ByVal _status As IImporter.Status, ByVal description As String) Implements IImporter.StatusChanged
    Private lastOutputFolderUsed As Integer
    Protected m_IsLocalDBAvailable As Boolean = False
    'Config variables
    Protected m_localdbMaster As New utopy.bDBServices.DBServices
    Private m_localConStr As String
    Private m_logDBMaster As utopy.bDBServices.DBServices
    Private m_logTableName As String
    Private m_keepLogDays As Integer = -1
    Private m_logCalls As Boolean = False
    Private m_logRecords As DataTable
    Protected m_system As String
    Protected m_xmlDoc As XmlDocument
    Protected m_minRecordStart As DateTime
    Private m_NullableminRecordStart As Nullable(Of DateTime)
    Protected m_lastRecID As Integer
    Private m_NullablelastRecID As Nullable(Of Integer)
    Protected m_batchSize As Integer
    Private m_NullablebatchSize As Nullable(Of Integer)
    Protected m_outputFolder() As String
    Private m_outputFolders As Generic.Dictionary(Of String, String) = Nothing
    Protected m_callFetchUser As String
    Protected m_callFetchPassword As String
    Protected m_callFetchDomain As String
    Private m_startTime As New TimeSpan
    Private m_endTime As New TimeSpan
    Private m_NullableendTime As New Nullable(Of TimeSpan)
    Private m_speechminerVersion As Nullable(Of Single)
    Protected m_workgroup As String
    Protected m_program As String
    Protected m_pollIntervalSecs As Integer = Nothing
    Private m_NullablepollIntervalSecs As Nullable(Of Integer)
    Protected m_connectionStringName As String
    Protected m_configFile As String
    Private m_nHandledCalls As Integer
    Private m_freeSpaceSize As Integer
    Protected m_threadObjArray() As GenerateImporterDataRow
    Private m_threadObjArrayLock As New Object
    Protected m_numThreads As Integer
    Private m_NullablenumThreads As Nullable(Of Integer)
    Protected m_convertVoice As Boolean = False
    Protected m_callIncrementTrackingColumn As String
    Private currentImp As Importer
    Private m_convertThreadArray() As convertAudio
    Protected m_convertedAudioTempFolder As String
    Private m_preConvertPath As String
    Private m_convertThreadObjArrayLock As New Object
    Private m_lastConvAudioTempCount As DateTime = DateTime.MinValue
    Private WithEvents m_monitorTimer As Timers.Timer
    Private m_AudioConvertThread As Thread
    Private m_StandardTimeUtcOffset As Double
    Private m_serverDaylightSavings As Boolean
    Protected m_AudioConvertType As convertAudio.ConvertType

    Protected m_shouldUpdateAgents As Boolean = False
    Private m_agentUpdateThread As Thread
    Protected m_agentUpdateInterval As Integer = 360
    Private m_nullableAgentUpdateInterval As Nullable(Of Integer)
    Protected Shared m_agentSupervisorHash As Generic.Dictionary(Of Integer, String) = Nothing
    Protected Shared m_agentNameHash As Generic.Dictionary(Of Integer, String) = Nothing
    Private Shared m_agentUpdateLockObj As New Object()

    Protected m_metaItemsToUse As Generic.IDictionary(Of String, String) ' mapping from originalName to newName

    Protected m_toolsPath As String
    Private m_skipOutputFolderDiskSpaceCheck As Boolean = False

    Protected m_AudioKeyId As Integer = 0

    Protected m_leftChannelSpeaker As String = Nothing
    Protected m_rightChannelSpeaker As String = Nothing
    Protected m_agentId As String = Nothing

    Protected m_generateWaitTimeSecs As Integer = 10
    Private m_conversionWaitTimeSecs As Integer = 10

    Protected Const OPTION_IDENTIFIER As String = "op_"
    Protected Const OPTION_DEFAULT_VALUE As String = ""
    Protected Const OPTION_USE_DEFAULT_VALUE As Boolean = False
    Protected m_isDifferentFolder As Boolean = False
    Protected m_audioFileConvertion As Boolean = False

    Private m_doRestart As Boolean = False
    Private m_restartInterval As New TimeSpan(3, 0, 0)

    Protected Friend Const SQL_BATCH_SIZE As String = "[BATCH_SIZE]"
    Protected Friend Const SQL_LAST_CALL_ID As String = "[LAST_CALL_ID]"
    Protected Friend Const SQL_LAST_DATE_TIME As String = "[LAST_DATE_TIME]"
    Protected Friend Const SQL_OP_COLUMNS As String = "[OP_COLUMNS]"

    Protected Friend Const RESULTS_COLUMN_OUT_PATH As String = "outPath"
    Protected Friend Const RESULTS_COLUMN_CALL_ID As String = "callId"
    Protected Friend Const RESULTS_COLUMN_SOURCE_PATH As String = "sourcePath"
    Protected Friend Const RESULTS_COLUMN_STARTED_AT As String = "startedAt"
    Protected Friend Const RESULTS_COLUMN_AGENT_DN As String = "agentDN"
    Protected Friend Const RESULTS_COLUMN_ANI As String = "ani"
    Protected Friend Const RESULTS_COLUMN_DNIS As String = "dnis"
    Protected Friend Const RESULTS_COLUMN_DURATION As String = "duration"
    Protected Friend Const RESULTS_COLUMN_XML_FILE As String = "XmlFile"
    Protected Friend Const RESULTS_COLUMN_AGENT_ID As String = "AgentID"
    Protected Friend Const RESULTS_COLUMN_WORKGROUP As String = "Workgroup"
    Protected Friend Const RESULTS_COLUMN_PARTITION As String = "partition"

    Protected Friend Const XML_SETTINGS As String = "Settings"
    Protected Friend Const XML_SETTINGS_CIC As String = "CIC"
    Protected Friend Const XML_SETTINGS_CIC_PRIMARY_CONNECTION_STRING_NAME As String = "CICDataPrimaryConnectionStringName"
    Protected Friend Const XML_SETTINGS_CIC_SECONDARY_CONNECTION_STRING_NAME As String = "CICDataSecondaryConnectionStringName"
    Protected Friend Const XML_SETTINGS_GENERAL_XML_NAME As String = "GeneralXmlName"
    Protected Friend Const XML_SETTINGS_SYSTEM As String = "System"
    Protected Friend Const XML_SETTINGS_SPEECHMINER_VERSION As String = "SpeechMinerVersion"
    Protected Friend Const XML_SETTINGS_START_TIME As String = "StartTime"
    Protected Friend Const XML_SETTINGS_END_TIME As String = "EndTime"
    Protected Friend Const XML_SETTINGS_DO_RESTART As String = "DoRestart"
    Protected Friend Const XML_SETTINGS_RESTART_INTERVAL As String = "RestartIntervalHours"
    Protected Friend Const XML_SETTINGS_POLL_INTERVAL As String = "PollIntervalSecs"
    Protected Friend Const XML_SETTINGS_CONVERT_VOICE As String = "ShouldConvertVoice"
    Protected Friend Const XML_SETTINGS_MSSQL As String = "Mssql"
    Protected Friend Const XML_SETTINGS_ORACLE As String = "Oracle"
    Protected Friend Const XML_SETTINGS_ODBC As String = "ODBC"
    Protected Friend Const XML_SETTINGS_OLEDB As String = "OLEDB"
    Protected Friend Const XML_SETTINGS_SERVICE As String = "Service"
    Protected Friend Const XML_SETTINGS_SERVER As String = "Server"
    Protected Friend Const XML_SETTINGS_PORT As String = "Port"
    Protected Friend Const XML_SETTINGS_DB As String = "Db"
    Protected Friend Const XML_SETTINGS_USER As String = "User"
    Protected Friend Const XML_SETTINGS_PASSWORD As String = "Password"
    Protected Friend Const XML_SETTINGS_NAME As String = "Name"
    Protected Friend Const XML_SETTINGS_CONNECTION_STRING_NAME As String = "ConnectionStringName"
    Protected Friend Const XML_SETTINGS_SPEECHMINER_CONNECTION_STRING_NAME As String = "SpeechMinerConnectionStringName"
    Protected Friend Const XML_SETTINGS_LOCAL_CONNECTION_STRING_NAME As String = "localConnectionStringName"
    Protected Friend Const XML_SETTINGS_LOG_CONNECTION_STRING_NAME As String = "logConnectionStringName"
    Protected Friend Const XML_SETTINGS_CONNECTION_STRING As String = "ConStr"
    Protected Friend Const XML_SETTINGS_SELECT As String = "Select"
    Protected Friend Const XML_SETTINGS_UPDATE_AGENTS As String = "ShouldUpdateAgents"
    Protected Friend Const XML_SETTINGS_AGENT_SELECT As String = "AgentSelect"
    Protected Friend Const XML_SETTINGS_AGENTS_UPDATE_INTERVAL As String = "AgentsUpdateIntervalMinutes"
    Protected Friend Const XML_SETTINGS_CALL_FETCH_USER As String = "CallFetchUser"
    Protected Friend Const XML_SETTINGS_CALL_FETCH_PASSWORD As String = "CallFetchPassword"
    Protected Friend Const XML_SETTINGS_CALL_FETCH_DOMAIN As String = "CallFetchDomain"
    Protected Friend Const XML_SETTINGS_CALL_FETCH_NUM_TRIES As String = "callFetchNumTries"
    Protected Friend Const XML_SETTINGS_CALL_FETCH_TIMEOUT_SECS As String = "callFetchTimeoutSecs"
    Protected Friend Const XML_SETTINGS_SOURCE_HOST As String = "SourceHost"
    Protected Friend Const XML_SETTINGS_SOURCE_FOLDER As String = "SourceFolder"
    Protected Friend Const XML_SETTINGS_OUTPUT_FOLDER As String = "OutputFolder"
    Protected Friend Const XML_SETTINGS_OUTPUT_FOLDERS As String = "OutputFolders"
    Protected Friend Const XML_SETTINGS_OUTPUT_FOLDERS_IS_DIFFERENT_FOLDER As String = "IsDifferentOutputFolder"
    Protected Friend Const XML_SETTINGS_SPEECHMINER_AGENT_SELECT As String = "SpeechMinerAgentSelect"
    Protected Friend Const XML_SETTINGS_PROGRAM As String = "Program"
    Protected Friend Const XML_SETTINGS_WORKGROUP As String = "WorkGroup"
    Protected Friend Const XML_SETTINGS_PARTITIONS As String = "partitions"
    Protected Friend Const XML_SETTINGS_CALL_CENTER_NAME As String = "callCenterName"
    Protected Friend Const XML_SETTINGS_OP_COLUMNS As String = "OpColumns"
    Protected Friend Const XML_SETTINGS_LEFT_CHANNEL_SPEAKER As String = "LeftChannelSpeaker"
    Protected Friend Const XML_SETTINGS_RIGHT_CHANNEL_SPEAKER As String = "RightChannelSpeaker"
    Protected Friend Const XML_SETTINGS_LAST_REC_ID As String = "LastRecID"
    Protected Friend Const XML_SETTINGS_LAST_CALL_TIME As String = "LastCallTime"
    Protected Friend Const XML_SETTINGS_LAST_CALL_ID As String = "LastCallId"
    Protected Friend Const XML_SETTINGS_LAST_DATE_TIME As String = "LastDateTime"
    Protected Friend Const XML_SETTINGS_MIN_RECORD_START As String = "MinRecordStart"
    Protected Friend Const XML_SETTINGS_BATCH_SIZE As String = "BatchSize"
    Protected Friend Const XML_SETTINGS_NUM_THREADS As String = "numThreads"
    Protected Friend Const XML_SETTINGS_FREE_SPACE As String = "freeSpace"
    Protected Friend Const XML_SETTINGS_SKIP_OUTPUT_SPACE_CHECK As String = "SkipOutputFolderDiskSpaceCheck"
    Protected Friend Const XML_SETTINGS_GENERATE_WAIT_TIME As String = "GenerateWaitTime"
    Protected Friend Const XML_SETTINGS_CONVERSION_WAIT_TIME As String = "ConversionWaitTime"
    Protected Friend Const XML_SETTINGS_COLUMN_CALL_INCREMENT_TRACKING As String = "callIncrementTrackingColumn"
    Protected Friend Const XML_SETTINGS_CSV_PATH As String = "CSVpath"
    Protected Friend Const XML_SETTINGS_USED_CSV_PATH As String = "usedCSVpath"
    Protected Friend Const XML_SETTINGS_LOG_PARSER_PATH As String = "logParserPath"
    Protected Friend Const XML_SETTINGS_LOG_PARSER_ARGUMENTS As String = "logParserCommandAurguments"
    Protected Friend Const XML_SETTINGS_LOG_PARSER_IN_FILE As String = "logParserCommandinFileToken"
    Protected Friend Const XML_SETTINGS_LOG_PARSER_OUT_FILE As String = "logParserCommandOutFileToken"
    Protected Friend Const XML_SETTINGS_META_ITEMS_TO_USE As String = "metaItemsToUse"
    Protected Friend Const XML_SETTINGS_LOG_LEVEL As String = "LogLevel"
    Protected Friend Const XML_SETTINGS_CALL_LOG_TABLE As String = "CallLogTable"
    Protected Friend Const XML_SETTINGS_CALL_LOG_RETENTION As String = "CallLogRetentionDays"
    Protected Friend Const XML_SETTINGS_ENCRYPT_AUDIO As String = "EncryptAudio"
    Protected Friend Const XML_SETTINGS_TOOLS_PATH As String = "ToolsPath"
    Protected Friend Const XML_SETTINGS_FFMPEG_PATH As String = "ffmpegPath"
    Protected Friend Const XML_SETTINGS_SCRIPT_EXE_PATH As String = "scriptExePath"
    Protected Friend Const XML_SETTINGS_AUDIO_CONVERT_CMD As String = "AudioFileConversionCommand"
    Protected Friend Const XML_SETTINGS_AUDIO_CONVERT_ARGS As String = "AudioFileConversionCommandArgs"
    Protected Friend Const XML_SETTINGS_G723_TO_WMA_PATH As String = "g723toWMACommandPath"
    Protected Friend Const XML_SETTINGS_WMA_TO_PCM_PATH As String = "WMAtoWAVPCMcommandPath"
    Protected Friend Const XML_SETTINGS_G726_TO_WAV_CMD As String = "g726toWAVCommand"
    Protected Friend Const XML_SETTINGS_G726_TO_WAV_ARGS As String = "g726toWAVArguments"
    Protected Friend Const XML_SETTINGS_G729_TO_WAV_CMD As String = "g729toWAVCommand"
    Protected Friend Const XML_SETTINGS_G729_TO_WAV_ARGS As String = "g729toWAVArguments"
    Protected Friend Const XML_SETTINGS_GSM610_CMD As String = "Gsm610toGsm610Command"
    Protected Friend Const XML_SETTINGS_GSM610_ARGS As String = "Gsm610toGsm610Arguments"
    Protected Friend Const XML_SETTINGS_AUDIO_FORMAT As String = "AudioFormat"
    Protected Friend Const XML_SETTINGS_AUDIO_FORMAT_NODE As String = "/Settings/Audio/Format"
    Protected Friend Const XML_SETTINGS_AUDIO_FORMAT_NAME As String = "Name"
    Protected Friend Const XML_SETTINGS_AUDIO_FORMAT_COMMAND As String = "Command"
    Protected Friend Const XML_SETTINGS_AUDIO_DEFAULT As String = "/Settings/Audio/Default"
    Protected Friend Const XML_SETTINGS_META_ITEM As String = "Settings/MetaItemsToUse/Item"
    Protected Friend Const XML_SETTINGS_META_ITEM_ORIGINAL As String = "OriginalName"
    Protected Friend Const XML_SETTINGS_META_ITEM_NEW As String = "NewName"
    Protected Friend Const XML_SETTINGS_MIN_CALL_AGE As String = "MinimumCallAge"
    Protected Friend Const XML_SETTINGS_AGENTS As String = "Agents"
    Protected Friend Const XML_SETTINGS_AGENTS_FILE As String = "File"
    Protected Friend Const XML_SETTINGS_AGENTS_FOLDER As String = "Folder"
    Protected Friend Const XML_SETTINGS_AGENTS_ARCHIVE_FOLDER As String = "ArchiveFolder"
    Protected Friend Const XML_SETTINGS_AGENTS_FIELD_ID As String = "IDField"
    Protected Friend Const XML_SETTINGS_AGENTS_FIELD_NAME As String = "NameField"
    Protected Friend Const XML_SETTINGS_AGENTS_FIELD_TEAM As String = "TeamField"
    Protected Friend Const XML_SETTINGS_AGENTS_FIELD_SUPERVISOR As String = "SupervisorField"
    Protected Friend Const XML_SETTINGS_AGENTS_FIELD_MANAGER As String = "ManagerField"
    Protected Friend Const XML_SETTINGS_AGENTS_PBX_ID_FIELDS As String = "PbxIdFields"
    Protected Friend Const XML_SETTINGS_AGENTS_PBX_ID_FIELDS_FIELDS As String = "Field"
    Protected Friend Const XML_SETTINGS_AGENTS_PBX_ID_FIELDS_FIELDS_ORDER As String = "Order"
    Protected Friend Const XML_SETTINGS_FTP As String = "FTP"
    Protected Friend Const XML_SETTINGS_EMAIL_NOTIFICATIONS_TO As String = "EmailNotificationsTo"
    Protected Friend Const XML_SETTINGS_SITE_ID As String = "SiteID"
    Protected Friend Const XML_SETTINGS_IMPERSONATION_USER As String = "ImpersonationUser"
    Protected Friend Const XML_SETTINGS_IMPERSONATION_DOMAIN As String = "ImpersonationDomain"
    Protected Friend Const XML_SETTINGS_IMPERSONATION_PASSWORD As String = "ImpersonationPassword"
    Protected Friend Const XML_SETTINGS_RECORDINGS_FILE_SYSTEM_USER As String = "RecordingsFileSystemUser"
    Protected Friend Const XML_SETTINGS_RECORDINGS_FILE_SYSTEM_PASSWORD As String = "RecordingsFileSystemPassword"
    Protected Friend Const XML_SETTINGS_AGENT_ID As String = "AgentId"
    Protected Friend Const XML_SETTINGS_AUDIO_FILE_CONVERSION As String = "AudioFileConversion"


    Protected Friend Const XML_OUTPUT_COMMENT As String = "This file includes the meta information for the call"
    Protected Friend Const XML_OUTPUT_CALLINFORMATION As String = "callInformation"
    Protected Friend Const XML_OUTPUT_MEDIATYPE As String = "mediaType"
    Protected Friend Const XML_OUTPUT_CALLTIME As String = "callTime"
    Protected Friend Const XML_OUTPUT_TEXTTIME As String = "textTime"
    Protected Friend Const XML_OUTPUT_CALL_LENGTH As String = "callLength"
    Protected Friend Const XML_OUTPUT_PROGRAMID As String = "programID"
    Protected Friend Const XML_OUTPUT_AUDIOFORMAT As String = "audioFormat"
    Protected Friend Const XML_OUTPUT_TEXTFORMAT As String = "textFormat"
    Protected Friend Const XML_OUTPUT_ENCRYPTION_KEY As String = "encryptionKey"
    Protected Friend Const XML_OUTPUT_PARTITIONS As String = "partitions"
    Protected Friend Const XML_OUTPUT_PARTITION As String = "partition"
    Protected Friend Const XML_OUTPUT_SPEAKERS As String = "speakers"
    Protected Friend Const XML_OUTPUT_SPEAKER As String = "speaker"
    Protected Friend Const XML_OUTPUT_SPEAKER_ID As String = "id"
    Protected Friend Const XML_OUTPUT_SPEAKER_TYPE As String = "speakerType"
    Protected Friend Const XML_OUTPUT_SPEAKER_TYPE_AGENT As String = "agent"
    Protected Friend Const XML_OUTPUT_SPEAKER_START_TIME As String = "startTime"
    Protected Friend Const XML_OUTPUT_SPEAKER_END_TIME As String = "endTime"
    Protected Friend Const XML_OUTPUT_SPEAKER_WORKGROUP As String = "workgroup"
    Protected Friend Const XML_OUTPUT_CHANNELS As String = "channels"
    Protected Friend Const XML_OUTPUT_CHANNEL_LEFT As String = "left"
    Protected Friend Const XML_OUTPUT_CHANNEL_RIGHT As String = "right"
    Protected Friend Const XML_OUTPUT_CUSTOMER_ID As String = "customerID"
    Protected Friend Const XML_OUTPUT_OTHER As String = "other"
    Protected Friend Const XML_OUTPUT_AGENT_DN As String = "agentDN"
    Protected Friend Const XML_OUTPUT_ANI As String = "ani"
    Protected Friend Const XML_OUTPUT_DNIS As String = "dnis"

    Protected Friend Const CALL_STATUS_OK As String = "OK"
    Protected Friend Const CALL_STATUS_FAIL As String = "Failed"
    Protected Friend Const CALL_STATUS_EXCEPTION As String = "Exception"

    Public Overloads Sub Start() Implements IImporter.Start
        MyBase.Start()
    End Sub

    Public Overloads Sub Cancel() Implements IImporter.Cancel
        MyBase.Cancel()
    End Sub

    Public ReadOnly Property DoRestart() As Boolean Implements IImporter.DoRestart
        Get
            Return m_doRestart
        End Get
    End Property

    Protected Overridable Function FormatLogMsg(
        level As String,
        msg As String
        ) As String

        Return String.Format("[{0}] {1}: {2}", Me.GetType().FullName, level.ToUpper(), msg)
    End Function

    Public ReadOnly Property RestartInterval() As TimeSpan Implements IImporter.RestartInterval
        Get
            Return m_restartInterval
        End Get
    End Property

    Public Sub raiseCallImported(ByVal _callId As String)
        m_nHandledCalls += 1
        FireAsync(callImportedEvent, New InteractionData(_callId), New ItemStatus(True))
    End Sub

    Protected Overridable Sub OnStatusChanged(ByVal _status As String)
        Dim status As IImporter.Status
        Select Case _status
            Case "Starting"
                status = IImporter.Status.STARTING
            Case "Getting", "Adding", "Fetching"
                status = IImporter.Status.FETCHING
            Case "Stopped"
                status = IImporter.Status.STOPPED
            Case "Idle"
                status = IImporter.Status.IDLE
            Case "ERROR!"
                status = IImporter.Status.ERROR
            Case Else
                status = IImporter.Status.OTHER
        End Select
        FireAsync(statusChangedEvent, status, _status)
    End Sub

    Public Sub New(ByVal _isi As ISynchronizeInvoke)
        MyBase.New(_isi)
    End Sub

    Public Sub New(ByVal _isi As System.ComponentModel.ISynchronizeInvoke, ByVal _configFile As String)
        Me.New(_isi)
        m_configFile = _configFile
    End Sub

    Public Overridable Sub init() Implements IImporter.Init
        Using (TraceTopic.importerTopic.scope())
            Try
                ConnectToLocalDB()
                LoadXML()
                registerForStatus()
                updateStatus(IImporter.Status.STARTING, "Starting")
                If m_logTableName IsNot Nothing Then
                    initLogDb()
                End If
                initThreadPool()
                lastOutputFolderUsed = 0
                If m_convertVoice Then
                    initTempFolder()
                    initConvertThread()
                End If

                m_nHandledCalls = 0
                initMonitorTimer()

                Try
                    If m_shouldUpdateAgents = True Then
                        initAgentUpdateThread()
                    End If
                Catch ex As Exception
                    TraceTopic.importerTopic.warning("Could not get agent data. {}", ex)
                End Try
            Catch ex As Exception
                'Adding EventID from CustomEventId
                PSOTrace.WriteRegisteredMessage(8011)
            End Try
        End Using
    End Sub

    Public Sub registerForStatus()
        If m_IsLocalDBAvailable AndAlso m_speechminerVersion >= 8.1 Then
            AddHandler statusChanged, AddressOf updateStatus
            AddHandler callImported, AddressOf updateCall
        End If
    End Sub

    Protected ReadOnly Property ConfigFileName() As String
        Get
            Return IO.Path.GetFileNameWithoutExtension(m_configFile)
        End Get
    End Property

    Protected ReadOnly Property ComputerName() As String
        Get
            Return System.Net.Dns.GetHostName()
        End Get
    End Property

    Public Sub updateStatus(ByVal _status As IImporter.Status, ByVal _description As String)
        Using (TraceTopic.importerTopic.scope())
            Try
                If m_speechminerVersion >= 8.1 AndAlso m_IsLocalDBAvailable Then
                    Dim nameParam As New SqlParameter("@name", ConfigFileName)
                    Dim computerParam As New SqlParameter("@computer", ComputerName)
                    Dim systemParam As New SqlParameter("@system", m_system)
                    Dim statusParam As New SqlParameter("@status", _description)
                    m_localdbMaster.runSQL("IF EXISTS(SELECT * FROM monitorConnectorsTbl WHERE name=@name AND computer=@computer)" & vbCrLf &
                                           "  UPDATE monitorConnectorsTbl SET status=@status,lifesign=" & m_localdbMaster.getDBTime() & " WHERE name=@name AND computer=@computer" & vbCrLf &
                                           "ELSE" & vbCrLf &
                                           "  INSERT INTO monitorConnectorsTbl VALUES(@name, @computer, @system, @status, 1, 0, 0, 0, null, " & m_localdbMaster.getDBTime() & ")",
                                           New SqlParameter() {nameParam, computerParam, systemParam, statusParam})
                End If
            Catch ex As Exception
                TraceTopic.importerTopic.warning("Error in updateStatus. Status: {}, ConfigFileName: {}, {}", _status, ConfigFileName, ex)
            End Try
        End Using
    End Sub

    Public Sub updateCall(ByVal _call As IInteractionData, ByVal _status As IItemStatus)
        Using (TraceTopic.importerTopic.scope())
            Try
                If m_IsLocalDBAvailable AndAlso _status.Success Then

                    Dim nameParam As New SqlParameter("@name", ConfigFileName)
                    Dim computerParam As New SqlParameter("@computer", ComputerName)
                    Dim idParam As New SqlParameter("@callId", _call.ID)
                    m_localdbMaster.runSQL("UPDATE monitorConnectorsTbl SET lastCall=@callId,lifesign=" & m_localdbMaster.getDBTime() & " WHERE name=@name AND computer=@computer",
                                           New SqlParameter() {nameParam, computerParam, idParam})
                End If
            Catch ex As Exception
                TraceTopic.importerTopic.warning("Error in updateCall. _callId: {}, ConfigFileName: {}, {}", _call.ID, ConfigFileName, ex)
            End Try
        End Using
    End Sub

    Public Overridable Sub UpdateAgents() Implements IImporter.UpdateAgents
        Using (TraceTopic.importerTopic.scope())
            Try
                OnStatusChanged("Getting")
                LoadXML()
                ConnectToLocalDB()
                If Not m_IsLocalDBAvailable Then
                    doError("Cannot get agents. Local database not available")
                    Return
                End If
                Dim agentPartitions As String() = Nothing
                Dim agentEntities As Agent() = Nothing
                Try
                    If m_speechminerVersion >= 7.0 Then
                        agentEntities = GetAgents()
                    Else
                        agentPartitions = GetAgentPartitions()
                    End If
                Catch nie As NotImplementedException
                    doError("Getting agents is not supported by the importer")
                    Return
                End Try
                OnStatusChanged("Adding")
                Dim dbMaster As New utopy.bDBServices.DBServices()
                dbMaster.setDB(SpeechminerConnectionString)
                Dim agentManager As utopy.bAgentsAgents.AgentManager = GetAgentManager(dbMaster)
                ' note that if you set forceUniqueAgentNames to false this you also need to remove the unique index on the 'name' column in the table 'agentEntityTbl'
                If m_speechminerVersion >= 7.0 Then
                    agentManager.setAgents(agentEntities)
                Else
                    addPartitions(agentPartitions, dbMaster)
                End If
                OnStatusChanged("Stopped")
                TraceTopic.importerTopic.note("Updated agents")
            Catch ex As Exception
                doError(ex.Message, ex.ToString)
            End Try
        End Using
    End Sub

    Protected Overridable Function GetAgentManager(ByVal _dbMaster As utopy.bDBServices.DBServices) As utopy.bAgentsAgents.AgentManager
        Dim agentManager As New AgentManager(_dbMaster)
        agentManager.forceUniqueAgentNames = False
        agentManager.shouldKeepOtherSiteAgents = False
        Return agentManager
    End Function

    Public MustOverride Sub Quit() Implements IImporter.Quit

    'Public Overridable Function GetNumberOfWaitingCalls() As Integer
    '    Return 1
    'End Function

    Protected MustOverride Function ImportNextBatch() As Boolean Implements IImporter.ImportNextBatch
    Public MustOverride Sub GenerateMetaFile(ByVal _fileName As String, ByVal _dataRow As DataRow)
    Public MustOverride Function GenerateAudioFile(ByVal _filename As String, Optional ByVal _outPath As String = "", Optional ByVal _inum As String = "", Optional ByVal _recorderPath As String = "", Optional ByVal _callTime As String = Nothing, Optional ByVal _dataRow As DataRow = Nothing) As Boolean

    Protected Overridable Function GetAgentPartitions() As String()
        Throw New NotImplementedException
    End Function

    Protected Overridable Function GetAgents() As Agent()
        Throw New NotImplementedException
    End Function

    Public Sub quitConvertThread()
        Using (TraceTopic.importerTopic.scope())
            If m_AudioConvertThread IsNot Nothing AndAlso m_AudioConvertThread.IsAlive Then
                TraceTopic.importerTopic.note("Stopping audio conversion thread for program: {}", m_program)
                m_AudioConvertThread.Abort()
            End If
        End Using
    End Sub

    Public Sub quitAgentUpdateThread()
        Using (TraceTopic.importerTopic.scope())
            If m_agentUpdateThread IsNot Nothing AndAlso m_agentUpdateThread.IsAlive Then
                TraceTopic.importerTopic.note("Stopping agent update thread for program: {}", m_program)
                m_agentUpdateThread.Abort()
            End If
        End Using
    End Sub

    Public Overridable Sub setAudioConvertType()
        m_AudioConvertType = convertAudio.ConvertType.NO_CONVERSION_TYPE
    End Sub

    Public Shared Function createImporter(ByVal _isi As ISynchronizeInvoke, ByVal _configFile As String) As IImporter
        Using (TraceTopic.importerTopic.scope())
            Dim xmlDoc As New Xml.XmlDocument
            Try
                Dim readerSettings = New XmlReaderSettings()
                readerSettings.IgnoreComments = True
                Dim reader = XmlReader.Create(_configFile, readerSettings)
                xmlDoc.Load(reader)
                reader.Close()
            Catch ex As Exception
                TraceTopic.importerTopic.error("Failed to load the config file {}", ex)
                Throw
            End Try
            Try
                Dim importerType As String = xmlDoc(XML_SETTINGS)(XML_SETTINGS_SYSTEM).InnerText
                Dim assemblyType As Type = Type.GetType(importerType)
                If assemblyType Is Nothing Then
                    Throw New TypeLoadException("Assembly for " & importerType & " not found")
                End If
                Dim ci As System.Reflection.ConstructorInfo = assemblyType.GetConstructor(New Type() {GetType(ISynchronizeInvoke), GetType(String)})
                Return CType(ci.Invoke(New Object() {_isi, _configFile}), IImporter)
            Catch ex As Exception
                TraceTopic.importerTopic.error("Exception in createImporter {}", ex)
                Throw
            End Try
        End Using
        Return Nothing
    End Function

    Private Sub initTempFolder()
        m_convertedAudioTempFolder = IO.Directory.GetParent(My.Application.Info.DirectoryPath).Parent.FullName & "\audio_temp"
        If IO.Directory.Exists(m_convertedAudioTempFolder) = False Then
            IO.Directory.CreateDirectory(m_convertedAudioTempFolder)
        End If
        Try
            For Each f As String In IO.Directory.GetFiles(m_convertedAudioTempFolder, "*.wav")
                IO.File.Delete(f)
            Next
            For Each f As String In IO.Directory.GetFiles(m_convertedAudioTempFolder, "*.xml")
                IO.File.Delete(f)
            Next
        Catch ex As Exception
        End Try
    End Sub

    Private Sub initConvertThread()
        m_preConvertPath = m_convertedAudioTempFolder & "\" & m_program.Replace(" "c, "_"c) & "_nonconverted_calls"
        If IO.Directory.Exists(m_preConvertPath) = False Then
            IO.Directory.CreateDirectory(m_preConvertPath)
        End If

        setAudioConvertType()
        m_AudioConvertThread = New Thread(AddressOf convertMainLoop)
        m_AudioConvertThread.Start()
    End Sub

    Private Sub initMonitorTimer()
        m_monitorTimer = New Timers.Timer(60 * 1000)
        m_monitorTimer.AutoReset = False
        m_monitorTimer.Start()
    End Sub

    Protected Sub initAgentUpdateThread()
        m_agentUpdateThread = New Thread(AddressOf AgentUpdateMainFunc)
        m_agentUpdateThread.Name = "Agent Update"
        m_agentUpdateThread.Start()
    End Sub

    Private Sub initConvertThreadPool()
        Dim arr As New List(Of convertAudio)
        For i As Integer = 0 To m_numThreads
            Dim newObj As New convertAudio
            arr.Add(newObj)
        Next

        m_convertThreadArray = arr.ToArray
    End Sub

    Protected Sub initThreadPool()
        Dim arr As New List(Of GenerateImporterDataRow)
        For i As Integer = 0 To m_numThreads - 1
            Dim newObj As New GenerateImporterDataRow()
            arr.Add(newObj)
        Next

        m_threadObjArray = arr.ToArray
    End Sub

    Protected Sub generateFromDataRow(ByVal _row As DataRow, Optional ByVal _doDebug As Boolean = True)
        Using (TraceTopic.importerTopic.scope())
            Dim generated As Boolean = False
            While generated = False
                ' check space on output folder and wait if space is low
                Dim outPath As String = CStr(_row(RESULTS_COLUMN_OUT_PATH))

                If GetAvailableSpaceGB(IO.Path.GetDirectoryName(outPath)) > m_freeSpaceSize Then
                    currentImp = Me
                    Dim m_obj As GenerateImporterDataRow = GetFreeThreadObj()
                    m_obj.m_dataRow = _row
                    m_obj.m_program = m_program
                    m_obj.m_doDebug = _doDebug
                    SyncLock Me
                        m_obj.imp = currentImp
                        m_obj.go()
                    End SyncLock

                    raiseCallImported(_row(RESULTS_COLUMN_CALL_ID).ToString())
                    generated = True

                Else
                    TraceTopic.importerTopic.warning("storage is low on local drive {}. Will wait and try again. ", outPath)
                    Thread.Sleep(60000)
                End If
            End While
        End Using
    End Sub

    Protected ReadOnly Property NumWorkingThreads() As Integer
        Get
            Dim nThreads As Integer = 0
            SyncLock m_threadObjArrayLock
                For i As Integer = 0 To m_numThreads - 1
                    If m_threadObjArray(i).isInUse Then
                        nThreads += 1
                    End If
                Next
            End SyncLock
            Return nThreads
        End Get
    End Property

    Private Function GetFreeThreadObj() As GenerateImporterDataRow
        Dim ret As GenerateImporterDataRow = Nothing
        Dim success As Boolean = False

        SyncLock m_threadObjArrayLock
            While Not success
                For i As Integer = 0 To m_numThreads - 1
                    If Not m_threadObjArray(i).isInUse() Then
                        ret = m_threadObjArray(i)
                        success = True
                        Exit For
                    End If
                Next
                If Not success Then
                    Thread.Sleep(m_generateWaitTimeSecs * 1000)
                End If
            End While
        End SyncLock

        Return ret
    End Function

    Private Function GetFreeConvertThreadObj() As convertAudio
        Dim ret As convertAudio = Nothing
        Dim success As Boolean = False

        SyncLock m_convertThreadObjArrayLock
            While Not success
                For i As Integer = 0 To m_numThreads - 1
                    If Not m_convertThreadArray(i).isInUse Then
                        ret = m_convertThreadArray(i)
                        ret.setInUse()
                        success = True
                        Exit For
                    End If
                Next
                If Not success Then
                    Thread.Sleep(m_conversionWaitTimeSecs * 1000)
                End If
            End While
        End SyncLock

        Return ret
    End Function

    Protected Shared Function inTimeWindow(ByVal _currTime As TimeSpan, ByVal _startTime As TimeSpan, ByVal _endTime As TimeSpan) As Boolean
        If _startTime.TotalMinutes < _endTime.TotalMinutes AndAlso _currTime.TotalMinutes > _startTime.TotalMinutes AndAlso _currTime.TotalMinutes < _endTime.TotalMinutes Then
            Return True
        ElseIf _startTime.TotalMinutes > _endTime.TotalMinutes AndAlso (_currTime.TotalMinutes > _startTime.TotalMinutes OrElse _currTime.TotalMinutes < _endTime.TotalMinutes) Then
            Return True
        ElseIf _startTime.TotalMinutes = _endTime.TotalMinutes Then
            Return True
        End If
        Return False
    End Function

    Protected Function getOptionals(ByVal _row As DataRow) As Generic.IDictionary(Of String, String)
        Dim ret As New Generic.Dictionary(Of String, String)
        For Each c As DataColumn In _row.Table.Columns
            If c.ColumnName.StartsWith(OPTION_IDENTIFIER.ToLower) OrElse c.ColumnName.StartsWith(OPTION_IDENTIFIER.ToUpper) Then
                Dim value As Object = _row(c.ColumnName)
                If Not value Is DBNull.Value Then
                    ret.Add(c.ColumnName.Substring(3), CStr(_row(c.ColumnName)).Trim)
                ElseIf useOptionalDefaultValue() Then
                    ret.Add(c.ColumnName.Substring(3), getOptionalDefaultValue())
                End If
            End If
        Next
        Return ret
    End Function

    Protected Overridable Function getOptionalDefaultValue() As String
        Return OPTION_DEFAULT_VALUE
    End Function

    Protected Overridable Function useOptionalDefaultValue() As Boolean
        Return OPTION_USE_DEFAULT_VALUE
    End Function

    Protected Overridable Function initLoggerWithDB() As Boolean
        Return True
    End Function

    Protected Overridable Function ConnectToLocalDB() As Boolean
        If m_IsLocalDBAvailable = True Then
            Return True
        End If

        m_IsLocalDBAvailable = ConnectToMSSqlDb(XML_SETTINGS_LOCAL_CONNECTION_STRING_NAME, m_localdbMaster, m_localConStr)
        Return m_IsLocalDBAvailable
    End Function

    Protected Overridable Sub initLogDb()
        m_logCalls = ConnectToMSSqlDb(XML_SETTINGS_LOG_CONNECTION_STRING_NAME, m_logDBMaster)
        If m_logCalls Then
            m_logRecords = New DataTable()
            m_logRecords.Columns.Add("ProcessTime", GetType(DateTime))
            m_logRecords.Columns.Add("Status", GetType(String))
            For i As Integer = 0 To nLogFields() - 1
                m_logRecords.Columns.Add("field" & i)
            Next
        End If
    End Sub

    Protected Overridable Function nLogFields() As Integer
        Return 1 ' Default is just ID
    End Function

    Protected Function ConnectToMSSqlDb(ByVal _conSetting As String, ByRef _dbMaster As utopy.bDBServices.DBServices, Optional ByRef _conStr As String = Nothing) As Boolean
        Dim xmlDoc As New Xml.XmlDocument
        Dim readerSettings = New XmlReaderSettings()
        readerSettings.IgnoreComments = True
        Dim reader = XmlReader.Create(m_configFile, readerSettings)
        xmlDoc.Load(reader)
        reader.Close()
        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_MSSQL) IsNot Nothing Then
            If (xmlDoc(XML_SETTINGS)(XML_SETTINGS_MSSQL)(_conSetting)) IsNot Nothing Then
                Dim localconnectionStringName As String = xmlDoc(XML_SETTINGS)(XML_SETTINGS_MSSQL)(_conSetting).InnerText
                _conStr = loadConnectionString(localconnectionStringName)
                If _dbMaster Is Nothing Then
                    _dbMaster = New utopy.bDBServices.DBServices()
                End If
                Return _dbMaster.setDB(_conStr, False)
            End If
        End If
        Return False
    End Function

    Protected Overridable ReadOnly Property SpeechminerConnectionString() As String
        Get
            Return m_localConStr
        End Get
    End Property

    Public Property CustomEventId As Object

    Protected Overridable Sub loadGeneralXml(ByVal fileName As String)
        Dim xmlDoc As New Xml.XmlDocument

        Dim readerSettings = New XmlReaderSettings()
        readerSettings.IgnoreComments = True
        Dim reader = XmlReader.Create(fileName, readerSettings)
        xmlDoc.Load(reader)
        reader.Close()

        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_SYSTEM) IsNot Nothing Then
            m_system = CStr(xmlDoc(XML_SETTINGS)(XML_SETTINGS_SYSTEM).InnerText.Trim)
        End If

        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_BATCH_SIZE) IsNot Nothing Then
            m_NullablebatchSize = CInt(xmlDoc(XML_SETTINGS)(XML_SETTINGS_BATCH_SIZE).InnerText.Trim)
        End If

        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_LAST_REC_ID) IsNot Nothing Then
            m_NullablelastRecID = CInt(xmlDoc(XML_SETTINGS)(XML_SETTINGS_LAST_REC_ID).InnerText.Trim)
        End If

        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_MIN_RECORD_START) IsNot Nothing Then
            m_NullableminRecordStart = CDate(xmlDoc(XML_SETTINGS)(XML_SETTINGS_MIN_RECORD_START).InnerText.Trim)
        End If

        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_SPEECHMINER_VERSION) IsNot Nothing Then
            m_speechminerVersion = CSng(xmlDoc(XML_SETTINGS)(XML_SETTINGS_SPEECHMINER_VERSION).InnerText)
        End If

        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_PROGRAM) IsNot Nothing Then
            m_program = xmlDoc(XML_SETTINGS)(XML_SETTINGS_PROGRAM).InnerText.Trim
        End If

        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_CALL_FETCH_USER) IsNot Nothing Then
            m_callFetchUser = xmlDoc(XML_SETTINGS)(XML_SETTINGS_CALL_FETCH_USER).InnerText.Trim
        End If

        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_CALL_FETCH_PASSWORD) IsNot Nothing Then
            m_callFetchPassword = xmlDoc(XML_SETTINGS)(XML_SETTINGS_CALL_FETCH_PASSWORD).InnerText.Trim
        End If

        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_CALL_FETCH_DOMAIN) IsNot Nothing Then
            m_callFetchDomain = xmlDoc(XML_SETTINGS)(XML_SETTINGS_CALL_FETCH_DOMAIN).InnerText.Trim
        End If

        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_END_TIME) IsNot Nothing Then
            m_NullableendTime = TimeSpan.FromHours(CDbl(xmlDoc(XML_SETTINGS)(XML_SETTINGS_END_TIME).InnerText))
        End If

        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_POLL_INTERVAL) IsNot Nothing Then
            m_NullablepollIntervalSecs = CInt(xmlDoc(XML_SETTINGS)(XML_SETTINGS_POLL_INTERVAL).InnerText)
        End If

        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_WORKGROUP) IsNot Nothing Then
            m_workgroup = CStr(xmlDoc(XML_SETTINGS)(XML_SETTINGS_WORKGROUP).InnerText)
        End If

        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_COLUMN_CALL_INCREMENT_TRACKING) IsNot Nothing Then
            m_callIncrementTrackingColumn = xmlDoc(XML_SETTINGS)(XML_SETTINGS_COLUMN_CALL_INCREMENT_TRACKING).InnerText
        End If
        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_NUM_THREADS) IsNot Nothing Then
            m_NullablenumThreads = CInt(xmlDoc(XML_SETTINGS)(XML_SETTINGS_NUM_THREADS).InnerText)
        End If
        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_CONVERT_VOICE) IsNot Nothing Then
            Boolean.TryParse(xmlDoc(XML_SETTINGS)(XML_SETTINGS_CONVERT_VOICE).InnerText, m_convertVoice)
        End If

        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_UPDATE_AGENTS) IsNot Nothing Then
            Boolean.TryParse(xmlDoc(XML_SETTINGS)(XML_SETTINGS_UPDATE_AGENTS).InnerText, m_shouldUpdateAgents)
        End If
        If xmlDoc(XML_SETTINGS)(XML_SETTINGS_AGENTS_UPDATE_INTERVAL) IsNot Nothing Then
            m_nullableAgentUpdateInterval = CInt(xmlDoc(XML_SETTINGS)(XML_SETTINGS_AGENTS_UPDATE_INTERVAL).InnerText)
        End If
    End Sub
    Protected Function GetAdvanisSatisfaction(ByVal satisfactionVal As Integer) As String
        Dim result As String = Nothing
        Select Case satisfactionVal
            Case 1
                result = "0.000"
            Case 2
                result = "0.250"
            Case 3
                result = "0.500"
            Case 4
                result = "0.750"
            Case 5
                result = "1.000"
        End Select

        Return result
    End Function

    Protected Function GetAdvanisYesNo(ByVal yesNoVal As String) As String
        Dim result As String = Nothing
        If yesNoVal = "2" Then
            result = "0.000"
        ElseIf yesNoVal = "1" Then
            result = "1.000"
        End If
        Return result
    End Function

    Protected Overridable Sub LoadXML()
        If m_xmlDoc IsNot Nothing Then
            Return
        End If
        Using (TraceTopic.importerTopic.scope())
            Try
                Dim readerSettings = New XmlReaderSettings()
                readerSettings.IgnoreComments = True

                Dim reader = XmlReader.Create(m_configFile, readerSettings)
                m_xmlDoc = New XmlDocument
                m_xmlDoc.Load(reader)
                reader.Close()
                Dim eleSettings = m_xmlDoc(XML_SETTINGS)

                Dim eleMsSql = eleSettings(XML_SETTINGS_MSSQL)
                If eleMsSql IsNot Nothing AndAlso eleMsSql(XML_SETTINGS_LOCAL_CONNECTION_STRING_NAME) IsNot Nothing Then
                    Dim connStringName = eleMsSql(XML_SETTINGS_LOCAL_CONNECTION_STRING_NAME).InnerText
                    m_localConStr = loadConnectionString(connStringName)
                End If

                If Not eleSettings(XML_SETTINGS_GENERAL_XML_NAME) Is Nothing Then
                    loadGeneralXml(eleSettings(XML_SETTINGS_GENERAL_XML_NAME).InnerText)
                End If
                If (m_system Is Nothing) Then
                    If eleSettings(XML_SETTINGS_SYSTEM).InnerText IsNot Nothing Then
                        m_system = CStr(eleSettings(XML_SETTINGS_SYSTEM).InnerText)
                    End If
                End If
                If (m_NullablebatchSize.HasValue) Then
                    m_batchSize = m_NullablebatchSize.Value
                Else
                    If eleSettings(XML_SETTINGS_BATCH_SIZE) Is Nothing Then
                        m_batchSize = 30
                    Else
                        m_batchSize = CInt(eleSettings(XML_SETTINGS_BATCH_SIZE).InnerText.Trim)
                    End If
                End If

                If (m_NullablelastRecID.HasValue) Then
                    m_lastRecID = m_NullablelastRecID.Value
                Else
                    If eleSettings(XML_SETTINGS_LAST_REC_ID) IsNot Nothing Then
                        m_lastRecID = CInt(eleSettings(XML_SETTINGS_LAST_REC_ID).InnerText.Trim)
                    End If
                End If
                If (m_NullableminRecordStart.HasValue) Then
                    m_minRecordStart = m_NullableminRecordStart.Value
                Else
                    If eleSettings(XML_SETTINGS_MIN_RECORD_START) IsNot Nothing Then
                        m_minRecordStart = CDate(eleSettings(XML_SETTINGS_MIN_RECORD_START).InnerText.Trim)
                    End If
                End If
                If (m_speechminerVersion Is Nothing) Then

                    If eleSettings(XML_SETTINGS_SPEECHMINER_VERSION) Is Nothing Then
                        m_speechminerVersion = 5.0
                    Else
                        m_speechminerVersion = CSng(eleSettings(XML_SETTINGS_SPEECHMINER_VERSION).InnerText)
                    End If
                End If
                initOutputFolder(m_xmlDoc)
                If (m_program Is Nothing) Then

                    If eleSettings(XML_SETTINGS_PROGRAM) IsNot Nothing Then
                        m_program = eleSettings(XML_SETTINGS_PROGRAM).InnerText.Trim
                    End If
                End If
                If Not validateProgram() Then
                    TraceTopic.importerTopic.note("The program: {} does not exist or is not active", m_program)
                End If
                If (m_callFetchUser Is Nothing) Then

                    If eleSettings(XML_SETTINGS_CALL_FETCH_USER) IsNot Nothing Then
                        m_callFetchUser = eleSettings(XML_SETTINGS_CALL_FETCH_USER).InnerText.Trim
                    End If
                End If
                If (m_callFetchPassword Is Nothing) Then

                    If eleSettings(XML_SETTINGS_CALL_FETCH_PASSWORD) IsNot Nothing Then
                        m_callFetchPassword = eleSettings(XML_SETTINGS_CALL_FETCH_PASSWORD).InnerText.Trim
                    End If
                End If
                If (m_callFetchDomain Is Nothing) Then

                    If eleSettings(XML_SETTINGS_CALL_FETCH_DOMAIN) IsNot Nothing Then
                        m_callFetchDomain = eleSettings(XML_SETTINGS_CALL_FETCH_DOMAIN).InnerText.Trim
                    End If
                End If
                If (m_NullableendTime.HasValue) Then
                    m_endTime = m_NullableendTime.Value
                Else
                    If eleSettings(XML_SETTINGS_END_TIME) IsNot Nothing Then
                        m_endTime = TimeSpan.FromHours(CDbl(eleSettings(XML_SETTINGS_END_TIME).InnerText))
                    End If
                End If
                If (m_NullablepollIntervalSecs.HasValue) Then
                    m_pollIntervalSecs = m_NullablepollIntervalSecs.Value
                Else
                    If eleSettings(XML_SETTINGS_POLL_INTERVAL) IsNot Nothing Then
                        m_pollIntervalSecs = CInt(eleSettings(XML_SETTINGS_POLL_INTERVAL).InnerText)
                    End If
                End If
                If (m_workgroup Is Nothing) Then

                    If eleSettings(XML_SETTINGS_WORKGROUP) IsNot Nothing Then
                        m_workgroup = CStr(eleSettings(XML_SETTINGS_WORKGROUP).InnerText)
                    End If
                End If
                If (m_callIncrementTrackingColumn Is Nothing) Then

                    If eleSettings(XML_SETTINGS_COLUMN_CALL_INCREMENT_TRACKING) IsNot Nothing Then
                        m_callIncrementTrackingColumn = eleSettings(XML_SETTINGS_COLUMN_CALL_INCREMENT_TRACKING).InnerText
                    End If
                End If
                If (m_NullablenumThreads.HasValue) Then
                    m_numThreads = m_NullablenumThreads.Value
                Else
                    If eleSettings(XML_SETTINGS_NUM_THREADS) Is Nothing Then
                        m_numThreads = 1
                    Else
                        m_numThreads = CInt(eleSettings(XML_SETTINGS_NUM_THREADS).InnerText)
                    End If
                End If

                If eleSettings(XML_SETTINGS_CONVERT_VOICE) Is Nothing Then
                    m_convertVoice = False
                Else
                    m_convertVoice = CBool(eleSettings(XML_SETTINGS_CONVERT_VOICE).InnerText)
                End If

                If eleSettings(XML_SETTINGS_TOOLS_PATH) Is Nothing Then
                    m_toolsPath = ""
                Else
                    m_toolsPath = CStr(eleSettings(XML_SETTINGS_TOOLS_PATH).InnerText)
                End If
                If eleSettings(XML_SETTINGS_FREE_SPACE) Is Nothing Then
                    m_freeSpaceSize = 2
                Else
                    m_freeSpaceSize = CInt(eleSettings(XML_SETTINGS_FREE_SPACE).InnerText)
                End If

                If eleSettings(XML_SETTINGS_UPDATE_AGENTS) IsNot Nothing Then
                    m_shouldUpdateAgents = CBool(eleSettings(XML_SETTINGS_UPDATE_AGENTS).InnerText)
                End If

                If m_nullableAgentUpdateInterval.HasValue Then
                    m_agentUpdateInterval = m_nullableAgentUpdateInterval.Value
                Else
                    If eleSettings(XML_SETTINGS_AGENTS_UPDATE_INTERVAL) IsNot Nothing Then
                        m_agentUpdateInterval = CInt(eleSettings(XML_SETTINGS_AGENTS_UPDATE_INTERVAL).InnerText)
                    End If
                End If

                If eleSettings(XML_SETTINGS_GENERATE_WAIT_TIME) IsNot Nothing Then
                    m_generateWaitTimeSecs = CInt(eleSettings(XML_SETTINGS_GENERATE_WAIT_TIME).InnerText)
                End If

                If eleSettings(XML_SETTINGS_CONVERSION_WAIT_TIME) IsNot Nothing Then
                    m_conversionWaitTimeSecs = CInt(eleSettings(XML_SETTINGS_CONVERSION_WAIT_TIME).InnerText)
                End If

                If eleSettings(XML_SETTINGS_DO_RESTART) IsNot Nothing Then
                    m_doRestart = CBool(eleSettings(XML_SETTINGS_DO_RESTART).InnerText)
                End If

                If eleSettings(XML_SETTINGS_RESTART_INTERVAL) IsNot Nothing Then
                    m_restartInterval = New TimeSpan(CInt(eleSettings(XML_SETTINGS_RESTART_INTERVAL).InnerText), 0, 0)
                End If

                If eleSettings(XML_SETTINGS_CALL_LOG_TABLE) IsNot Nothing Then
                    m_logTableName = eleSettings(XML_SETTINGS_CALL_LOG_TABLE).InnerText
                End If
                If eleSettings(XML_SETTINGS_CALL_LOG_RETENTION) IsNot Nothing Then
                    m_keepLogDays = CInt(eleSettings(XML_SETTINGS_CALL_LOG_RETENTION).InnerText)
                End If

                If eleSettings(XML_SETTINGS_G726_TO_WAV_CMD) IsNot Nothing Then
                    convertAudio.convertCmd(convertAudio.ConvertType.G726_TYPE) = eleSettings(XML_SETTINGS_G726_TO_WAV_CMD).InnerText
                End If
                If eleSettings(XML_SETTINGS_G726_TO_WAV_ARGS) IsNot Nothing Then
                    convertAudio.convertArgs(convertAudio.ConvertType.G726_TYPE) = eleSettings(XML_SETTINGS_G726_TO_WAV_ARGS).InnerText
                End If
                If eleSettings(XML_SETTINGS_G729_TO_WAV_CMD) IsNot Nothing Then
                    convertAudio.convertCmd(convertAudio.ConvertType.G729_TYPE) = eleSettings(XML_SETTINGS_G729_TO_WAV_CMD).InnerText
                End If
                If eleSettings(XML_SETTINGS_G729_TO_WAV_ARGS) IsNot Nothing Then
                    convertAudio.convertArgs(convertAudio.ConvertType.G729_TYPE) = eleSettings(XML_SETTINGS_G729_TO_WAV_ARGS).InnerText
                End If
                If eleSettings(XML_SETTINGS_GSM610_CMD) IsNot Nothing Then
                    convertAudio.convertCmd(convertAudio.ConvertType.GSM610_TYPE) = eleSettings(XML_SETTINGS_GSM610_CMD).InnerText
                End If
                If eleSettings(XML_SETTINGS) IsNot Nothing Then
                    convertAudio.convertArgs(convertAudio.ConvertType.GSM610_TYPE) = eleSettings(XML_SETTINGS_GSM610_ARGS).InnerText
                End If

                If eleSettings(XML_SETTINGS_SKIP_OUTPUT_SPACE_CHECK) IsNot Nothing Then
                    m_skipOutputFolderDiskSpaceCheck = CBool(eleSettings(XML_SETTINGS_SKIP_OUTPUT_SPACE_CHECK).InnerText)
                End If

                loadMetaItems(m_xmlDoc)

            Catch ex As Exception
                m_xmlDoc = Nothing
                TraceTopic.importerTopic.warning("UConnector: invalid config file {}, Exception: {}", m_configFile, ex)
            End Try
        End Using
    End Sub

    Protected Sub loadMetaItems(ByVal _xmlDoc As Xml.XmlDocument)
        m_metaItemsToUse = New Generic.Dictionary(Of String, String)
        For Each metaItemNode As Xml.XmlNode In _xmlDoc.SelectNodes(XML_SETTINGS_META_ITEM)
            Dim originalName As String = metaItemNode.Attributes(XML_SETTINGS_META_ITEM_ORIGINAL).Value.Trim()
            Dim newName As String = originalName

            ' use the newName value if exists
            If metaItemNode.Attributes(XML_SETTINGS_META_ITEM_NEW) IsNot Nothing AndAlso Not String.IsNullOrEmpty(metaItemNode.Attributes(XML_SETTINGS_META_ITEM_NEW).Value) Then
                newName = metaItemNode.Attributes(XML_SETTINGS_META_ITEM_NEW).Value.Trim()
            End If

            m_metaItemsToUse.Add(originalName, newName)
        Next
    End Sub

    Protected Sub initOutputFolder(ByVal xmlDoc As Xml.XmlDocument)
        Using (TraceTopic.importerTopic.scope())
            Dim arr As New List(Of String)
            Dim iSiteID As Integer = -1
            If xmlDoc(XML_SETTINGS)(XML_SETTINGS_SITE_ID) IsNot Nothing Then
                iSiteID = CInt(xmlDoc(XML_SETTINGS)(XML_SETTINGS_SITE_ID).InnerText)
            End If

            'loading the output folders from the local DB if the connection exists

            If Me.ConnectToLocalDB() = True Then
                Dim selectStatement As String = ""
                Dim foldersTbl As DataTable
                If m_speechminerVersion >= 8.1 Then
                    selectStatement = "SELECT path FROM inputFolderTbl"
                    If (iSiteID <> -1) Then
                        selectStatement = selectStatement & " WHERE siteId = " & iSiteID
                    End If
                ElseIf m_speechminerVersion >= 5.2 Then
                    If (iSiteID <> -1) Then
                        selectStatement = "SELECT value FROM " &
                                          " (SELECT t1.taskid,t2.m_hostComputerId,value FROM  TaskAttrValTbl As t1 " &
                                          " Join TaskList As t2  On t1.taskid = t2.index1 where  t1.attr='inputFolder') As t3 " &
                                          " Join ComputerList As t4 On t4.siteId = " & iSiteID & " And t4.computerId = t3.m_hostComputerId"
                    Else
                        selectStatement = "SELECT value FROM TaskAttrValTbl where attr='inputFolder'"
                    End If

                Else
                    selectStatement = "SELECT INPUT_FOLDER_PATH FROM cmrsParams"
                End If
                foldersTbl = m_localdbMaster.getDataTable(selectStatement)
                For Each row As DataRow In foldersTbl.Rows
                    arr.Add(row(0).ToString())
                Next

                If m_speechminerVersion >= 8.0 Then
                    If xmlDoc(XML_SETTINGS)(XML_SETTINGS_ENCRYPT_AUDIO) IsNot Nothing AndAlso CBool(xmlDoc(XML_SETTINGS)(XML_SETTINGS_ENCRYPT_AUDIO).InnerText) Then
                        utopy.bDBOffline.AudioKeys.init(m_localdbMaster)
                        selectStatement = "SELECT TOP(1) * FROM AudioKeys ORDER BY keyId DESC"
                        Dim keysTbl As DataTable = m_localdbMaster.getDataTable(selectStatement)
                        If keysTbl.Rows.Count > 0 Then
                            m_AudioKeyId = CInt(keysTbl.Rows(0)("keyId"))
                        End If
                    End If
                End If

            Else
                'loading the output folders from the input xml
                If xmlDoc(XML_SETTINGS)(XML_SETTINGS_OUTPUT_FOLDERS) IsNot Nothing AndAlso
                        CBool(xmlDoc(XML_SETTINGS)(XML_SETTINGS_OUTPUT_FOLDERS)(XML_SETTINGS_OUTPUT_FOLDERS_IS_DIFFERENT_FOLDER).InnerText) Then
                    m_outputFolders = New Dictionary(Of String, String)
                    For Each outputFolder As Xml.XmlNode In xmlDoc(XML_SETTINGS)(XML_SETTINGS_OUTPUT_FOLDERS)
                        TraceTopic.importerTopic.note("config files's outputFolders list is empty")
                        m_outputFolders.Add(outputFolder.Name, outputFolder.InnerText)
                    Next
                End If
                If xmlDoc(XML_SETTINGS)(XML_SETTINGS_OUTPUT_FOLDERS) IsNot Nothing Then
                    Dim index As Integer = 0

                    If xmlDoc(XML_SETTINGS)(XML_SETTINGS_OUTPUT_FOLDERS).ChildNodes.Count = 0 Then
                        TraceTopic.importerTopic.note("config files's outputFolders list is empty")
                    End If

                    For Each outputFolder As Xml.XmlNode In xmlDoc(XML_SETTINGS)(XML_SETTINGS_OUTPUT_FOLDERS).SelectNodes(XML_SETTINGS_OUTPUT_FOLDER)
                        arr.Add(outputFolder.InnerText.Trim)
                        index += 1
                    Next
                End If
            End If

            m_outputFolder = arr.ToArray()
        End Using
    End Sub

    Private Function validateProgram() As Boolean
        If m_IsLocalDBAvailable Then
            Dim isActive As Integer
            Dim idParam As New SqlParameter("@id", m_program)
            isActive = m_localdbMaster.getOneValue(Of Integer)("SELECT isActive FROM programInfoTbl where externalId=@id", , New SqlParameter() {idParam})
            Return isActive = 1
        End If
        Return True
    End Function

    Protected Function GetOutputFolder() As String
        If m_outputFolder.Count = 0 Then Return Nothing
        Using (TraceTopic.importerTopic.scope())
            Dim outputFolder As String = ""
            If lastOutputFolderUsed + 1 >= m_outputFolder.Count Then
                lastOutputFolderUsed = 0
            Else
                lastOutputFolderUsed += 1
            End If

            Dim outPath As String = m_outputFolder(lastOutputFolderUsed)

            If Not m_skipOutputFolderDiskSpaceCheck Then
                While (GetAvailableSpaceGB(outPath) < m_freeSpaceSize)
                    If lastOutputFolderUsed + 1 >= m_outputFolder.Count Then
                        TraceTopic.importerTopic.warning("storage is low on local drive {}. Will wait and try again. ", outPath)
                        Thread.Sleep(10 * 60 * 1000) ' sleep 10 minutes
                        lastOutputFolderUsed = 0 ' start again from the first folder
                    Else
                        lastOutputFolderUsed += 1 ' try the next folder
                    End If
                    outPath = m_outputFolder(lastOutputFolderUsed)
                End While
            End If

            outputFolder = m_outputFolder(lastOutputFolderUsed)

            Return outputFolder
        End Using
    End Function

    Protected Function GetOutputFolder(ByVal key As String) As String
        Using (TraceTopic.importerTopic.scope())
            Dim outPath As String = m_outputFolders.Item(key)
            Return outPath
        End Using
    End Function

    Protected Sub LoadBaseXML(
        xmlDoc As XmlDocument
        )
        Using (TraceTopic.importerTopic.scope())
            Try
                Dim eleSettings = xmlDoc(XML_SETTINGS)

                Dim eleValue = eleSettings(XML_SETTINGS_START_TIME)
                If eleValue IsNot Nothing AndAlso String.IsNullOrEmpty(eleValue.InnerText) = False Then
                    m_startTime = TimeSpan.FromHours(CDbl(eleValue.InnerText))
                End If

                eleValue = eleSettings(XML_SETTINGS_END_TIME)
                If eleValue IsNot Nothing AndAlso String.IsNullOrEmpty(eleValue.InnerText) = False Then
                    m_endTime = TimeSpan.FromHours(CDbl(eleValue.InnerText))
                End If

                eleValue = eleSettings(XML_SETTINGS_POLL_INTERVAL)
                If eleValue IsNot Nothing AndAlso String.IsNullOrEmpty(eleValue.InnerText) = False Then
                    m_pollIntervalSecs = CInt(eleValue.InnerText)
                End If

                If eleSettings(XML_SETTINGS_SPEECHMINER_VERSION) Is Nothing Then
                    m_speechminerVersion = 6.0
                Else
                    m_speechminerVersion = CSng(eleSettings(XML_SETTINGS_SPEECHMINER_VERSION).InnerText)
                End If

                m_system = eleSettings(XML_SETTINGS_SYSTEM).InnerText
                initOutputFolder(xmlDoc)
            Catch ex As Exception
                TraceTopic.importerTopic.warning("Error in importer.loadBaseXML. {}", ex)
            End Try
        End Using
    End Sub

    Protected Function loadConnectionString(ByVal _nameOfConnection As String) As String
        'Load the connection string from connectorApp.exe.config

        Dim config As System.Configuration.Configuration
        config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
        Dim settingsCollection As ConnectionStringSettingsCollection = config.ConnectionStrings.ConnectionStrings

        Dim strConnection As String = String.Empty

        For Each settings As ConnectionStringSettings In settingsCollection
            If settings.Name = _nameOfConnection Then
                strConnection = settings.ConnectionString
                Exit For
            End If
        Next

        Return strConnection
    End Function

    Protected Overrides Sub DoWork()
        Using (TraceTopic.importerTopic.scope())
            While Not Me.CancelRequested
                Try
                    Dim foundCalls As Boolean = False
                    Dim currentTime As Date = Date.Now
                    Dim currentTimeOfDay As New TimeSpan(currentTime.Hour, currentTime.Minute, 0)
                    Dim running As Boolean = True
                    If m_IsLocalDBAvailable AndAlso m_speechminerVersion >= 8.1 Then
                        Dim nameParam As New SqlParameter("@name", ConfigFileName)
                        Dim computerParam As New SqlParameter("@computer", ComputerName)
                        running = m_localdbMaster.getOneValue(Of Boolean)("select command from monitorConnectorsTbl where name=@name and computer=@computer", ,
                                                                          New SqlParameter() {nameParam, computerParam})
                    End If
                    If running AndAlso inTimeWindow(currentTimeOfDay, m_startTime, m_endTime) Then
                        OnStatusChanged("Fetching")
                        foundCalls = ImportNextBatch()
                    End If
                    If Not Me.CancelRequested AndAlso foundCalls = False Then
                        If running Then
                            OnStatusChanged("Idle")
                        Else
                            OnStatusChanged("Stopped")
                        End If
                        Thread.Sleep(Me.m_pollIntervalSecs * 1000)
                    End If
                Catch exInvalidOperation As InvalidOperationException
                    doError(exInvalidOperation.Message, exInvalidOperation.ToString)

                    If exInvalidOperation.Source = "System.Data" Then
                        TraceTopic.importerTopic.note("Attempting to re-establish database connection...")
                        Try
                            init()
                        Catch
                            'do nothing 
                        End Try
                    End If

                Catch ex As Exception
                    doError(ex.Message, ex.ToString)
                    PSOTrace.WriteRegisteredMessage(8011, "UConnector error")
                End Try
            End While
        End Using
        OnStatusChanged("Stopped")
        Quit()

    End Sub

    Protected Sub doError(ByVal _msg As String, Optional ByVal _msg2 As String = "", Optional ByVal _status As String = "warning")
        Using (TraceTopic.importerTopic.scope())
            Select Case _status.ToLower()
                Case "warning"
                    TraceTopic.importerTopic.warning("UConnector error - {} - {}", _msg, _msg2.ToString)
                Case "fatal"
                    TraceTopic.importerTopic.error("UConnector error - {} - {}", _msg, _msg2.ToString)
            End Select
        End Using
        OnStatusChanged("ERROR!")
        Thread.Sleep(Me.m_pollIntervalSecs * 1000)
    End Sub
    Protected Shared m_xmlSaveLock As New Object

    Public Shared Sub restartService()
        Thread.Sleep(5000) ' give the system some time to write all messages to the logger
        SyncLock m_xmlSaveLock
            Environment.Exit(1)
        End SyncLock
    End Sub

    Protected Overridable Sub saveToXML()
        Using (TraceTopic.importerTopic.scope())
            Try
                Dim tempName As String = m_configFile & ".new"
                SyncLock m_xmlSaveLock
                    m_xmlDoc.Save(tempName)
                    If New IO.FileInfo(tempName).Length > 0 Then
                        IO.File.Delete(m_configFile)
                        IO.File.Move(tempName, m_configFile)
                    End If
                End SyncLock
            Catch ex As Exception
                TraceTopic.importerTopic.warning("failed saving config file {}, Exception: {}", m_configFile, ex)
            End Try
        End Using
        FlushCallRecords()
    End Sub

    Protected Sub generateMetaFile(ByVal metaFile As Xml.XmlTextWriter, ByVal _callRow As DataRow, ByVal _agentName As String, Optional ByVal _isList As Boolean = False)
        Using (TraceTopic.importerTopic.scope())
            Try
                metaFile.Formatting = Xml.Formatting.Indented
                metaFile.WriteStartDocument(False)
                metaFile.WriteComment(XML_OUTPUT_COMMENT)
                metaFile.WriteStartElement(XML_OUTPUT_CALLINFORMATION)

                metaFile.WriteElementString(XML_OUTPUT_PROGRAMID, m_program)
                metaFile.WriteStartElement(XML_OUTPUT_SPEAKERS)
                generateSpeakers(metaFile, _callRow, _agentName, _isList)
                metaFile.WriteEndElement() 'speakers

                If m_AudioKeyId > 0 Then
                    metaFile.WriteElementString(XML_OUTPUT_ENCRYPTION_KEY, CStr(m_AudioKeyId))
                End If

                If Not String.IsNullOrEmpty(m_leftChannelSpeaker) AndAlso Not String.IsNullOrEmpty(m_rightChannelSpeaker) Then
                    metaFile.WriteStartElement(XML_OUTPUT_CHANNELS)
                    metaFile.WriteAttributeString(XML_OUTPUT_CHANNEL_LEFT, m_leftChannelSpeaker)
                    metaFile.WriteAttributeString(XML_OUTPUT_CHANNEL_RIGHT, m_rightChannelSpeaker)
                    metaFile.WriteEndElement() ' channels
                End If

            Catch ex As Exception
                TraceTopic.importerTopic.warning("Error writing general information to the meta file : {}", ex.Message)
                If Not metaFile Is Nothing Then
                    metaFile.Close()
                End If
            End Try
        End Using
    End Sub

    Protected Sub generateExtraMetaData(ByVal metaFile As Xml.XmlTextWriter, ByVal _row As DataRow, Optional ByVal _removeWhiteSpaces As Boolean = False)
        For Each metaItemOriginalName As String In m_metaItemsToUse.Keys
            Dim valueObj As Object = _row.Item(metaItemOriginalName)
            If Not IsDBNull(valueObj) Then
                Dim valueStr As String = CStr(valueObj).Trim()
                If Not String.IsNullOrEmpty(valueStr) Then
                    valueStr = formatMetaValue(metaItemOriginalName, valueStr)
                    Dim newMetaName As String = m_metaItemsToUse(metaItemOriginalName) ' take the 'new name' of the field
                    If _removeWhiteSpaces Then
                        metaFile.WriteElementString(newMetaName.Replace(" ", ""), valueStr)
                    Else
                        metaFile.WriteElementString(newMetaName, valueStr)
                    End If
                End If
            End If
        Next
    End Sub

    Protected Overridable Function formatMetaValue(ByVal _name As String, ByVal _value As String) As String
        Return _value
    End Function

    Protected Overridable Sub generateOptionals(
        metaFile As XmlTextWriter,
        callRow As DataRow
        )
        Dim optionalFields = getOptionals(callRow).Keys
        For Each fieldName As String In optionalFields
            If callRow.Table.Columns.Contains(fieldName) = False Then Continue For
            If callRow.IsNull(fieldName) = True Then Continue For

            Dim fieldValue = CStr(callRow(fieldName))
            If String.IsNullOrWhiteSpace(fieldName) = True Then Continue For

            metaFile.WriteElementString(fieldName, fieldValue)
        Next
    End Sub

    Protected Overridable Sub generateSpeakers(ByVal _metaFile As Xml.XmlTextWriter, ByVal _callRow As DataRow, Optional ByVal _agentName As String = Nothing, Optional ByVal _isList As Boolean = False)
        If _isList Then
            Dim agentArr As String() = getAgentNames(CStr(_callRow(_agentName)))
            Dim supArr As String() = getSupNames(CStr(_callRow(_agentName)))
            For i As Integer = 0 To agentArr.Length - 1
                _metaFile.WriteStartElement(XML_OUTPUT_SPEAKER)
                _metaFile.WriteAttributeString(XML_OUTPUT_SPEAKER_ID, agentArr(i))
                _metaFile.WriteAttributeString(XML_OUTPUT_SPEAKER_TYPE, XML_OUTPUT_SPEAKER_TYPE_AGENT)
                _metaFile.WriteElementString(XML_OUTPUT_SPEAKER_WORKGROUP, "/" & supArr(i))
                _metaFile.WriteEndElement() 'speaker
            Next

        Else
            _metaFile.WriteStartElement(XML_OUTPUT_SPEAKER)
            If _agentName Is Nothing Then
                _metaFile.WriteAttributeString(XML_OUTPUT_SPEAKER_ID, _callRow(RESULTS_COLUMN_AGENT_ID).ToString.Replace(",", " ").Trim)
            Else
                _metaFile.WriteAttributeString(XML_OUTPUT_SPEAKER_ID, _agentName.Replace(",", " ").Trim)
            End If
            _metaFile.WriteAttributeString(XML_OUTPUT_SPEAKER_TYPE, XML_OUTPUT_SPEAKER_TYPE_AGENT)
            Dim wg As String
            Try
                wg = _callRow(RESULTS_COLUMN_WORKGROUP).ToString().Trim()
            Catch ex As Exception
                wg = m_workgroup
            End Try
            If Not wg.StartsWith("/") Then
                wg = "/" & wg
            End If
            _metaFile.WriteElementString(XML_OUTPUT_SPEAKER_WORKGROUP, wg)
            _metaFile.WriteEndElement() 'speaker
        End If
    End Sub

    Protected Const AGENT_REGEX_PATTERN As String = "(?<agentID1>\d*)[^)]*[(*](?<firstName1>[^),]*)\s*,\s*(?<lastName1>[^)]*)[)]\s*(,\s*(?<agentID2>\d*)[^)]*[(*](?<firstName2>[^),]*)\s*,\s*(?<lastName2>[^)]*)[)])?"

    Protected Function getSupNames(ByVal _nameField As String) As String()
        Dim ret As New List(Of String)
        Using (TraceTopic.importerTopic.scope())
            Dim matches As Text.RegularExpressions.MatchCollection = Text.RegularExpressions.Regex.Matches(_nameField, AGENT_REGEX_PATTERN, Text.RegularExpressions.RegexOptions.None)
            Dim match As Text.RegularExpressions.Match = Nothing
            For i As Integer = 0 To matches.Count - 1
                match = matches(i)
                If match IsNot Nothing Then
                    Dim name As String
                    Dim groupName As String = "agentID" & (i + 1).ToString
                    If IsNumeric(match.Groups(groupName).Value) Then
                        name = getSupervisorFromId(CInt(match.Groups(groupName).Value))
                    Else
                        name = "N/A"
                    End If
                    ret.Add(name)
                Else
                    TraceTopic.importerTopic.warning("Supervisor name couldn't be parsed: {}", _nameField)
                End If

            Next i
        End Using
        Return ret.ToArray
    End Function

    Protected Function getAgentNames(ByVal _nameField As String) As String()
        Dim ret As New List(Of String)
        Using (TraceTopic.importerTopic.scope())
            Dim matches As Text.RegularExpressions.MatchCollection = Text.RegularExpressions.Regex.Matches(_nameField, AGENT_REGEX_PATTERN, Text.RegularExpressions.RegexOptions.None)
            Dim match As Text.RegularExpressions.Match = Nothing
            For i As Integer = 0 To matches.Count - 1
                match = matches(i)
                If match IsNot Nothing Then
                    Dim name As String
                    Dim groupName As String = "agentID" & (i + 1).ToString
                    If IsNumeric(match.Groups(groupName).Value) Then
                        name = getAgentFromId(CInt(match.Groups(groupName).Value))
                    Else
                        name = "N/A"
                    End If
                    ret.Add(name)
                Else
                    TraceTopic.importerTopic.warning("Agent name couldn't be parsed: {}", _nameField)
                End If

            Next i
        End Using
        Return ret.ToArray
    End Function

    Public Shared Function getAgentFromId(ByVal _acdId As Integer) As String
        Dim ret As String = Nothing

        If m_agentNameHash.ContainsKey(_acdId) Then
            ret = m_agentNameHash.Item(_acdId)
        Else
            ret = "N/A"
        End If

        Return ret
    End Function

    Public Shared Function getSupervisorFromId(ByVal _acdId As Integer) As String
        Dim ret As String = Nothing

        If m_agentNameHash.ContainsKey(_acdId) Then
            ret = m_agentSupervisorHash.Item(_acdId)
        Else
            ret = "N/A"
        End If

        Return ret
    End Function

    Private Sub convertMainLoop()
        initConvertThreadPool()
        Using (TraceTopic.importerTopic.scope())
            While Not Me.CancelRequested
                Try
                    Dim files() As String = IO.Directory.GetFiles(m_preConvertPath, "*.wav")
                    If DateDiff(DateInterval.Minute, m_lastConvAudioTempCount, Now) > 15 AndAlso files.Length > 0 Then
                        TraceTopic.importerTopic.warning("{} calls in convertAudio folder for {}", files.Length, m_program)
                        m_lastConvAudioTempCount = Now
                    End If
                    If files.Length > 0 Then
                        Array.Sort(files, New fileCreationComparer)
                        For Each f As String In files
                            Dim convObj As convertAudio = Nothing
                            Dim tmpPath As String = ""
                            Try
                                Dim xmlFile As String = f.Substring(0, f.LastIndexOf(".")) + ".xml"

                                If IO.File.Exists(xmlFile) Then
                                    convObj = GetFreeConvertThreadObj()
                                    tmpPath = m_convertedAudioTempFolder & "\" & IO.Path.GetFileName(f)
                                    Dim tmpXmlFile As String = tmpPath.Substring(0, tmpPath.LastIndexOf(".")) + ".xml"
                                    If IO.File.Exists(tmpPath) Then
                                        IO.File.Delete(tmpPath)
                                    End If
                                    IO.File.Move(f, tmpPath)
                                    If IO.File.Exists(tmpXmlFile) Then
                                        IO.File.Delete(tmpXmlFile)
                                    End If
                                    If moveOrCopy(xmlFile, tmpXmlFile) = True Then
                                        convObj.m_source = tmpPath
                                        convObj.m_target = GetOutputFolder() & "\" & IO.Path.GetFileName(tmpPath)
                                        convObj.m_type = m_AudioConvertType
                                        convObj.toolsPath = m_toolsPath

                                        convObj.go()
                                    End If
                                End If
                            Catch ex As Exception
                                TraceTopic.importerTopic.warning("Exception converting from {}, Main internal loop. {}, Exception: {}", m_AudioConvertType.ToString, vbCrLf, ex)
                                If convObj IsNot Nothing Then
                                    convObj.abort()
                                    convObj.setNotInUse()
                                End If
                            End Try
                        Next
                    Else
                        Thread.Sleep(60000)
                    End If
                Catch ex As Exception
                    TraceTopic.importerTopic.warning("Exception converting from {}, Main loop. {}, Exception: {}", m_AudioConvertType.ToString, vbCrLf, ex)
                End Try
            End While
        End Using
        quitConvertThread()
    End Sub

    Private Sub MonitorTimer_Elapsed() Handles m_monitorTimer.Elapsed
        Try
            Dim handledCalls As Integer = getAmountOfHandledCalls()
            Dim freeSpace As Double = getfreeSpace()

            utopy.bGeneralCounter.Counter.setTaskValue("Managed calls", "SpeechMiner UConnector", 1, handledCalls)
            utopy.bGeneralCounter.Counter.setTaskValue("Last call ID", "SpeechMiner UConnector", 1, DateToInt(m_minRecordStart))
            utopy.bGeneralCounter.Counter.setTaskValue("Target folder free space", "SpeechMiner UConnector", 1, CInt(freeSpace))

            If m_IsLocalDBAvailable AndAlso m_speechminerVersion >= 6.0 Then
                Dim computerParam As New SqlParameter("@computer", ComputerName)

                If m_speechminerVersion < 8.1 Then
                    Dim systemParam As New SqlParameter("@system", m_system + " - " + ConfigFileName)

                    m_localdbMaster.runSQL("delete from monitorConnectorsTbl where computer=@computer and system=@system " &
                                           "insert into monitorConnectorsTbl values(@system, @computer, " & handledCalls & ", " & freeSpace * 1024 & "," & m_localdbMaster.getDBTime() & ")",
                                           New SqlParameter() {systemParam, computerParam})
                Else
                    Dim systemParam As New SqlParameter("@system", m_system)
                    Dim nameParam As New SqlParameter("@name", ConfigFileName)

                    m_localdbMaster.runSQL("UPDATE monitorConnectorsTbl SET handledCalls=" & handledCalls & ", freeSpace=" & freeSpace * 1024 & ", lifesign=" & m_localdbMaster.getDBTime() & " WHERE name=@name AND computer=@computer",
                                           New SqlParameter() {nameParam, computerParam})
                End If
            End If

        Finally
            m_monitorTimer.Start()
        End Try
    End Sub
    Private Function getAmountOfHandledCalls() As Integer
        Return m_nHandledCalls
    End Function
    Private Function getfreeSpace() As Double
        Dim freeSpace As Double = 0
        For Each queue As String In m_outputFolder
            freeSpace += GetAvailableSpaceGB(queue)
        Next
        Return freeSpace
    End Function

    Private Structure spaceTimeStruct
        Dim space As Double
        Dim timestamp As DateTime
    End Structure
    Private Shared m_diskSpaceHash As New Generic.Dictionary(Of String, spaceTimeStruct)

    Public Overridable Function GetAvailableSpaceGB(ByVal _path As String) As Double
        Dim st As spaceTimeStruct

        If Not m_diskSpaceHash.TryGetValue(_path, st) OrElse DateDiff(DateInterval.Minute, st.timestamp, Now) > 2 Then
            Dim folder As New utopy.bIOFolder.Folder(_path)
            st.space = folder.GetAvailableSpace() \ 1073741824
            st.timestamp = Now
            m_diskSpaceHash(_path) = st
        End If

        Return st.space
    End Function

    Private Sub AgentUpdateMainFunc()
        Using (TraceTopic.importerTopic.scope())
            While Not Me.CancelRequested
                Try
                    UpdateAgents()
                Catch ex As Exception
                    TraceTopic.importerTopic.warning("Failed to update agents: {}", ex)
                End Try
                Thread.Sleep(m_agentUpdateInterval * 60 * 1000) ' Interval is in minutes
            End While
            quitAgentUpdateThread()
        End Using
    End Sub

    Protected Function moveOrCopy(ByVal _source As String, ByVal _target As String) As Boolean
        Try
            IO.File.Move(_source, _target)
        Catch ex As Exception
            IO.File.Copy(_source, _target)
        End Try

        Return IO.File.Exists(_target)
    End Function

    Protected Sub CleanupFiles(ByVal _fromDir As String, ByVal _pattern As String, ByVal _backupDir As String, ByVal _age As Integer, Optional ByVal _searchOption As IO.SearchOption = IO.SearchOption.TopDirectoryOnly)
        Dim oldDate As DateTime = DateTime.Now.AddDays(0 - _age)
        Dim di As New IO.DirectoryInfo(_fromDir)
        For Each f As IO.FileInfo In di.GetFiles(_pattern, _searchOption)
            If f.CreationTime.CompareTo(oldDate) < 0 Then
                Try
                    If _backupDir Is Nothing Then
                        IO.File.Delete(f.FullName)
                    Else
                        IO.File.Move(f.FullName, IO.Path.Combine(_backupDir, f.Name))
                    End If
                Catch
                End Try
            End If
        Next
    End Sub

    Protected Sub runCommandLine(ByVal _executablePath As String, ByVal _args As String)
        Using (TraceTopic.importerTopic.scope())
            TraceTopic.importerTopic.note("runCommandLine: {}, args: {}", _executablePath, _args)

            Dim p As New Process()
            p.StartInfo.FileName = _executablePath
            p.StartInfo.Arguments = _args
            p.StartInfo.UseShellExecute = True
            p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
            p.StartInfo.CreateNoWindow = True
            p.Start()

            p.WaitForExit()
        End Using
    End Sub

    Protected Function runCommandLineWithOutput(ByVal _executablePath As String, ByVal _args As String, ByVal _dir As String) As String
        Using (TraceTopic.importerTopic.scope())
            TraceTopic.importerTopic.note("runCommandLine: {}, args: {}", _executablePath, _args)
        End Using
        Dim ret As String
        Dim p As New Process
        p.StartInfo.UseShellExecute = False
        p.StartInfo.RedirectStandardOutput = True
        p.StartInfo.RedirectStandardError = True
        p.StartInfo.FileName = _executablePath
        p.StartInfo.WorkingDirectory = _dir
        p.StartInfo.Arguments = _args
        p.StartInfo.CreateNoWindow = True
        p.Start()
        ret = p.StandardOutput.ReadToEnd
        Dim errorString As String = p.StandardError.ReadToEnd
        ret += vbCrLf & If(String.IsNullOrEmpty(errorString), "", "Error: " & vbCrLf & errorString)

        p.WaitForExit()

        Return ret
    End Function

    Protected Class fileCreationComparer
        Implements Collections.IComparer
        Implements Generic.IComparer(Of String)

        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
            Dim fileX As String = CStr(x)
            Dim fileY As String = CStr(y)

            Dim dateX As DateTime = IO.File.GetCreationTime(fileX)
            Dim datey As DateTime = IO.File.GetCreationTime(fileY)

            If dateX > datey Then
                Return 1
            ElseIf datey > dateX Then
                Return -1
            Else
                Return 0
            End If

        End Function

        Public Function CompareGeneric(ByVal x As String, ByVal y As String) As Integer Implements System.Collections.Generic.IComparer(Of String).Compare
            Return Compare(x, y)
        End Function

        Public Sub New()

        End Sub

    End Class
    Protected Function String2Date(ByVal _callTime As String) As DateTime
        'Converts call time from yyyyMMddHHmmss to a datetime
        Dim dtmCallTime As New DateTime(CInt(_callTime.Substring(0, "yyyy".Length)), CInt(_callTime.Substring(4, "MM".Length)),
            CInt(_callTime.Substring(6, "dd".Length)), CInt(_callTime.Substring(8, "HH".Length)), CInt(_callTime.Substring(10, "mm".Length)),
            CInt(_callTime.Substring(12, "ss".Length)))
        Return dtmCallTime
    End Function
    Protected Shared Function DateToInt(ByVal _dtm As DateTime) As Integer
        Return CInt(_dtm.ToString("yyyyMMdd"))
    End Function
    Protected Shared Function ToISOString(ByVal _dtm As DateTime) As String
        Return _dtm.ToString("yyyy-MM-ddTHH:mm:ss")
    End Function

    Protected Function CallTime2UTC(ByVal _callTime As DateTime) As DateTime
        Dim utc As DateTime = _callTime.AddHours(-1 * m_StandardTimeUtcOffset)
        Dim daylightTimes As Globalization.DaylightTime = System.TimeZone.CurrentTimeZone.GetDaylightChanges(Year(_callTime))

        If (m_serverDaylightSavings AndAlso System.TimeZone.IsDaylightSavingTime(_callTime, daylightTimes)) Then 'assume UConnector's server and call centers change clocks on same day
            utc = DateAdd(DateInterval.Hour, -1 * daylightTimes.Delta.Hours, utc)
        End If

        Return utc
    End Function

    Protected Sub unzip(ByVal _zipFile As String, ByVal _outputDir As String)
        Using (TraceTopic.importerTopic.scope())
            Dim cmd As String = IO.Path.Combine(m_toolsPath, "7z.exe")
            Dim args As String = "x -y -o""" & _outputDir & """ """ & _zipFile & """"
            TraceTopic.importerTopic.note("Running: '{}' '{}'", cmd, args)
            Shell(cmd & " " & args, AppWinStyle.Hide, True)
        End Using
    End Sub

    Private Sub addPartitions(ByVal _agents As String(), ByVal _dbMaster As utopy.bDBServices.DBServices)
        Dim type As Integer
        For Each agent As String In _agents
            Dim prefix As String = ""
            For Each folder As String In agent.Split("/"c)
                If folder <> "" Then
                    prefix &= "/" & folder
                    If prefix = agent Then
                        type = 2 ' Agent
                    Else
                        type = 1 ' Workgroup
                    End If
                    Dim sql As String = "exec sp_createOrGetPartitionId @partition," & type
                    Dim partitionParam As New SqlParameter("@partition", SqlDbType.VarChar)
                    partitionParam.Value = prefix
                    Dim paramArr() As SqlParameter = {partitionParam}
                    _dbMaster.runSQL(sql, paramArr)
                End If
            Next
        Next
    End Sub

    ' Override to log more data about the call
    Protected Friend Overridable Sub logCall(ByVal _status As String, ByVal _row As DataRow)
        logCall(_status, CStr(_row(RESULTS_COLUMN_CALL_ID)))
    End Sub

    Protected Sub logCall(ByVal _status As String, ByVal ParamArray _fields As String())
        If m_logCalls Then
            Dim fieldsLength As Integer = 0
            If _fields IsNot Nothing Then
                fieldsLength = _fields.Length
            End If
            If fieldsLength <> nLogFields() Then
                Throw New Exception("Call log record size mismatch")
            End If
            SyncLock m_logRecords
                Dim row As DataRow = m_logRecords.NewRow()
                row(0) = DateTime.UtcNow()
                row(1) = _status
                For i As Integer = 0 To fieldsLength - 1
                    row(i + 2) = _fields(i)
                Next
                m_logRecords.Rows.Add(row)
            End SyncLock
        End If
    End Sub

    Protected Sub FlushCallRecords()
        If m_logCalls Then
            SyncLock m_logRecords
                m_logDBMaster.BulkInsert(m_logRecords, m_logTableName)
                m_logRecords.Clear()
            End SyncLock
            If m_keepLogDays > -1 Then
                m_logDBMaster.runSQL("DELETE FROM " & m_logTableName & " WHERE datediff(day, ProcessTime, GETUTCDATE())>" & m_keepLogDays)
            End If
        End If
    End Sub

    Public Shared Function GetDBField(ByRef _dr As DataRow, ByVal _fieldName As String) As Object
        If Not _dr.Table.Columns.Contains(_fieldName) Then
            Return Nothing
        End If

        If _dr.Item(_fieldName) Is System.DBNull.Value Then
            Return Nothing
        End If

        Return _dr.Item(_fieldName)
    End Function

    Protected Shared Function ParseCSVLine(ByVal _line As String) As String()
        Dim csvRegEx As Text.RegularExpressions.Regex = New Text.RegularExpressions.Regex(",(?=(?:[^\""]*\""[^\""]*\"")*(?![^\""]*\""))")
        Dim fields As String() = csvRegEx.Split(_line)
        For i As Integer = 0 To fields.Count - 1
            Dim field As String = fields(i)
            If field.Length > 1 AndAlso field.First = """" AndAlso field.Last = """" Then
                fields(i) = field.Substring(1, field.Length - 2)
            End If
        Next
        Return fields
    End Function

    Protected Shared Function GetFilesOlderThen(ByVal _path As String, ByVal _searchPattern As String, ByVal _olderThan As TimeSpan) As Generic.IEnumerable(Of String)
        Dim allTextFiles() As String = System.IO.Directory.GetFiles(_path, _searchPattern, IO.SearchOption.TopDirectoryOnly)

        Dim files As New List(Of String)

        For Each filename As String In allTextFiles
            Dim creationTime As DateTime = IO.File.GetCreationTime(filename)

            If creationTime.Add(_olderThan) < Date.Now Then
                files.Add(filename)
            End If
        Next

        Return files
    End Function

    Protected Shared Function CombinePath(ByVal ParamArray _parts() As String) As String
        Dim path As String = ""

        For Each part As String In _parts
            path = IO.Path.Combine(path, part)
        Next

        Return path
    End Function

    Public Sub Dispose() Implements IDisposable.Dispose
        quitAgentUpdateThread()
        quitConvertThread()
    End Sub
End Class

Public Class GenerateImporterDataRow

    Private m_t As Thread
    Private m_isInUse As Boolean = False
    'Private m_webC As System.Net.WebClient

    ' set from outside before starting
    Public m_dataRow As DataRow
    Public m_program As String = ""
    Public m_doDebug As Boolean = True


    Public imp As Importer
    Private m_hash As New Hashtable

    Public ReadOnly Property isInUse() As Boolean
        Get
            Return m_isInUse
        End Get
    End Property

    Public Sub go()
        m_isInUse = True

        m_t = New Thread(AddressOf doWork2)
        m_t.Name = "generateDataRow"
        m_t.Start()
    End Sub

    Public Sub doWork2()
        Using (TraceTopic.importerTopic.scope())
            Dim callId As String = ""
            Try

                callId = CStr(m_dataRow(Importer.RESULTS_COLUMN_CALL_ID))
                Dim callTime As String = CStr(m_dataRow(Importer.RESULTS_COLUMN_STARTED_AT))
                Dim sourcePath As String
                Dim outPath As String
                If m_dataRow(Importer.RESULTS_COLUMN_SOURCE_PATH) IsNot Nothing AndAlso Not IsDBNull(m_dataRow(Importer.RESULTS_COLUMN_SOURCE_PATH)) Then
                    sourcePath = CStr(m_dataRow(Importer.RESULTS_COLUMN_SOURCE_PATH))
                Else
                    sourcePath = ""
                End If
                If m_dataRow(Importer.RESULTS_COLUMN_OUT_PATH) IsNot Nothing AndAlso Not IsDBNull(m_dataRow(Importer.RESULTS_COLUMN_OUT_PATH)) Then
                    outPath = CStr(m_dataRow(Importer.RESULTS_COLUMN_OUT_PATH))
                Else
                    outPath = ""
                End If

                Dim status As Boolean = imp.GenerateAudioFile(sourcePath, outPath, callId, , callTime, m_dataRow)
                If status = True Then
                    TraceTopic.importerTopic.note("Generated call {}", callId)

                    imp.GenerateMetaFile(outPath, m_dataRow)
                    TraceTopic.importerTopic.note("Generated meta for {}", callId)
                    imp.logCall("OK", m_dataRow)
                Else
                    If m_doDebug Then
                        TraceTopic.importerTopic.warning("Could not generate audio for: {}", callId)
                    End If
                    imp.logCall("Failed", m_dataRow)
                End If

            Catch ex As Exception
                TraceTopic.importerTopic.warning("Failed generating one call. ID:{}, Exception: {} ", callId, ex)
                imp.logCall("Exception", m_dataRow)
            Finally
                m_isInUse = False
            End Try
        End Using
    End Sub

    Public Sub New()

    End Sub
End Class
