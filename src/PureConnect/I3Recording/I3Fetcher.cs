﻿using PureConnect.CICConfig;
using ININ.IceLib.Connection;
using ININ.IceLib.QualityManagement;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using GenUtils.TraceTopic;
using PureConnect.I3Audio;
using System.Net.Http;
using System.Threading.Tasks;
using PureConnect.Data;
using System.Collections.Generic;

namespace PureConnect
{
    /// <summary>
    /// Class to connect to PureConnect CIC Server and download recordings.
    /// </summary>
    internal class I3Fetcher:ConvertEmail
    {
        private string log_text = "I3Download## ";
        //private string m_host;
        //private bool m_opened = false;
        //private string m_password;
        //private int m_port = HostEndpoint.DefaultPort;
        private RecordingsManager m_recordingsManager;
        //private RecordingsManager m_backupRecordingsManager;
        //private Session m_session;
        //private CICCredentials credentials = null;
        //private string m_username;

        /// <summary>
        /// Constructor passing in the credentials used to connect to PureConnect
        /// </summary>
        /// <param name="credentials"></param>
        public I3Fetcher(bool m_debugFakeDownload)
        {
            if (!m_debugFakeDownload)
            {
                AttemptToOpen();
            }
            //this.credentials = credentials;
            //m_username = credentials.User;
            //m_password = credentials.Password;
            //m_host = credentials.Host;
        }

        /// <summary>
        /// Close the connection to PureConnect
        /// </summary>
        /*
        public void Close()
        {
            if (m_session != null)
            {
                try
                {
                    log.Debug(log_text + "Closing I3 session...");
                    m_session.Disconnect();
                    log.Debug(log_text + "I3 session closed.");
                }
                catch (Exception e)
                {
                    log.Error(log_text + "Error Closing I3 Session", e);
                }
            }
            m_opened = false;
        }
        */

        /// <summary>
        /// Download a Recording from PureConnect using the RecordingID
        /// </summary>
        /// <param name="recordingId"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool DownloadFile(string recordingId, string fileName, CallData call=null)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                //var watch = Stopwatch.StartNew();
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
                try
                {
                    TraceTopic.pureConnectTopic.verbose("{} Downloading I3 recording id {} to file {}", log_text, recordingId, fileName);
                    Uri downloadUri = m_recordingsManager.GetSecureExportUri(recordingId, RecordingMediaType.PrimaryMedia, "", 0);

                    if (downloadUri == null)
                    {
                        TraceTopic.pureConnectTopic.error("{} Download URI was NULL: RecordingId: {}", log_text, recordingId);
                    }
                    else
                    {
                        TraceTopic.pureConnectTopic.verbose("{} Download URI: {}", log_text, downloadUri.AbsoluteUri);
                    }

                    WebClient webClient = new WebClient();

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    if (call != null && call[Constants.RecordingInteractionType] == Constants.EMail)
                    {
                        var textFile = fileName.Replace(Constants.EMLExtension, Constants.TXTExtension);
                        webClient.DownloadFile(downloadUri, textFile);
                        ConvertTextToEMail(textFile, call[Constants.FromConnValue], call[Constants.ToConnValue], fileName);
                    }
                    else
                    {
                        webClient.DownloadFile(downloadUri, fileName);
                    }

                    //TraceTopic.pureConnectTopic.verbose("{} Download completed for recording id: {} in {}ms", log_text, recordingId, watch.ElapsedMilliseconds);

                    if (File.Exists(fileName))
                    {
                        TraceTopic.pureConnectTopic.verbose("{} Downloaded completed and checked if File exists for recording id: {}", log_text, recordingId);
                        return true;
                    }
                    else
                    {
                        TraceTopic.pureConnectTopic.error("{} The file: {} Does not exist. There was unexpected error downloading recording id: {}.", log_text, fileName, recordingId);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.error("{} Download failed for recording id:{}. {}" , log_text, recordingId, ex);
                    //EventLog.WriteEntry("UConnector", String.Format("Downloading failed for recording id:{0}, Error: {1}", recordingId, ex.Message), EventLogEntryType.Error, 8001);
                    throw;
                }
            }
        }

        /// <summary>
        /// Open the Connection to Pure Connect
        /// </summary>
        /*
        public bool Open()
        {
            for(int i = 0; i<5;i++)
            {
                if(!m_opened)
                {
                    AttemptToOpen();
                }
                else
                {
                    return m_opened;
                }
                Thread.Sleep(1000);
            }
            return m_opened;
        }
        */

        private void AttemptToOpen()
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                TraceTopic.pureConnectTopic.verbose("Creating I3 RecordingsManager...");

                try
                {
                    //AuthSettings auth = new ICAuthSettings(username, password);
                    //AuthSettings auth = new AlternateWindowsAuthSettings(m_username, m_password);

                    //HostSettings hostSettings = new HostSettings();
                    //hostSettings.HostEndpoint = new HostEndpoint(m_host, m_port);

                    //m_session = CICSession.Instance.GetSession();
                    //log.Debug(log_text + "Connecting to Host: " + m_host + " Port: " + m_port + "User: " + m_username + " Password: " + m_password);
                    //m_session.Connect(new SessionSettings(), hostSettings, auth, new StationlessSettings());
                    //log.Debug(log_text + "Connected to Host: " + m_host + " Port: " + m_port);

                    TraceTopic.pureConnectTopic.verbose("Create QualityManagementManager");
                    QualityManagementManager qualityManagementManager = QualityManagementManager.GetInstance(CICSession.Instance.GetSession());
                    TraceTopic.pureConnectTopic.verbose("Created QualityManagementManager");
                    TraceTopic.pureConnectTopic.verbose("Create RecordingsManager");
                    m_recordingsManager = qualityManagementManager.RecordingsManager;
                    TraceTopic.pureConnectTopic.verbose("Create RecordingsManager");

                    TraceTopic.pureConnectTopic.verbose("I3 session opened.");
                    //m_opened = true;
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error("Could not open I3 session. {}", e);
                    //m_opened = false;
                }
            }
        }

        public void Dispose()
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                if (m_recordingsManager != null)
                {
                    try
                    {
                        m_recordingsManager = null;
                    }
                    catch (Exception e)
                    {
                        TraceTopic.pureConnectTopic.error("Error Disposing of RecordingManager {}", e);
                    }
                }
            }
        }
        public void AddLabel(string recordingId, List<string> m_recordingTag)
        {

            m_recordingsManager.AddRecordingTags(recordingId, m_recordingTag);
        }
    }
}