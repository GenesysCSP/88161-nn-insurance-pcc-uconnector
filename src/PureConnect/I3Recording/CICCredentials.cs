﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PureConnect.CICConfig
{
    /// <summary>
    /// Simple class to obtain the CIC Server details from a DB Connection String
    /// </summary>
    public class CICCredentials
    {
        private string host = "";
        private string password = "";
        private string user = "";

        /// <summary>
        /// Constructor, pass in the DB Connection String
        /// </summary>
        /// <param name="cicConnectionString"></param>
        public CICCredentials(string cicConnectionString)
        {
            if (string.IsNullOrEmpty(cicConnectionString))
            {
                return;
            }
            //Extract the data
            string[] t = cicConnectionString.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            Dictionary<string, string> dictionary = t.Select(item => item.Split('=')).ToDictionary(s => s[0], s => s[1]);
            if (dictionary.ContainsKey("Data Source"))
            {
                this.Host = dictionary["Data Source"];
            }
            if (dictionary.ContainsKey("User ID"))
            {
                this.User = dictionary["User ID"];
            }
            if (dictionary.ContainsKey("Password"))
            {
                this.Password = dictionary["Password"];
            }
        }

        /// <summary>
        /// Host of the CIC Server
        /// </summary>
        public string Host
        {
            get
            {
                return host;
            }

            set
            {
                host = value;
            }
        }

        /// <summary>
        /// Password for the CIC Server
        /// </summary>
        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        /// <summary>
        /// User for the CIC Server
        /// </summary>
        public string User
        {
            get
            {
                return user;
            }

            set
            {
                user = value;
            }
        }
    }
}
