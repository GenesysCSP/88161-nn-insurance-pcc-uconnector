﻿using ININ.IceLib.Connection;
using System;
using PureConnect.CICConfig;
using ININ.IceLib.Configuration;
using System.Collections.ObjectModel;
using GenUtils.TraceTopic;
using ININ.IceLib.Data.TransactionBuilder;
using System.Data;
using ININ.PSO.Tools.PSOTrace;
using PureConnect.Logging;

namespace PureConnect.I3Audio
{
    // https://msdn.microsoft.com/en-gb/library/ff650316.aspx
    /// <summary>
    /// Singleton class that is used to create a CIC Session, it is then used by each ImporterThread to create the RecordingManager
    /// </summary>
    public sealed class CICSession
    {
        private static volatile CICSession instance;
        private static object syncRoot = new Object();
        private string log_text = "I3Download## ";
        private string m_host;

        //private bool m_opened = false;
        private string m_password;

        private int m_port = HostEndpoint.DefaultPort;
        private Session m_session;
        //private Session m_backupsession;
        private string m_username;
        private UserConfigurationList _userConfigurationList = null;
        internal CICCredentials PrimaryCICCredentials;
        internal CICCredentials SecondaryCICCredentials;
        internal int timeout = 60;
        internal int retryCount = 5;

        private bool IsSecondaryActive = false;
        private bool Initialized = false;

        /// <summary>
        /// Private Constructor, create the logger
        /// </summary>
        private CICSession()
        {
        }

        /// <summary>
        /// Get an Instance of this Class
        /// </summary>
        public static CICSession Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new CICSession();
                        }
                    }
                }
                return instance;
            }
        }

        internal int Timeout
        {
            get
            {
                return timeout;
            }

            set
            {
                timeout = value;
            }
        }

        internal int RetryCount
        {
            get
            {
                return retryCount;
            }

            set
            {
                retryCount = value;
            }
        }

        /// <summary>
        /// Close the connection to PureConnect
        /// </summary>
        public void Close()
        {
            if (m_session != null)
            {
                try
                {
                    TraceTopic.pureConnectTopic.verbose("Closing I3 session...");
                    m_session.Disconnect();
                    TraceTopic.pureConnectTopic.verbose("I3 session closed.");
                    m_session.Dispose();
                    m_session = null;
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error("Error Closing I3 Session", e);
                }
            }
        }

        /// <summary>
        /// Get primary CIC Session
        /// </summary>
        /// <returns></returns>
        public Session GetSession()
        {
            if (Initialized)
            {
                if (IsSecondaryActive && m_session == null)
                {
                    Open(SecondaryCICCredentials, ref m_session);
                }
                else if (m_session == null)
                {
                    Open(PrimaryCICCredentials, ref m_session);
                }

                return m_session;
            }
            else
            {
                throw new ApplicationException("CicConnectionProvider is not initialized.");
            }
        }

        public bool Open()
        {
            try
            {
                if (Open(PrimaryCICCredentials, ref m_session))
                {
                    TraceTopic.pureConnectTopic.note("Connecting to the CIC server successful. Server {}", m_session.CustomerName);
                }
                else
                {
                    //Connecting to the secondary server when the primary is switched off / Restarting
                    IsSecondaryActive = true;
                    TraceTopic.pureConnectTopic.note("Connecting to the secondary server {}", SecondaryCICCredentials.Host);
                    Open(SecondaryCICCredentials, ref m_session);
                }

                if (m_session != null)
                {
                    TraceTopic.pureConnectTopic.note("Connecting to the CIC server successful. Server {}", m_session.CustomerName);
                    PSOTrace.WriteRegisteredMessage(CustomEventId.CICConnectionSuccessful);
                    return true;
                }
                else
                {
                    TraceTopic.pureConnectTopic.error("Connecting to the CIC server failed.");
                    PSOTrace.WriteRegisteredMessage(CustomEventId.CICConnectionFailed);
                    return false;
                }
            }
            catch (Exception ex)
            {
                TraceTopic.pureConnectTopic.error("Unable to connect to server. Error: {}", ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Open a CIC Session
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        private bool Open(CICCredentials credentials, ref Session session)
        {
            lock (syncRoot)
            {
                using (TraceTopic.pureConnectTopic.scope())
                {
                    TraceTopic.pureConnectTopic.verbose("{} Open CIC Session", log_text);
                    m_username = credentials.User;
                    m_password = credentials.Password;
                    m_host = credentials.Host;

                    if (session != null && session.ConnectionState == ININ.IceLib.Connection.ConnectionState.Up)
                    {
                        return true;
                    }
                    else
                    {
                        session = AttemptToOpen(session);
                        if (session != null)
                        {
                            return true;
                        }
                    }
                    return false;
                }
            }
        }

        /// <summary>
        /// Attempt to Open a CIC Session
        /// </summary>
        /// <returns></returns>
        private Session AttemptToOpen(Session session)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                if (session != null && session.ConnectionState == ININ.IceLib.Connection.ConnectionState.Up)
                {
                    TraceTopic.pureConnectTopic.verbose("Session Already Opened and state is Up");
                    return session;
                }

                TraceTopic.pureConnectTopic.verbose("Attempting to Open session...");

                try
                {
                    AuthSettings auth = new ICAuthSettings(m_username, m_password);
                    //AuthSettings auth = new AlternateWindowsAuthSettings(m_username, m_password);
                    HostSettings hostSettings = new HostSettings(new HostEndpoint(m_host));
                    //hostSettings.HostEndpoint = new HostEndpoint(m_host, m_port);
                    TraceTopic.pureConnectTopic.verbose("Create New Session");
                    session = new Session();
                    session.AutoReconnectEnabled = true;
                    session.ConnectionStateChanged += ConnectionStateChanged;
                    TraceTopic.pureConnectTopic.verbose("Connecting to Host: {} Port: {} User: {} Password: ***********", m_host, m_port, m_username);
                    session.Connect(new SessionSettings(), hostSettings, auth, new StationlessSettings());
                    TraceTopic.pureConnectTopic.verbose("Connected to Host: {} Port: {}", m_host, m_port);
                    TraceTopic.pureConnectTopic.verbose("CIC session opened.");
                    //m_opened = true;
                    return session;
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.error("Could not open I3 session. {}", e);
                    PSOTrace.WriteRegisteredMessage(EventId.CICConnectionLogOnFailed);
                    //m_opened = false;
                }
            }
            return null;

        }

        private void ConnectionStateChanged(object sender, ConnectionStateChangedEventArgs e)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    TraceTopic.pureConnectTopic.status("Session State Changed Event: \r\n State: {}\r\n Reason: {}\r\n Message: {}", e.State, e.Reason, e.Message);
                    var session = sender as Session;
                    if (e.State == ININ.IceLib.Connection.ConnectionState.Up)
                    {
                        // All actions to be performed when the CIC connection transitions to an UP state
                        PSOTrace.WriteRegisteredMessage(EventId.CICConnectionLogOn);
                        m_session = session;
                        Initialized = true;
                    }
                    //It's crucial that we're not checking state here. Only on a state change when the session was initialized do we care.
                    //If the session is null and the state changes, but it was not changed to 'Up'... there's nothing to do. 
                    //We simply wait for the next state changed event.
                    else if (m_session != null)
                    {
                        switch (e.Reason)
                        {
                            case ConnectionStateReason.ServerNotResponding:
                                PSOTrace.WriteRegisteredMessage(EventId.CICConnectionServerNotResponding, "Message: " + e.Message);
                                break;
                            case ConnectionStateReason.LogOnFailed:
                                PSOTrace.WriteRegisteredMessage(EventId.CICConnectionLogOnFailed, "Message: " + e.Message);
                                break;
                            case ConnectionStateReason.AdminLogOff:
                                PSOTrace.WriteRegisteredMessage(EventId.CICConnectionAdminLogOff, "Message: " + e.Message);
                                break;
                            case ConnectionStateReason.UserDeleted:
                                PSOTrace.WriteRegisteredMessage(EventId.CICConnectionUserDeleted, "Message: " + e.Message);
                                break;
                            case ConnectionStateReason.StationDeleted:
                                PSOTrace.WriteRegisteredMessage(EventId.CICConnectionStationDeleted, "Message: " + e.Message);
                                break;
                            case ConnectionStateReason.StationDeactivated:
                                PSOTrace.WriteRegisteredMessage(EventId.CICConnectionStationDeactivated, "Message: " + e.Message);
                                break;
                            case ConnectionStateReason.AnotherLogOn:
                                PSOTrace.WriteRegisteredMessage(EventId.CICConnectionAnotherLogOn, "Message: " + e.Message);
                                break;
                            case ConnectionStateReason.UserLogOff:
                                PSOTrace.WriteRegisteredMessage(EventId.CICConnectionUserLogOff, "Message: " + e.Message);
                                break;
                            case ConnectionStateReason.SessionTimeout:
                                PSOTrace.WriteRegisteredMessage(EventId.CICConnectionSessionTimeOut, "Message: " + e.Message);
                                break;
                            case ConnectionStateReason.Switchover:
                                PSOTrace.WriteRegisteredMessage(EventId.CICConnectionSwitchover, "Message: " + e.Message);
                                break;
                            default:
                                var message = $"Session went down for reason: {e.Reason} - {e.Message}";
                                PSOTrace.WriteRegisteredMessage(EventId.CICConnectionLogOnFailed, "Message: " + e.Message);
                                //PSOTrace.WriteRegisteredMessage(CustomEventId.CicSessionDown, message);
                                //PSOTraceCustom.DataAccessTopic.error(message);
                                break;
                        }
                        m_session = null;
                        Initialized = false;
                    }
                }
                catch (Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                }
            }
        }

        public ReadOnlyCollection<UserConfiguration> CreateUserConfigurationList()
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    Session session = Instance.GetSession();
                    if (_userConfigurationList == null && session.ConnectionState.Equals(ININ.IceLib.Connection.ConnectionState.Up))
                    {
                        _userConfigurationList = new UserConfigurationList(ConfigurationManager.GetInstance(session));

                        var querySettings = _userConfigurationList.CreateQuerySettings();

                        querySettings.SetPropertiesToRetrieve(
                            UserConfiguration.Property.DisplayName,
                            UserConfiguration.Property.PersonalInformation_GivenName,
                            UserConfiguration.Property.PersonalInformation_Surname,
                            UserConfiguration.Property.Workgroups,
                            UserConfiguration.Property.Id,
                            UserConfiguration.Property.StandardProperties_CustomAttributes,
                            UserConfiguration.Property.Roles
                            );

                        querySettings.SetRightsFilterToView();

                        if (!_userConfigurationList.IsCaching)
                        {
                            _userConfigurationList.StartCaching(querySettings);
                        }

                        var users = _userConfigurationList.GetConfigurationList();
                        return users;
                    }
                }
                catch (Exception e)
                {
                    TraceTopic.pureConnectTopic.exception(e);
                }
                finally
                {
                    if (_userConfigurationList.IsCaching)
                    {
                        _userConfigurationList.StopCaching();
                        _userConfigurationList = null;
                    }
                }
            }
            return null;

        }
    }
}