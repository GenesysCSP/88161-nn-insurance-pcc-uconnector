﻿using ININ.IceLib.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace PureConnect.I3Audio
{
    class SegmentDetails
    {

        //private string segmentLog = "/20180219T105121.209+00/0?S=50&E=3502&CUTC=20180219T105421.437+00&1=100117531950180219&2=0&3=15&4=&5=workgroup%20queue:Customer%20Support%20Signposting&6=LocalTransfer&7=%3cDetails%20TransferredInteractionId%3d%22100117531950180219%22%20/%3e%0a|/20180219T105121.225+00/0?S=50&E=3502&CUTC=20180219T105421.437+00&1=100117531950180219&2=0&3=0&4=&5=workgroup%20queue:_BusyRuleFlowOut&6=LocalDisconnect&7=";
        private string segmentLog = "/20161117T100559.058-05/0?S=50&E=3501&CUTC=20161117T132558.963-05&1=2001297713N0161117&2=0&3=405&4=&5=&6=Queue&7=|/20161117T100559.463-05/0?S=50&E=3502&CUTC=20161117T132558.963-05&1=2001297713N0161117&2=0&3=48672&4=&5=workgroup%20queue:SDC_East_Email_Bil&6=Interact&7=%3cDetails%20ACDSkillSet%3d%22Email%7cChristine.Matthews_Email%202016-11-17%2015:05:59.4638775%22%20/%3e%0a|/20161117T100648.136-05/0?S=50&E=3504&CUTC=20161117T132558.963-05&1=2001297713N0161117&2=0&3=819520&4=user%20queue:Christine.Matthews&5=workgroup%20queue:SDC_East_Email_Bil&6=LocalTransfer&7=%3cDetails%20TransferringUser%3d%22Christine%20Matthews%22%20TransferringInteractionId%3d%222001297713N0161117%22%20TransferredInteractionId%3d%222001297713N0161117%22%20ACDSkillSet%3d%22Email%7cChristine.Matthews_Email%202016-11-17%2015:05:59.4638775%22%20/%3e%0a&8=REQUEUED/TRANSFER&9=3|/20161117T102027.656-05/0?S=50&E=3502&CUTC=20161117T132558.963-05&1=2001297713N0161117&2=0&3=18002&4=user%20queue:Laura.Montreuil&5=workgroup%20queue:SDC_East_Email_Bil&6=Park&7=%3cDetails%20ACDSkillSet%3d%22Email%7cChristine.Matthews_Email%202016-11-17%2015:05:59.4638775%22%20/%3e%0a|/20161117T102045.658-05/0?S=50&E=3513&CUTC=20161117T132558.963-05&1=2001297713N0161117&2=0&3=379285&4=user%20queue:Laura.Montreuil&5=workgroup%20queue:SDC_East_Email_Bil&6=Interact&7=%3cDetails%20ACDSkillSet%3d%22Email%7cChristine.Matthews_Email%202016-11-17%2015:05:59.4638775%22%20/%3e%0a|/20161117T102114.410-05/0?S=50&E=3510&CUTC=20161117T132558.963-05&1=2001297713N0161117&2=0&3=60014&4=user%20queue:Christine.Matthews&5=workgroup%20queue:SDC_East_Email_Bil|/20161117T102704.943-05/0?S=50&E=3504&CUTC=20161117T132558.963-05&1=2001297713N0161117&2=0&3=10553994&4=user%20queue:Laura.Montreuil&5=workgroup%20queue:SDC_East_Email_Bil&6=LocalDisconnect&7=%3cDetails%20ACDSkillSet%3d%22Email%7cChristine.Matthews_Email%202016-11-17%2015:05:59.4638775%22%20/%3e%0a&8=OPTIONAL%20SERVICES&9=4";


        public SegmentDetails()
        {
            processLog(this.segmentLog);
        }

        private void processLog(string segmentLog)
        {

            string[] segDetail = segmentLog.Split('|');
            if (!segDetail[6].Contains("ConfParty"))
            {
                var userCount = segDetail[6].Count(f => f == ':');
                //ParserEngineTopic.note("userCount: {}.", userCount);
                if (userCount > 0)
                {
                    var users = new List<string>();

                    var split = segDetail[6].Split(); //what are we splitting? 

                    foreach (var t in split)
                    {
                        //ParserEngineTopic.note("split[i]: {}.", t);
                        if (t.Contains(":"))
                        {
                            users.Add(t.Split(':')[1]);
                        }
                    }
                    segDetail[6] = string.Join("|", users);
                }
                else
                {
                    segDetail[6] = segDetail[6].Split('=')[1];
                }
            }
            else
            {
                var users = new List<string>();
                const string pattern = "Name=(\".*?\"|'.*?'|[^\"'][^\\s]*)";
                var regEx = new Regex(pattern);
                //foreach (var match in regEx.Matches(HttpUtility.UrlDecode(segDetail[6])))
                foreach (var match in regEx.Matches(segDetail[6]))
                {
                    if (string.IsNullOrEmpty(match.ToString().Split('"')[1]))
                    {
                        continue;
                    }

                    Console.WriteLine(match);
                    users.Add(match.ToString().Split('"')[1]);
                }
                segDetail[6] = string.Join("|", users.Where(se => !string.IsNullOrEmpty(segmentLog)));

            }

        }


    }
}
