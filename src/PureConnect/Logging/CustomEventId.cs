﻿using ININ.PSO.Tools.PSOTrace;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureConnect.Logging
{
    public class CustomEventId : EventId
    {
        [EventIdAttributes(EventMessage = "Connection to CIC server successful.", EventType = EventLogEntryType.SuccessAudit)]
        public const int CICConnectionSuccessful = 8001;

        [EventIdAttributes(EventMessage = "Connection to CIC server failed.", EventType = EventLogEntryType.Error)]
        public const int CICConnectionFailed = 8002;

        [EventIdAttributes(EventMessage = "Connection to SpeechMiner Database failed.", EventType = EventLogEntryType.Error)]
        public const int SMConnectionFailed = 8003;

        [EventIdAttributes(EventMessage = "Error occured in reading configuration.", EventType = EventLogEntryType.Error)]
        public const int ConfigLoadingFailed = 8004;

        [EventIdAttributes(EventMessage = "Error during elections of working instance.", EventType = EventLogEntryType.Error)]
        public const int HAInstanceSelectionFailed = 8005;

        [EventIdAttributes(EventMessage = "Error during Importing of interactions", EventType = EventLogEntryType.Error)]
        public const int ErrorInImportingInteractions = 8006;

        [EventIdAttributes(EventMessage = "Error during Processing of interaction", EventType = EventLogEntryType.Error)]
        public const int ErrorInProcessingInteractions = 8007;

        [EventIdAttributes(EventMessage = "Error in downloading interaction data", EventType = EventLogEntryType.Error)]
        public const int ErrorInDownloadingData = 8008;

        [EventIdAttributes(EventMessage = "Error in content file generation of interaction", EventType = EventLogEntryType.Error)]
        public const int ErrorInGenerationOfContentData = 8009;

        [EventIdAttributes(EventMessage = "Error in metadata file generation of interaction", EventType = EventLogEntryType.Error)]
        public const int ErrorInGenerationOfMetadata = 8010;

        [EventIdAttributes(EventMessage = "Error in UConnector", EventType = EventLogEntryType.Error)]
        public const int UConnectorError = 8011;

    }
}
