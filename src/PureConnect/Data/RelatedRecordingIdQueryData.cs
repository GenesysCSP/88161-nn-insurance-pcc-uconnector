﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using PureConnect.I3Audio;
using utopy.bDBServices;
using GenUtils.TraceTopic;
using ININ.IceLib.Data.TransactionBuilder;

namespace PureConnect.Data
{
    public class RelatedRecordingIdQueryData:BaseQueryData
    {

        public RelatedRecordingIdQueryData()
        {
        }

        public bool GetData(Dictionary<string, CallData> calls, ConcurrentDictionary<string, string> relatedRecordingIds)
        {

            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    if (relatedRecordingIds.Count == 0)
                    {
                        TraceTopic.pureConnectTopic.note("No RelatedRecordingIds to process");
                        return false;
                    }
                    string sRelatedRecordingIds = string.Join(",", relatedRecordingIds.Keys);

                    TraceTopic.pureConnectTopic.always("Details of the Related Recordings: {}", sRelatedRecordingIds);

                    var session = CICSession.Instance.GetSession();

                    var transactionData = new TransactionData { ProcedureName = "PCC_UC_Select_RelatedRecordings" };

                    var sqlParamrelatedRecordingIds = new TransactionParameter
                    {
                        Value = sRelatedRecordingIds,
                        Type = ParameterType.In,
                        DataType = ParameterDataType.String
                    };

                    transactionData.Parameters.Add(sqlParamrelatedRecordingIds);

                    DataTable dtRelatedRecordings = GetData(session, transactionData);

                    DataRowCollection rows = dtRelatedRecordings.Rows;
                    TraceTopic.pureConnectTopic.always("Got rows: {}", rows.Count);
                    if (rows.Count == 0)
                    {
                        return false;
                    }

                    //Get Data from IR_RecordingMedia.
                    //DateTimes have a DST Offset in Minutes.
                    //StringBuilder interactionIds = new StringBuilder();
                    foreach (DataRow row in rows)
                    {
                        Guid recordingId = new Guid(row["RecordingId"].ToString());

                        if (row.IsNull("QueueObjectIdKey"))
                        {
                            TraceTopic.pureConnectTopic.always("QueueObjectIdKey column is null for RecordingId = {}, skipping.", recordingId);
                            continue;
                        }
                        string callId = (string)row["QueueObjectIdKey"]; // nullable

                        string sOriginalCallId = "";
                        if (relatedRecordingIds.TryGetValue(recordingId.ToString(), out sOriginalCallId))
                        {
                            calls[sOriginalCallId]["RelatedInteractionIdKey"] = callId;
                        }
                        //DateTime callTime = (DateTime)row["RecordingDate"]; // not null
                        //var test = row["RecordingDate"];

                    }
                }
                catch(Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                }
            }
            return true;
        }
    }
}