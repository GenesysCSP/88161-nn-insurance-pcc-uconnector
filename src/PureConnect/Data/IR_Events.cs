﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PureConnect.Data
{
    public class IR_Events
    {

        /// <summary>
        /// Class the contains the information from each row of the IR_Event table
        /// Used to determine when the Secure Pause occurs
        /// </summary>




        /// <summary>
        /// Get the Duration, EventType and EventDate timestamp from the data row
        /// </summary>
        /// <param name="row"></param>
        public IR_Events(DataRow row)
        {
            if(row !=null)
            {
                Duration = (int)row["Duration"];
                EventType = Convert.ToInt16((short)row["EventType"]);
                EventDate = (DateTime)row["EventDate"];
            }
        }

        /// <summary>
        /// Duration of the Event
        /// </summary>
        public int Duration { get; set; }
        /// <summary>
        /// EventType
        /// 0-  Participant Connected
        /// 1-  Call went on hold
        /// 2-  Bookmark on the recording
        /// 3-  Keyword spot on the recording
        /// 4-  Recording started on the media server
        /// 5-  Secure pause
        /// 6-  Secure IVR
        /// 7-  Wrapup Code
        /// 8-  Transfer
        /// 9-  Snippet Request
        /// 10- Park
        /// </summary>
        public int EventType { get; set; }

        /// <summary>
        /// DateTime of Event
        /// </summary>
        public DateTime EventDate{get;set;}
    }
}
