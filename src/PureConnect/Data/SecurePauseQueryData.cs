﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using PureConnect.I3Audio;
using utopy.bDBServices;
using GenUtils.TraceTopic;
using ININ.IceLib.Data.TransactionBuilder;

namespace PureConnect.Data
{
    /// <summary>
    /// Class to process the MultipleSegment Query
    /// </summary>
    class SecurePauseQueryData:BaseQueryData
    {
        public SecurePauseQueryData()
        {
        }

        public bool GetData(string recordingIds, Dictionary<string, CallData> calls, ConcurrentDictionary<string, string> recordingIdsToInteractionID)
        {
            using (TraceTopic.pureConnectTopic.scope())
            {
                try
                {
                    TraceTopic.pureConnectTopic.always("Secure Pause for the Recording Ids: {}", recordingIds);

                    var session = CICSession.Instance.GetSession();

                    var transactionData = new TransactionData { ProcedureName = "PCC_UC_Select_SecurePause" };

                    var sqlParamRecordingIds = new TransactionParameter
                    {
                        Value = recordingIds,
                        Type = ParameterType.In,
                        DataType = ParameterDataType.String
                    };

                    transactionData.Parameters.Add(sqlParamRecordingIds);

                    DataTable dtSecurePause = GetData(session, transactionData);

                    DataRowCollection rows = dtSecurePause.Rows;
                    foreach (DataRow row in rows)
                    {
                        Guid recordingId = new Guid(row["RecordingId"].ToString());
                        if (recordingIdsToInteractionID.ContainsKey(recordingId.ToString()))
                        {
                            string callId = "";
                            if (recordingIdsToInteractionID.TryGetValue(recordingId.ToString(), out callId))
                            {
                                CallData call = calls[callId];
                                IR_Events ir_event = new IR_Events(row);
                                call.AddIREvent(ir_event);
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    TraceTopic.pureConnectTopic.exception(ex);
                }
            }
            return true;
        }
    }
}
