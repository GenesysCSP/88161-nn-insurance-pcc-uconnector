<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ViewConnectionStrings
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.lstConnectionStrings = New System.Windows.Forms.ListBox
    Me.btnAddNewConnection = New System.Windows.Forms.Button
    Me.SuspendLayout()
    '
    'lstConnectionStrings
    '
    Me.lstConnectionStrings.FormattingEnabled = True
    Me.lstConnectionStrings.Location = New System.Drawing.Point(15, 11)
    Me.lstConnectionStrings.Name = "lstConnectionStrings"
    Me.lstConnectionStrings.Size = New System.Drawing.Size(262, 251)
    Me.lstConnectionStrings.TabIndex = 0
    '
    'btnAddNewConnection
    '
    Me.btnAddNewConnection.Location = New System.Drawing.Point(203, 275)
    Me.btnAddNewConnection.Name = "btnAddNewConnection"
    Me.btnAddNewConnection.Size = New System.Drawing.Size(75, 23)
    Me.btnAddNewConnection.TabIndex = 1
    Me.btnAddNewConnection.Text = "New"
    Me.btnAddNewConnection.UseVisualStyleBackColor = True
    '
    'ViewConnectionStrings
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(293, 308)
    Me.Controls.Add(Me.btnAddNewConnection)
    Me.Controls.Add(Me.lstConnectionStrings)
    Me.Name = "ViewConnectionStrings"
    Me.Text = "Connection Strings"
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents lstConnectionStrings As System.Windows.Forms.ListBox
  Friend WithEvents btnAddNewConnection As System.Windows.Forms.Button
End Class
