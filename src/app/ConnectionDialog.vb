Imports System.Windows.Forms
Imports System.Data.SqlClient

Public Class ConnectionDialog


    Private Shared boolConnectionChanged As Boolean = False
    Dim strOldConnectionString As String
    Dim radOracle As Object

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Check syntax of connection string.  If ok then save it in the connectorApp.exe.config file and close this dialog.

        'Make sure user has named the connection. This name is to be placed in the ConnectionStringName section of config xml's such as NICE.xml.
        If Me.txtName.Text = String.Empty Then
            MessageBox.Show("Please enter a name for this connection string.")
            Me.txtName.Focus()
            Exit Sub
        End If

        'Check the connection string syntax for SQL Server and Oracle connections.
        Dim boolSyntaxOk As Boolean
        If Me.radSqlServer.Checked Then
            boolSyntaxOk = Me.checkSqlServerConnection(True)
        Else
            boolSyntaxOk = True 'do not check syntax
        End If

        If Not boolSyntaxOk Then
            Me.txtConnectionString.Focus()
            Exit Sub
        End If

        'Add connection string to connectorApp.exe.config.
        Dim boolEncrypt As Boolean = True
        Dim strConnectionString As String = Me.txtConnectionString.Text

        Dim frmVCS As New ViewConnectionStrings
        frmVCS.SetConfigConnectionString(Me.txtName.Text, strConnectionString, boolEncrypt)
        frmVCS.Owner = Me.Owner

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()


    End Sub


    Private Function checkSqlServerConnection(Optional ByVal _boolSyntaxOnly As Boolean = False) As Boolean
        Dim ret As Boolean = False
        Try
            Using cn As SqlConnection = New SqlConnection(Me.txtConnectionString.Text)
                If Not _boolSyntaxOnly Then
                    cn.Open()
                    If cn.State() = ConnectionState.Open Then 'connection is good
                        MessageBox.Show("Connection successfully established", "", MessageBoxButtons.OK)
                        cn.Close()
                        ret = True
                    End If
                Else
                    ret = True
                End If
            End Using
            Return ret
        Catch exSql As SqlException
            MessageBox.Show("Connection failed.", "Problem establishing connection", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return ret
        Catch exArgument As System.ArgumentException
            MessageBox.Show("Incorrect syntax for SQL Server connection.", "Error in connection string.", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return ret
        End Try
    End Function

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub


    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Set  <connectionStrings> property in appConnector.exe.config
        If Me.txtName.Text.Length = 0 Then
            Exit Sub
        End If

        If MessageBox.Show("Are you sure that you want to delete " + Me.txtName.Text + "?", "Delete connection string", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = System.Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim frmVCS As New ViewConnectionStrings
        frmVCS.DeleteConnectionString(Me.txtName.Text)
        frmVCS.Owner = Me.Owner()
        frmVCS.PopulateConnectionListBox()
        Me.Close()


    End Sub

    Private Sub btnTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTest.Click
        If Me.radSqlServer.Checked Then
            Me.checkSqlServerConnection()
        End If

    End Sub

    Private Sub ConnectionDialog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        boolConnectionChanged = False
    End Sub

    Private Sub chkLocked_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLocked.CheckedChanged
        'Enable/disable text-box txtConnectionString.
        Me.txtConnectionString.Enabled = Not Me.chkLocked.Checked
    End Sub

    Private Sub txtConnectionString_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtConnectionString.KeyPress

        'Clear the connection string when user presses a key in txConnectionString for the first time.
        If boolConnectionChanged = False Then

            strOldConnectionString = Me.txtConnectionString.Text 'store old connection string in case user hits ESC or Ctrl-Z
            Me.txtConnectionString.Clear()
            boolConnectionChanged = True
            Me.txtConnectionString.UseSystemPasswordChar = False  'allow user to see what they are typing


        End If
        'Undo changes if ESC or Ctrl-Z is pressed.
        If Asc(e.KeyChar) = Keys.Escape Or Asc(e.KeyChar) = &H1A Then 'Ascii value of Ctrl-Z = 0x1A
            Me.txtConnectionString.UseSystemPasswordChar = True
            Me.txtConnectionString.Text = strOldConnectionString
            boolConnectionChanged = False
        End If



    End Sub

    Private Sub radOther_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radOther.CheckedChanged
        Me.btnTest.Enabled = Not Me.radOther.Checked
    End Sub


End Class
