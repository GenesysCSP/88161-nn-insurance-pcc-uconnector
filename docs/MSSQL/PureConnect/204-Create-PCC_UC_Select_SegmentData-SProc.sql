
GO
/****** Object:  StoredProcedure [dbo].[PCC_UC_Select_SegmentData]    Script Date: 1/2/2019 12:32:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PCC_UC_Select_SegmentData] (@interactionIds VARCHAR(MAX))
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @ProcDate datetime=DATEADD(day, -7, GETDATE())
	Create table  #temp
	(callID varchar(max))

	insert into #temp (callID)
	select Item from dbo.SplitString(@interactionIds,',')
	SELECT b.AssignedWorkGroup,b.Duration,b.SegmentType,a.CallIDKey,c.ICUserID 
     FROM
     (SELECT IntxID,CallIDKey,StartDateTime,IndivID FROM Intx_Participant) a
     left outer JOIN
     (SELECT Duration,SegmentType,IntxID,AssignedWorkGroup FROM IntxSegment) b
     ON a.IntxID =  b.IntxID
     left outer JOIN
     (SELECT ICUserID,IndivID FROM Individual) c
     ON a.IndivID = c.IndivID
	 
     WHERE b.SegmentType IN ('32', '64', '128') AND 
	 a.CallIDKey IN(select callID from  #temp) and ICUserID is not null and StartDateTime > @ProcDate
     ORDER BY a.StartDateTime, a.CallIDKey Desc;

   drop table #temp
END
