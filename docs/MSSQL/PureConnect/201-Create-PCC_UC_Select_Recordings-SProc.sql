
GO
/****** Object:  StoredProcedure [dbo].[PCC_UC_Select_Recordings]    Script Date: 1/2/2019 12:36:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PCC_UC_Select_Recordings] (@batch_size nVARCHAR(50), @last_call_time nVARCHAR(50))  
AS  
BEGIN  
 SET NOCOUNT ON;  
 DECLARE @size INT  
 SET @size = CONVERT(INT, @batch_size)  
 SELECT TOP (@size)  
                RecordingId,   
                RelatedRecordingId,   
                FileSize,   
                Duration,   
                FromConnValue,   
                ToConnValue,   
                -- IR_RecordingMedia.Direction,  
				CASE IR_RecordingMedia.Direction
				   WHEN '1'
				   THEN 'Inbound'
				   WHEN '2'
				   THEN 'Outbound'
				   WHEN '3'
				   THEN 'Intercom'
				END AS Direction,				
                QueueObjectIdKey,   
                CallType,   
                IsArchived,   
                RecordingType,  
                RecordingDate,  
                RecordingDateOffset ,
				InteractionSummary.MediaType
            FROM IR_RecordingMedia  
            LEFT OUTER JOIN InteractionSummary  on IR_RecordingMedia.QueueObjectIdKey = InteractionSummary.InteractionIDKey   
            WHERE IR_RecordingMedia.MediaType in (1, 2, 3, 6)
				and RecordingDate >= convert(date,dateadd(day,-1,@last_call_time))
				and InteractionSummary.MediaType Is not null
                and QueueObjectIdKey <> ''  
                and Duration > 0  
				and IR_RecordingMedia.RecordingId Not In (Select RecordingID from IR_TagMap WHERE TagID = (SELECT TagId FROM IR_Tag WHERE TagName = 'UConnector_Processed'))
				and MediaStatus IS NOT NULL
                and (InteractionSummary.LastAssignedWorkgroupID is null or InteractionSummary.LastAssignedWorkgroupID NOT like 'ITA %')  
				-- and RIGHT(LEFT(QueueObjectIdKey,4),3) = @site_id
            ORDER BY RecordingDate ASC;  
END  
  