
GO
/****** Object:  StoredProcedure [dbo].[PCC_UC_Select_RelatedRecordings]    Script Date: 1/2/2019 12:23:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PCC_UC_Select_RelatedRecordings] (@relatedRecordingIds VARCHAR(MAX))
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
      RecordingId,
      QueueObjectIdKey
      FROM IR_RecordingMedia
      WHERE RecordingId IN (select * from dbo.SplitString(@relatedRecordingIds,','));
END
