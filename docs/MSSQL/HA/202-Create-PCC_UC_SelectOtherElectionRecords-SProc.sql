
GO

/****** Object:  StoredProcedure [dbo].[PCC_UC_SelectOtherElectionRecords]    Script Date: 1/2/2019 5:35:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PCC_UC_SelectOtherElectionRecords] (@guid varchar(50), @lastTime datetime, @comment varchar(MAX))
AS
BEGIN
select * from PURECONNECT_UCONNECTOR where guid <> @guid and ts > @lastTime and comment = @comment
END
GO


