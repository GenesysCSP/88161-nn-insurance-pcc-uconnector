GO

/****** Object:  StoredProcedure [dbo].[PCC_UC_DeleteElectionRecord]    Script Date: 1/2/2019 5:32:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PCC_UC_DeleteElectionRecord] (@guid VARCHAR(50), @comment varchar(MAX))
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM PURECONNECT_UCONNECTOR WHERE guid = @guid and comment = @comment
END
GO


